describe('AccessVirus', function () {

  var program = [];

  beforeEach(function () {
    module('f0f7.devices');
    module(function ($provide) {
      $provide.factory('Sysex', function() {
        return {
          sendSysex : function() {
            return true;
          }
        };
      });
      $provide.factory('WebMIDI', function() {
        return {
          midiOut : {}
        };
      });
    });
    inject(function($injector) {
        AccessVirus = $injector.get('AccessVirus');
    });
  });

  it('can get an instance of my factory', function() {
    expect(AccessVirus).toBeDefined();
  });
});

