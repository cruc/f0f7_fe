describe('IntellijelRainmaker', function () {

  var program = [];
  var packedProgram = [0xF0, 0x00, 0x02, 0x14, 0x00, 0x7E, 0x02, 0x40, 0x14, 0x00, 0x0C, 0x02, 0x10, 0x25, 0x62, 0x00, 0x03, 0x00, 0x44, 0x09, 0x40, 0x60, 0x60, 0x03, 0x51, 0x02, 0x30, 0x08, 0x40, 0x13, 0x40, 0x00, 0x0C, 0x3E, 0x10, 0x25, 0x00, 0x03, 0x08, 0x00, 0x44, 0x09, 0x40, 0x20, 0x00, 0x51, 0x10, 0x02, 0x30, 0x08, 0x40, 0x14, 0x00, 0x0C, 0x10, 0x02, 0x10, 0x25, 0x00, 0x03, 0x00, 0x44, 0x02, 0x09, 0x40, 0x20, 0x00, 0x51, 0x02, 0x30, 0x04, 0x08, 0x40, 0x14, 0x00, 0x0C, 0x02, 0x10, 0x44, 0x25, 0x00, 0x03, 0x00, 0x44, 0x09, 0x40, 0x40, 0x20, 0x00, 0x51, 0x02, 0x30, 0x08, 0x2C, 0x1F, 0x3E, 0x20, 0x26, 0x7F, 0x64, 0x13, 0x02, 0x55, 0x42, 0x79, 0x04, 0x00, 0x20, 0x06, 0x50, 0x69, 0x7F, 0x2A, 0x27, 0x77, 0x04, 0x53, 0x7F, 0x05, 0x7F, 0x75, 0x18, 0x78, 0x15, 0x00, 0x00, 0x29, 0x20, 0x5C, 0x2D, 0x50, 0x6D, 0x7A, 0x04, 0x00, 0x0A, 0x18, 0x02, 0x00, 0x09, 0x28, 0x30, 0x00, 0x1F, 0x2C, 0x2E, 0x03, 0x29, 0x27, 0x1C, 0x03, 0x7F, 0x7F, 0x58, 0xF7];

  beforeEach(function () {
    module('f0f7.devices');
    module(function ($provide) {
      $provide.factory('Sysex', function() {
        return {
          sendSysex : function() {
            return true;
          }
        };
      });
      $provide.factory('WebMIDI', function() {
        return {
          midiOut : {}
        };
      });
    });
    inject(function($injector) {
        IntellijelRainmaker = $injector.get('IntellijelRainmaker');
    });
  });

  it('can get an instance of my factory', function() {
    expect(IntellijelRainmaker).toBeDefined();
  });

  describe('Splice Raw Data', function () {
    it('Mock Data should have the right length', function() {
      expect(packedProgram.length).toEqual(155);
    });
    it('Raw Data should have the right length', function() {
      var parserConfig = IntellijelRainmaker.params.parserConfig;
      var rawData = packedProgram.slice(parserConfig.rawData.from, parserConfig.rawData.to);
      expect(rawData.length).toEqual(147);
    });
  });

  describe('convert Rainmaker Charset', function () {
    var nameArray = [9, 40, 48, 31, 44, 46, 3, 41, 39, 28, 255];

    it('Should have the right length', function() {
      expect(nameArray.length).toEqual(11);
    });

    it('Should convert Hex to Ascii', function() {
      var result = IntellijelRainmaker.mapRainmakerHexToASCII(11);
      expect(result).toEqual('K');
    });

    it('Should have the right name', function() {
      var patchName = IntellijelRainmaker.nameArrayToString(nameArray);
      expect(patchName).toEqual('InvertComb');
    });

    it('Should have no name', function() {
      var patchName = IntellijelRainmaker.nameArrayToString([0, 255, 300]);
      expect(patchName).toEqual('no name');
    });

    it('Should create padded Array of right length', function() {
      var result = IntellijelRainmaker.createPaddedRMArray('InvertComb');
      expect(result.length).toEqual(10);
    });

    it('Should create padded Array', function() {
      var result = IntellijelRainmaker.createPaddedRMArray('InvertComb');
      // expect(result).toEqual(nameArray);
      expect(result).toEqual([9, 40, 48, 31, 44, 46, 3, 41, 39, 28]);
    });

  });

});

