#!/bin/bash
_now=$(date +"%m_%d_%Y")
_file="/var/www/html/f0f7/sql_backups/f0f7_$_now.sql"
echo "Starting backup to $_file..."
mysqldump --user=root -proot --databases f0f7 > "$_file"