angular.module('f0f7.restService', [])
.factory('RestService', function($http, $q, urls, $log) {
  return {
    listPrivateBanks: function(data, success, error) {
      $http({
        method : 'GET',
        url : urls.API_ENDPOINT + '/banks',
        params : data,
        cache : false
      }).success(success).error(error);
    },
    listPublicBanks: function(data, success, error) {
      $http({
        method : 'GET',
        url : urls.API_ENDPOINT + '/banks/public_index',
        params : data,
        cache : false
      }).success(success).error(error);
    },
    getBank: function(bankId, success, error) {
      $http({
        method : 'GET',
        url : urls.API_ENDPOINT + '/banks/' + bankId,
        cache : false
      }).success(success).error(error);
    },
    deleteBank: function(bankId, success, error) {
      $http({
        method : 'DELETE',
        url : urls.API_ENDPOINT + '/banks/' + bankId,
        cache : false
      }).success(success).error(error);
    },
    saveBank: function(data, success, error) {
      $http({
        method : 'POST',
        url : urls.API_ENDPOINT + '/banks/add',
        data : data,
        cache : false
      }).success(success).error(error);
    },
    updateProgramsInBank: function(data, success, error) {
      $http({
        method : 'PUT',
        url : urls.API_ENDPOINT + '/banks/update_programs/' +  data.bank.id,
        data : data,
        cache : false
      }).success(success).error(error);
    },
    editBank: function(data, success, error) {
      $http({
        method : 'PUT',
        url : urls.API_ENDPOINT + '/banks/' + data.id,
        data : data,
        cache : false
      }).success(success).error(error);
    },
  };
})
.factory('Auth', function ($http, $localStorage, urls, User) {
  function urlBase64Decode(str) {
    var output = str.replace('-', '+').replace('_', '/');
    switch (output.length % 4) {
      case 0:
      break;
      case 2:
      output += '==';
      break;
      case 3:
      output += '=';
      break;
      default:
      throw 'Illegal base64url string!';
    }
    return window.atob(output);
  }

  function getClaimsFromToken() {
    var token = $localStorage.token;
    var user = {};
    if (typeof token !== 'undefined') {
      var encoded = token.split('.')[1];
      user = JSON.parse(urlBase64Decode(encoded));
    }
    return user;
  }

  var tokenClaims = getClaimsFromToken();

  return {
    signup: function (data, success, error) {
      $http.post(urls.API_ENDPOINT + '/users/add', data).success(success).error(error);
    },
    editProfile: function (data, success, error) {
      $http.post(urls.API_ENDPOINT + '/users/edit/' + tokenClaims.sub, data).success(success).error(error);
    },
    deleteProfile: function (success, error) {
      $http({
        method : 'DELETE',
        url : urls.API_ENDPOINT + '/users/' + tokenClaims.sub,
        cache : false
      }).success(success).error(error);
    },
    login: function (data, success, error) {
      $http.post(urls.API_ENDPOINT + '/users/token', data).success(success).error(error);
    },
    getUserData: function(success, error) {
      tokenClaims = getClaimsFromToken();
      $http.get(urls.API_ENDPOINT + '/users/view/' + tokenClaims.sub).success(function(res) {
        User.username     = res.data.username;
        User.is_valid_pro = res.data.is_valid_pro;
        User.email        = res.data.email;

        success(res);
      }).error(error);
    },
    logout: function (success) {
      tokenClaims = {};
      User.username     = '';
      User.is_valid_pro = false;
      User.email        = '';

      delete $localStorage.token;
      success();
    },
    passwordRequest: function (data, success, error) {
      $http.post(urls.API_ENDPOINT + '/users/password_request', data).success(success).error(error);
    },
    setNewPassword: function (data, success, error) {
      $http.post(urls.API_ENDPOINT + '/users/password_reset', data).success(success).error(error);
    },
    optin: function (data, success, error) {
      $http.post(urls.API_ENDPOINT + '/users/optin', data).success(success).error(error);
    },
    getTokenClaims: function () {
      return getClaimsFromToken();
    },
    getBtClientToken: function(success, error) {
      $http.get(urls.API_ENDPOINT + '/braintree/get_client_token').success(success).error(error);
    },
    sendPaymentMethodNonceToServer : function(data, success, error) {
      $http.post(urls.API_ENDPOINT + '/braintree/payment_method_nonce_received', data).success(success).error(error);
    }
  };
}
)
.factory('optinService', function ($location, Auth, $route, $localStorage, Flash) {
    return function () {
        var routeParams = $route.current.params;
        if(typeof routeParams.id != 'undefined' && typeof routeParams.hash != 'undefined') {
          var formData = {
            id   : routeParams.id,
            hash : routeParams.hash
          };

          Auth.optin(formData, function(res) {
            if(res.success) {
              $localStorage.token = res.data.token;
              Flash.create('success', '<strong>Success!</strong> ' + res.data.message, 0);
              $location.path('/welcome');
            } else {
              Flash.create('warning', '<strong>Mhh...</strong> ' + res.data.message, 0);
              $location.path('/login');
            }
          }, function (res) {
            Flash.create('warning', '<strong>Mhh...</strong> ' + res.data.message, 0);
            $location.path('/login');
          });
        } else {
          Flash.create('warning', '<strong>Mhh...</strong> stuff is missing.', 0);
          $location.path('/login');
        }
    };
});

