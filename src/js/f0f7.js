angular.module('f0f7App', [
  'ngRoute',
  'ngMidi',
  'dndLists',
  'ngStorage',
  'f0f7.sysexService',
  'f0f7.controllers',
  'f0f7.pagesController',
  'f0f7.proController',
  'f0f7.usersController',
  'f0f7.devices',
  'f0f7.restService',
  'f0f7.directives',
  'ngFlash',
  'angular-md5',
  'xeditable',
  'angular-loading-bar',
  'ngAnimate',
  'cfp.hotkeys',
  'ngSanitize',
  'ui.bootstrap.position',
  'mwl.confirm',
  'eloquent'
])
.constant('config', {
  accounts : {
    pro : {
      price : 7.90,
      bankmanager : {
        limit : 100
      }
    },
    standard : {
      bankmanager : {
        limit : 3
      }
    }
  }
 })
.value('User', {})
.config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {

    $routeProvider.
      when('/SysexLibrarian', {
        templateUrl: 'partials/SysexLibrarian.html',
        controller: 'SysexController',
      }).
      when('/SysexLibrarian/:deviceId', {
        templateUrl: 'partials/SysexLibrarian.html',
        controller: 'SysexController',
      }).
      when('/imprint', {
        templateUrl: 'partials/imprint.html',
        controller: 'PagesController',
      }).
      when('/privacy', {
        templateUrl: 'partials/privacy.html',
        controller: 'PagesController',
      }).
      when('/about', {
        templateUrl: 'partials/about.html',
        controller: 'PagesController',
      }).
      when('/pro', {
        templateUrl: 'partials/pro/index.html',
        controller: 'ProController',
      }).
      when('/thank_you_for_supporting_us', {
        templateUrl: 'partials/pro/thank_you_for_supporting_us.html',
        controller: 'PagesController',
      }).
      when('/login', {
        templateUrl: 'partials/users/login.html',
        controller: 'UsersController',
      }).
      when('/register', {
        templateUrl: 'partials/users/register.html',
        controller: 'UsersController',
      }).
      when('/welcome', {
        templateUrl: 'partials/users/welcome.html',
        controller: 'UsersController',
      }).
      when('/profile', {
        templateUrl: 'partials/users/profile.html',
        controller: 'UsersController',
      }).
      when('/password_request', {
        templateUrl: 'partials/users/password_request.html',
        controller: 'UsersController',
      }).
      when('/set_a_new_password', {
        templateUrl: 'partials/users/set_a_new_password.html',
        controller: 'UsersController',
      }).
      when('/optin', {
        resolve: {
          optin: ['optinService', function (optinService) {
            optinService();
          }]
        }
      }).
      otherwise({
        redirectTo: '/SysexLibrarian'
      });

      $httpProvider.interceptors.push(['$q', '$location', '$localStorage', function ($q, $location, $localStorage) {
         return {
             'request': function (config) {
                 config.headers = config.headers || {};
                 if ($localStorage.token) {
                     config.headers.Authorization = 'Bearer ' + $localStorage.token;
                 }
                 config.headers.Accept = 'application/json';
                 config.headers['Content-Type'] = 'application/json';
                 return config;
             },
             'responseError': function (response) {
                 if (response.status === 401 || response.status === 403) {
                     $location.path('/login');
                 }
                 return $q.reject(response);
             }
         };
      }]);

  }])
.run(['editableOptions', 'editableThemes', '$localStorage', 'Auth', function(editableOptions, editableThemes, $localStorage, Auth) {
  editableThemes.bs3.inputClass = 'input-sm';
  editableThemes.bs3.buttonsClass = 'btn-sm';
  editableOptions.theme = 'bs3';

  if($localStorage.token) {
    Auth.getUserData(function(res) {
      // console.log(res);
    }, function(res) {
      // console.log(res);
    });
  }
}]);

angular.module('ngMidi', [])
.factory('WebMIDI', ['$window', '$q', '$localStorage', '$log', function($window, $q, $localStorage, $log) {
    function _test() {
        return ($window.navigator && $window.navigator.requestMIDIAccess) ? true : false;
    }

    function _connect() {
        var defer = $q.defer();
        var p = defer.promise;

        if(_test()) {
            defer.resolve($window.navigator.requestMIDIAccess({sysex: true}));
        } else {
            defer.reject(new Error('No Web MIDI support'));
        }
        return p;
    }

    return {
        connect: function() {
            return _connect();
        },
        midiAccess : {},
        midiIn : {},
        midiOut : {},
        checkMIDIImplementation : function() {
            if ((typeof(this.midiAccess.inputs) == "function")) {
                $log.log('Old Skool MIDI inputs() code not supported');
            } else {
                $log.log('new MIDIMap implementation');
            }
        },
        selectFirstAvailableInPort : function(success, error) {
            var inputs = this.midiAccess.inputs.values();
            var port = inputs.next();
            if(typeof port.value != 'undefined') {
                success(port);
            } else {
                error();
            }
        },
        selectFirstAvailableOutPort : function(success, error) {
            var outputs = this.midiAccess.outputs.values();
            var port = outputs.next();
            if(typeof port.value != 'undefined') {
                success(port);
            } else {
                error();
            }
        },
        selectInPort : function (id, success, error) {
            var newPort = this.midiAccess.inputs.get(id);
            if ( !newPort ) {
                error();
            } else {
                if(this.midiIn) {
                    this.midiIn.onmidimessage = false; //disable existing callback
                }
                this.midiIn = newPort;
                $log.log("switched to MIDI In Port " + this.midiIn.manufacturer + " " + this.midiIn.name);
                $localStorage.midiInPortId = id;
                 this.midiIn.onmidimessage = function(ev) {
                     success(ev);
                 };
                 return true;
            }
        },
        selectOutPort : function (id, error) {
            var newPort = this.midiAccess.outputs.get(id);
            if ( !newPort ) {
                error();
            } else {
                this.midiOut = newPort;
                $log.log("switched to MIDI Out Port " + this.midiOut.manufacturer + " " + this.midiOut.name);
                $localStorage.midiOutPortId = id;
                return true;
            }
        },
        getAvailableInPorts : function() {
            var availableMIDIInputs = [];
            var inputs=this.midiAccess.inputs.values();
            for ( var input = inputs.next(); input && !input.done; input = inputs.next()) {
                availableMIDIInputs.push({id : input.value.id, name : input.value.manufacturer + ' ' + input.value.name});
            }

            if(availableMIDIInputs.length === 0) {
                availableMIDIInputs = [{name : 'no MIDI in'}];
            }
            return availableMIDIInputs;
        },
        getAvailableOutPorts : function() {
            var availableMIDIOutputs = [];
            var outputs = this.midiAccess.outputs.values();
            for ( var output = outputs.next(); output && !output.done; output = outputs.next()) {
                availableMIDIOutputs.push({id : output.value.id, name : output.value.manufacturer + ' ' + output.value.name});
            }

            if(availableMIDIOutputs.length === 0) {
                availableMIDIOutputs = [{name : 'no MIDI out'}];
            }
            return availableMIDIOutputs;
        },
    };
}]);

angular.module('f0f7.controllers', [])
.controller('SysexController', ['$scope', '$log', '$timeout', 'WebMIDI', 'Sysex', 'Devices', 'RestService', '$localStorage', 'Auth', 'Flash', '$location', '$routeParams', 'config', '$q', 'hotkeys', 'User', 'Eloquent', function($scope, $log, $timeout, WebMIDI, Sysex, Devices, RestService, $localStorage, Auth, Flash, $location, $routeParams, config, $q, hotkeys, User, Eloquent){

	var device = {};
	var progressTerminatorTimer;
	var selectProgramTimer;
	var suggestedBankName = '';

	// Model to JSON for demo purpose
	// $scope.$watch('models', function(models) {
	//     $scope.modelAsJson = angular.toJson(models, true);
	// }, true);

	$scope.requestMidiAccess = function() {
		$localStorage.userHasRequestedMidi = true;
		initWebMIDI();
	};

	function initWebMIDI() {
		if($localStorage.userHasRequestedMidi === true) {
			WebMIDI.connect().then(onMIDIStarted, onMIDISystemError);
		}
	}
	initWebMIDI();

	function preCheckProgramInList() {
		var prgArr = $scope.models.lists[$scope.models.listInFocus];
		var elementPos = -1;
		if(typeof $scope.models.selected !== 'undefined') {
			if(typeof $scope.models.selected.$$hashKey !== 'undefined') {
				elementPos = prgArr.map(function(x) {return x.$$hashKey; }).indexOf($scope.models.selected.$$hashKey);
			}
		}
		if(elementPos >= (prgArr.length)) {
			elementPos = -1;
		}
		return elementPos;
	}

	hotkeys.bindTo($scope)
	.add({
		combo: 'down',
		description: 'navigate down through program list',
		callback: function() {
			var elementPos = preCheckProgramInList();
			if(typeof $scope.models.lists[$scope.models.listInFocus][elementPos+1] === 'undefined') {
				elementPos = -1;
			}
			var program = $scope.models.lists[$scope.models.listInFocus][elementPos+1];
			$scope.selectProgram(program, $scope.models.listInFocus);
		}
	})
	.add({
		combo: 'up',
		description: 'navigate up through program list',
		callback: function() {
			var elementPos = preCheckProgramInList();
			if(typeof $scope.models.lists[$scope.models.listInFocus][elementPos-1] === 'undefined') {
				elementPos = $scope.models.lists[$scope.models.listInFocus].length;
			}
			var program = $scope.models.lists[$scope.models.listInFocus][elementPos-1];
			$scope.selectProgram(program, $scope.models.listInFocus);
		}
	})
	.add({
		combo: 'left',
		description: 'focus on Masterboard',
		callback: function() {
			if($scope.models.lists.A.length > 0) {
				var elementPos = preCheckProgramInList();
				$scope.models.listInFocus = 'A';
				if(typeof $scope.models.lists[$scope.models.listInFocus][elementPos] === 'undefined') {
					elementPos = 0;
				}
				var program = $scope.models.lists[$scope.models.listInFocus][elementPos];
				$scope.selectProgram(program, $scope.models.listInFocus);
			}
		}
	})
	.add({
		combo: 'right',
		description: 'focus on Clipboard',
		callback: function() {
			if($scope.models.lists.B.length > 0) {
				var elementPos = preCheckProgramInList();
				$scope.models.listInFocus = 'B';
				if(typeof $scope.models.lists[$scope.models.listInFocus][elementPos] === 'undefined') {
					elementPos = 0;
				}
				var program = $scope.models.lists[$scope.models.listInFocus][elementPos];
				$scope.selectProgram(program, $scope.models.listInFocus);
			}
		}
	})
	.add({
		combo: 'backspace',
		description: 'remove program from list',
		callback: function(e) {
			e.preventDefault();
			var elementPos = preCheckProgramInList();
			if(typeof $scope.models.lists[$scope.models.listInFocus][elementPos] !== 'undefined') {
				$scope.models.lists[$scope.models.listInFocus].splice(elementPos, 1);
				if(typeof $scope.models.lists[$scope.models.listInFocus][elementPos] !== 'undefined') {
					var program = $scope.models.lists[$scope.models.listInFocus][elementPos];
					$scope.selectProgram(program, $scope.models.listInFocus);
				}
			}
		}
	})
	.add({
		combo: 'space',
		description: 'move program to other list',
		callback: function(e) {
			e.preventDefault();
			var elementPos = preCheckProgramInList();
			if(typeof $scope.models.lists[$scope.models.listInFocus][elementPos] !== 'undefined') {
				if($scope.models.listInFocus == 'A') {
					$scope.models.lists.B.push($scope.models.lists[$scope.models.listInFocus][elementPos]);
				} else {
					$scope.models.lists.A.push($scope.models.lists[$scope.models.listInFocus][elementPos]);
				}
				$scope.models.lists[$scope.models.listInFocus].splice(elementPos, 1);
				if(typeof $scope.models.lists[$scope.models.listInFocus][elementPos] !== 'undefined') {
					var program = $scope.models.lists[$scope.models.listInFocus][elementPos];
					$scope.selectProgram(program, $scope.models.listInFocus);
				}
			}
		}
	});

	$scope.getBankmanagerData = function() {
		$scope.getPrivateBankmanagerData();
		$scope.getPublicBankmanagerData();
	};

	$scope.getPrivateBankmanagerData = function(page, selectId) {
		if(typeof $scope.tokenClaims.sub != 'undefined') {
			page = typeof page == 'undefined' ? 1 : page;
			selectId = typeof selectId == 'undefined' ? null : selectId;
			RestService.listPrivateBanks({ngclass : device.id, page : page}, function(banks) {
				$scope.bankmanager.private = banks;
				$scope.bankmanager.private.selected = selectId;
				if($scope.user.is_valid_pro === true) { //todo: listen on user change
					$scope.bankmanager.limit = config.accounts.pro.bankmanager.limit;
				}
			}, httpError);
		}
	};

	$scope.getPublicBankmanagerData = function(page) {
		page = typeof page == 'undefined' ? 1 : page;
		RestService.listPublicBanks({ngclass : device.id, page : page}, function(banks) {
			$scope.bankmanager.public = banks;
			$scope.bankmanager.public.selected = null;
		}, httpError);
	};

	$scope.togglePublishBank = function(index, bank) {
		$scope.bankmanager.private.data[index].is_public = bank.is_public == 1 ? 0 : 1;
		var data = {
			id : bank.id,
			is_public : $scope.bankmanager.private.data[index].is_public
		};

		RestService.editBank(data, $scope.getPublicBankmanagerData, httpError);
	};

	$scope.deleteBank = function(bank) {
		RestService.deleteBank(bank.id, $scope.getBankmanagerData, httpError);
	};

	$scope.editBankName = function(data, bankId) {
		var d = $q.defer();
		RestService.editBank({id : bankId, name : data}, function() {
			d.resolve();
			$scope.getPublicBankmanagerData();
		}, function(res) {
			var errorObj = res.data.errors.name;
			d.resolve(errorObj[Object.keys(errorObj)[0]]);
		});
		return d.promise;
	};

	$scope.loadBank = function(bank, patchListId) {
		if(patchListId === 'A') {
			$scope.bankmanager.private.selected = bank.id;
		} else {
			$scope.bankmanager.public.selected = bank.id;
		}

		RestService.getBank(bank.id, function(res) {
			var programs = [];
			for(var i = 0; i < res.data.programs.length; i++) {
				programs[i] = {
					name :res.data.programs[i].name.trim(),
					dna : res.data.programs[i].dna,
					data : angular.fromJson(res.data.programs[i].data),
				};
			}
			$scope.models.lists[patchListId] = programs;
		}, httpError);
	};

	$scope.changeDevice = function() {
		device = Devices.setDevice($scope.selectedDevice);
		if(typeof device.id == 'undefined') {
			$location.path('/');
			return;
		}
		$log.log(device.manuf + ' ' + device.model);

		getAndSetBanks();

		$scope.models.lists.A = [];
		$scope.models.lists.B = [];
		Flash.clear();

		$scope.getBankmanagerData();
	};

	$scope.go = function() {
		$location.path('/SysexLibrarian/' + $scope.selectedDevice);
	};

	$scope.setDeviceFromRouteParams = function() {
		if(typeof $routeParams.deviceId != 'undefined') {
			$scope.selectedDevice = $routeParams.deviceId;
			$scope.changeDevice();
		}
		$log.log($routeParams.deviceId);
	};

	$scope.selectProgram = function(program, patchListId) {
		$scope.models.listInFocus = patchListId;
		$scope.models.selected = program;

		if(selectProgramTimer) {
			$log.log("Someone is selecting programs like mad.");
			$timeout.cancel(selectProgramTimer);
		}

		selectProgramTimer = $timeout(function() {
			$log.log("O.k. now I am sending the MIDIs");
			$log.log('program ' , program.name);

			if(WebMIDI.midiOut.state == 'connected') {
				device.sendToBuffer(program);
				$scope.logger = Sysex.params.logData;
				Sysex.params.logData = '';
				
			} else {
				alertMessage('No MIDI out connected.');
				return;
			}

		}, 300);
	};

	$scope.editProgramName = function(patchListId, newName, programId) {
		var maxNameLength = device.getMaxNameLength();
		if(newName == "RaND") {
			newName = Eloquent.phrase(maxNameLength);
		} else {
			if(newName.length > maxNameLength) {
				return "No more than " + maxNameLength + " characters.";
			}
			if(!/^[\x00-\x7F]*$/.test(newName)) {
				return "You cannot use special chars.";
			}
		}
		$scope.models.lists[patchListId][programId] = device.renameProgram(newName, $scope.models.lists[patchListId][programId]);
		$scope.logger = Sysex.params.logData;
	};

	$scope.swapLists = function() {
		var tempList = $scope.models.lists.A;
		$scope.models.lists.A = $scope.models.lists.B;
		$scope.models.lists.B = tempList;
	};

	$scope.clearList = function(patchListId) {
		$scope.models.lists[patchListId] = [];
	};

	$scope.retrieveBank = function(patchListId) {
		if(WebMIDI.midiOut.state == 'connected') {
			$scope.models.lists.A = [];
			if($scope.amountOfBanks > 1) {
				suggestedBankName = $scope.banks[$scope.selectedBank].label;
			}
			$scope.progressbar.percentage = 1;
			device.retrieveBank($scope.banks[$scope.selectedBank].address);
		} else {
			alertMessage('No MIDI out connected.');
		}
	};

	$scope.sendBank = function(patchListId) {

		Sysex.params.logData = '';

		var programList = $scope.models.lists[patchListId];
		var qtyOfProgramsToTransmit = programList.length;
		var bank = $scope.banks[$scope.selectedBank].address;
		var prgNo = 0;
		var program;
		var i = 0;

		if(qtyOfProgramsToTransmit > device.params.patchesPerBank) {
			qtyOfProgramsToTransmit = device.params.patchesPerBank;
		}

		var delayedProgramSend = function(prgNo) {
			$log.log(prgNo + ' / ' + qtyOfProgramsToTransmit);

			program = programList[prgNo];
			if(typeof program == 'undefined') {
				$log.error('Dump Error');
				$scope.progressbar.percentage = 0;
			}
			device.sendProgramToBank(program, bank, prgNo);

			$scope.progressbar.percentage = Math.round((prgNo+1) * 100 / qtyOfProgramsToTransmit);

			$scope.logger = Sysex.params.logData;
			Sysex.params.logData = '';

			if(prgNo < (qtyOfProgramsToTransmit-1)) {
				prgNo++;
				$timeout(delayedProgramSend, device.params.transferPause, true, prgNo);
			} else {
				$scope.progressbar.percentage = 100;
				$timeout(function() {$scope.progressbar.percentage = 0;}, 500, true);
				alertMessage('All programs transferred.', 'success');
			}
		};

		delayedProgramSend(prgNo);
	};

	$scope.sendProgram = function(program, prgNo) {
		var bank = $scope.banks[$scope.selectedBank].address;
		device.sendProgramToBank(program, bank, prgNo);
		$scope.logger = Sysex.params.logData;
		Sysex.params.logData = '';
		alertMessage('Programs transferred.', 'success');
	};

	$scope.downloadBank = function(patchListId) {
		var programList = $scope.models.lists[patchListId];
		var amountOfPatchesInList = programList.length;
		var bank = $scope.banks[$scope.selectedBank].address;
		var prgNo = 0;
		var blobData = [];

		for(prgNo = 0; prgNo < amountOfPatchesInList; prgNo++ ) {
			blobData = blobData.concat(device.reorderProgram(programList[prgNo], bank, prgNo));
		}

		var arrayUint8 = Sysex.convertToUint8(blobData);
		var downloadFile =  new Blob([arrayUint8], {type: "application/octet-stream"});
		saveAs(downloadFile, "f0f7_" + device.model + "_bank.syx");
	};

	$scope.saveBank = function(patchListId) {
		var bankName;
		if(suggestedBankName.length > 0) {
			bankName = suggestedBankName;
			suggestedBankName = '';
		} else {
			bankName = 'My ' + device.model + ' Bank';
		}
		RestService.saveBank({
			bank : {
				name : bankName,
				is_public : 0,
				ngclass : device.id,
			},
			programs : $scope.models.lists[patchListId]
		}, function(res) {
			$scope.getPrivateBankmanagerData(1, res.data.id);
			alertMessage('Bank saved!', 'success');
		}, httpError);
	};

	$scope.updateProgramsInBank = function() {
		RestService.updateProgramsInBank({
			bank : {
				id : $scope.bankmanager.private.selected,
				ngclass : device.id,
			},
			programs : $scope.models.lists.A
		}, function(res) {
			if(res.success === true) {
				alertMessage('Bank updated!', 'success');
			} else {
				alertMessage('An error occured while updating the Bank.', 'danger');
			}
		}, httpError);
	};

	$scope.processSysexFile = function(event) {
		var input = event.target;

		var reader = new FileReader();
		if(typeof input.files[0].name != 'undefined') {
			suggestedBankName = input.files[0].name;
		}

		reader.onloadend = function(){
			var raw = Sysex.decodeDataURI(reader.result);
			var md = [];
			var byte;
			var i = 0;
			var thisIsSysExData = false;

			if(raw.charCodeAt(0) != 0xF0) {
				alertMessage('Caution, this file may contain invalid Sysex data. LASER Mammoth will still try to process what is there.');
				// return;
			} else {
				Flash.clear();
			}

			for(i = 0; i < raw.length; i++) {
				byte = raw.charCodeAt(i);
				if(byte == 0xF0) {
					thisIsSysExData = true;
				}
				if(thisIsSysExData === true) {
					md.push(byte);
				}
				if(byte == 0xF7) {
					md = Sysex.convertToUint8(md); //important for dna
					extractPatchNames(md, 'B');
					md = [];
					thisIsSysExData = false;
				}
			}
		};
		reader.readAsDataURL(input.files[0]);
	};

	$scope.safeApply = function(fn) {
		var phase = this.$root.$$phase;
		if(phase == '$apply' || phase == '$digest') {
			if(fn && (typeof(fn) === 'function')) {
				fn();
			}
		} else {
			this.$apply(fn);
		}
	};

	function onMIDIStarted(midiAccess) {
		$scope.midiAvailable = true;
		WebMIDI.midiAccess = midiAccess;

		function selectFirstAvailableInPort() {
			$log.log('selectFirstAvailableInPort');
			return WebMIDI.selectFirstAvailableInPort(function(port) {
				$log.log('first inport is ' + port.value.name);
				$scope.midiInPort.id = port.value.id;
				$scope.changeMIDIInPort();
			}, function() {
				$scope.midiInPort.id = null;
				$log.log("no midiInPort available");
			});
		}

		function selectFirstAvailableOutPort() {
			$log.log('selectFirstAvailableOutPort');
			return WebMIDI.selectFirstAvailableOutPort(function(port) {
				$log.log('first outport is ' + port.value.name);
				$scope.midiOutPort.id = port.value.id;
				$scope.changeMIDIOutPort();
			}, function() {
				$scope.midiOutPort.id = null;
				$log.log("no midiOutPort available");
			});
		}

		function getAndSelectPorts() {
			$scope.midiInPort.availableOptions = WebMIDI.getAvailableInPorts();
			$scope.changeMIDIInPort();
			$scope.midiOutPort.availableOptions = WebMIDI.getAvailableOutPorts();
			$scope.changeMIDIOutPort();
		}

		$scope.changeMIDIInPort = function() {
			WebMIDI.selectInPort($scope.midiInPort.id, midiMessageReceived, selectFirstAvailableInPort);
		};

		$scope.changeMIDIOutPort = function() {
			WebMIDI.selectOutPort($scope.midiOutPort.id, selectFirstAvailableOutPort);
		};

		$scope.midiInPort = {};
		$scope.midiOutPort = {};

		if($localStorage.midiInPortId) {
			$scope.midiInPort.id = $localStorage.midiInPortId;
		}
		if($localStorage.midiOutPortId) {
			$scope.midiOutPort.id = $localStorage.midiOutPortId;
		}

		getAndSelectPorts();

		WebMIDI.midiAccess.onstatechange = function(e) {
			var msg, msgClass;
			if(e.port) {
				msg = e.port.manufacturer + ' ' + e.port.name + ' ' + e.port.state; 
				msgClass = e.port.state == 'connected' ? 'success' : 'warning';
 			} else {
 				msg = 'Interface changed';
 			}
 			getAndSelectPorts();
			alertMessage(msg, msgClass);
		};
	}

	function onMIDISystemError( msg ) {
		msg = msg + '<hr/><p>Currently only <a href="https://www.google.com/chrome/">Chrome</a> and <a href="http://www.opera.com">Opera</a> support the Web MIDI API which is neccessary to run LASER Mammoth.<br/>If you have blocked the access accidently you can unblock it in your browser preferences, e.g. copy/paste this in your browser addressbar:<br/>chrome://settings/contentExceptions#midi-sysex</p>';
		$log.log( "Error encountered:" + msg );
		alertMessage(msg, 'danger', 0);
	}

	function midiMessageReceived(ev) {
		$log.info("midi Message Received on " + ev.target.name);
		midiMonitor(ev.data);
		extractPatchNames(ev.data, 'A');
	}

	function extractPatchNames(md, patchListId) {
		if(md[0] == 0xF0) {
			if (typeof device.id == 'undefined') {
				device = Devices.detect(md);
				if (typeof device.id == 'undefined') {
					alertMessage('Sysex device is not supported.');
					return;
				}
				$scope.selectedDevice = device.id;
				$scope.changeDevice();

			} else {
				var tmpDevice = Devices.detect(md);
				if(tmpDevice.id != device.id) {
					alertMessage('Received Sysex dump from ' + tmpDevice.model + ' but ' + device.model + ' is already set as device.');
					return;
				}
			}

			var patches = device.extractPatchNames(md);
			if(patches.length > 0) {
				$scope.$apply(function() {

					//more programs than space in bank? clear list.
					// if($scope.models.lists[patchListId].length >= device.params.patchesPerBank) {
					// 	$scope.models.lists[patchListId] = [];
					// }

					for(var i=0; i<patches.length; i++) {
						$scope.models.lists[patchListId].push(patches[i]);
					}

					//trigger progressbar
					var programsInList = $scope.models.lists[patchListId].length;
					$scope.progressbar.percentage = Math.ceil((programsInList+1) * 100 / device.params.patchesPerBank);

					if(progressTerminatorTimer) {
						$timeout.cancel(progressTerminatorTimer);
					}
					progressTerminatorTimer = $timeout(function() {
						$log.log("no more incoming events? I end progressbar.");
						finishProgresssbar(); 
						alertMessage("I expected a different amount of programs.", 'warning');
					}, 5000);

					if(programsInList == device.params.patchesPerBank) {
						$timeout.cancel(progressTerminatorTimer);
						finishProgresssbar();
					}

					$scope.logger = Sysex.params.logData;
				});
			}
		}
	}

	function finishProgresssbar() {
		$scope.progressbar.percentage = 100;
		$timeout(function() {$scope.progressbar.percentage = 0;}, 500, true);
		Flash.clear();
	}

	function alertMessage(message, msgClass, flashTimer) {
		Flash.clear();
		if(typeof msgClass == 'undefined') {
			msgClass = 'warning';
		}
		if(typeof flashTimer == 'undefined') {
			flashTimer = 10000;
		}
		$scope.safeApply(function() {
			Flash.create(msgClass, message, flashTimer, {class: 'alert-top'});
		});
	}

	function getAndSetBanks() {
		if(typeof device.params != 'undefined') {
			$scope.banks = device.params.banks;
			$scope.selectedBank = Object.keys($scope.banks)[0];

			$scope.patchesPerBank = device.params.patchesPerBank;
			$scope.programsCanBeRenamed = typeof device.params.parserConfig.name !== 'undefined';
			$scope.singleProgramsCannotBeTransmitted = typeof device.params.singleProgramsCannotBeTransmitted !== 'undefined';
			$scope.deviceModel = device.model;
			$scope.deviceHints = device.hints;

			$scope.amountOfBanks = Object.keys($scope.banks).length;
		} else {
			initScope();
		}
	}

	function midiMonitor(midiData) {
		// var logData = Array.from(midiData).splice(0, 24);
		// for(var i=0; i<logData.length; i++) {
		// 	logData[i] = Sysex.byteToHex(logData[i]);
		// }
		// var logStr = logData.join(' ');
		// alertMessage("Incoming MIDI data: " + logStr +'...', 'info');
		
		$scope.safeApply(function() {
			$scope.shootLasers = true;
			$timeout(function() {
				$scope.shootLasers = false;
			}, 300, true);
		});

	}

	function httpError(res) {
		var message = ((res || {}).data || {}).message || {};
		if(typeof message == 'object') {
			message = res.message;
		}
		if(message == 'Expired token') {
			Auth.logout(function () {
				$location.path('/login');
			});
		}
		alertMessage(message, 'danger');
	}

	function initScope() {
		$scope.token = $localStorage.token;
		$scope.tokenClaims = Auth.getTokenClaims();

		$scope.availableDevices = Devices.getOptions();
		$scope.selectedDevice = null;

		$scope.banks = {};
		$scope.selectedBank = null;

		$scope.patchesPerBank = null;
		$scope.programsCanBeRenamed = false;
		$scope.singleProgramsCannotBeTransmitted = false;
		$scope.deviceModel = 'unknown';
		$scope.deviceHints = [];
		
		$scope.amountOfBanks = 0;

		$scope.models = {
		    selected: {},
		    listInFocus : 'A',
		    lists: {"A": [], "B": []}
		};

		$scope.progressbar = {
			percentage : 0
		};

		$scope.logger = '';

		$scope.bankmanager = {
			limit : config.accounts.standard.bankmanager.limit,
			public : {
				selected : null,
				data : {}
			},
			private : {
				selected : null,
				data : {}
			},
		};

		$scope.setDeviceFromRouteParams();

		$scope.user = User;
		// if(User.is_valid_pro === true) {
		// 	$scope.bankmanager.limit = config.accounts.pro.bankmanager.limit;
		// }

		$scope.showHelp = false;

		// $scope.banks = {
		// 	options : {},
		// 	selected : null,
		// 	qty : 0,
		// 	patches : 0
		// }

		// for (var i = 1; i <= 3; ++i) {
		//     $scope.models.lists.A.push({name: "Item A" + i, program : [10,11,12]});
		//     $scope.models.lists.B.push({name: "Item B" + i, program : [10,11,12]});
		// }
	}
	initScope();
	Flash.clear();
}])
.controller('NavController', ['$scope', '$location', '$localStorage', 'Auth', 'User', function ($scope, $location, $localStorage, Auth, User) {
		$scope.token = $localStorage.token;
		$scope.user = User;
		
		$scope.logout = function () {
		    Auth.logout(function () {
		    	$location.path('/login');
		    });
		};
}]);

angular.module('f0f7.pagesController', [])
.controller('PagesController', ['$scope', 'Flash', function ($scope, Flash) {
		Flash.clear();
	}]
);

angular.module('f0f7.proController', [])
.controller('ProController', ['$scope', 'Flash', '$localStorage', 'Auth', 'RestService', '$timeout', 'config', '$location', function ($scope, Flash, $localStorage, Auth, RestService, $timeout, config, $location) {

	$scope.token = $localStorage.token;
	$scope.accounts = config.accounts;

	if($localStorage.token) {
		Auth.getUserData(function(res) {
				$scope.user = res.data;

				Auth.getBtClientToken(function (res) {
					setupBraintree(res);
				}, httpError);

			}, 	httpError);
	}

	function setupBraintree(res) {
		$timeout(function() {
			var clientToken = res.data.clientToken;

			braintree.setup(clientToken, "dropin", {
				container: "dropin-container",
				paypal : {
					singleUse : true,
					amount	: config.accounts.pro.price,
					currency : 'EUR',
					button: {
						type: 'checkout'
					},
				},
				onPaymentMethodReceived: function (obj) {
					console.log('onPaymentMethodReceived');
					sendPaymentMethodNonceToServer(obj);
				},
				onReady: function () {
					console.log('Braintree is ready');
					$timeout(function() {
						$scope.braintreeIsReady = true;
					}, 300);
				}
			});
		}, 100);
	}

	function sendPaymentMethodNonceToServer(obj) {
		Auth.sendPaymentMethodNonceToServer(obj, function(res) {
			console.log('sendPaymentMethodNonceToServer: success');
			if(res.success === true) {
				Auth.getUserData(function() {
					$location.path('/thank_you_for_supporting_us');
				}, httpError);
			} else {
				console.log(res);
				Flash.create('danger', '<strong>Something went wrong.</strong>', 0);
			}
		}, httpError);
	}

	function httpError(res) {
		Flash.create('danger', '<strong>Error:</strong> ' + res.message);
	}

	Flash.clear();

}]);

angular.module('f0f7.usersController', [])
.controller('UsersController', ['$scope', '$location', '$localStorage', 'Auth', 'Flash', '$routeParams', function ($scope, $location, $localStorage, Auth, Flash, $routeParams) {

		$scope.successAlert = function (message) {
			Flash.clear();
		    var id = Flash.create('success', '<strong>Success!</strong> ' + message, 3000, {class: 'alert-top'}, true);
		    // First argument (string) is the type of the flash alert.
		    // Second argument (string) is the message displays in the flash alert (HTML is ok).
		    // Third argument (number, optional) is the duration of showing the flash. 0 to not automatically hide flash (user needs to click the cross on top-right corner).
		    // Fourth argument (object, optional) is the custom class and id to be added for the flash message created.
		    // Fifth argument (boolean, optional) is the visibility of close button for this flash.
		    // Returns the unique id of flash message that can be used to call Flash.dismiss(id); to dismiss the flash message.
		};

		$scope.dangerAlert = function (message) {
			Flash.clear();
		    Flash.create('danger', '<strong>Error!</strong> ' + message, 0, {class: 'alert-top'}, true);
		};

		$scope.warningAlert = function (message) {
			Flash.clear();
		    Flash.create('warning', '<strong>Oh no!</strong> ' + message, 0, {class: 'alert-top'}, true);
		};

		function successAuth(res) {
			$localStorage.token = res.data.token;

			Auth.getUserData(function(res) {
				$scope.successAlert('Welcome !');
				$location.path('/');
			}, function(res) {
				$scope.dangerAlert(res.data.message);
			});
		}

		$scope.login = function () {
			Auth.login($scope.Users, successAuth, function (res) {
				$scope.dangerAlert(res.data.message);
			});
		};

		$scope.signup = function () {
			Auth.signup($scope.Users, function(res) {
				if(res.success) {
					$scope.successAlert(res.data.message);
					$location.path('/login');
				} else {
					$scope.errors = res.data.errors;
					// $scope.warningAlert(res.data.message);
				}
			}, function (res) {
				$scope.dangerAlert(res.data.message);
			});
		};

		$scope.changeProfileData = function () {
			var formData = $scope.Users;
			if(typeof $scope.Users.password != 'undefined' && $scope.Users.password.length === 0) {
				delete formData.password;
			}
			Auth.editProfile(formData, function() {
				$scope.successAlert('Data successful changed');
			}, function (res) {
				$scope.dangerAlert(res.data.message);
				if(typeof res.data.errors != 'undefined') {
					$scope.errors = res.data.errors;
				}
			});
		};

		$scope.deleteProfile = function() {
			// if (confirm("No further notice! Your profile and all of your banks will be immediately deleted.")) {
				Auth.deleteProfile(function() {
					$scope.successAlert('Profile deleted');
					Auth.logout(function () {
						$location.path('/');
					});
				}, function (res) {
					$scope.dangerAlert(res.data.message);
				});
			// }
		};

		$scope.passwordRequest = function() {
			var formData = {
				username: $scope.username,
			};

			Auth.passwordRequest(formData, function(res) {
				if(res.success) {
					$scope.successAlert(res.data.message);
				} else {
					$scope.warningAlert(res.data.message);
				}
			}, function (res) {
				$scope.dangerAlert(res.data.message);
			});
		};
		$scope.setNewPassword = function() {
			if(typeof $routeParams.id != 'undefined' && typeof $routeParams.hash != 'undefined') {
				var formData = {
					id: $routeParams.id,
					hash: $routeParams.hash,
					password: $scope.password,
				};

				Auth.setNewPassword(formData, function(res) {
					if(res.success) {
						$scope.successAlert(res.data.message);
						$location.path('/login');
					} else {
						var message;
						var errorObj;
						if(typeof res.data.errors != 'undefined') {
							errorObj = res.data.errors.password;
							message = errorObj[Object.keys(errorObj)[0]];
						} else {
							message = res.data.message;
						}
						$scope.warningAlert(message);
					}
				}, function (res) {
					$scope.dangerAlert(res.data.message);
				});
			}
		};

		if($location.$$path == '/profile') {
			Auth.getUserData(function(res) {
				$scope.Users = res.data;
				// $scope.Users.password = '';
			}, function(res) {
				$scope.dangerAlert(res.data.message);
			});
		}

		if($location.$$path == '/set_a_new_password') {
			if($localStorage.token) {
				Auth.logout(function() {
					$scope.warningAlert('Logged you out because I can.');
				});
			}
		}
}]);

angular.module('f0f7.sysexService', [])
.factory('Sysex', ['$log', '$timeout', 'md5', function($log, $timeout, md5) {
	var obj = {
		params : {
			logData		: ''
		},
		extractPatchName : function(midiData, parserConfig) {
			var data = angular.fromJson(angular.toJson(midiData));
			var patchName = '';
            var char;

			for(var k=parserConfig.name.from; k<=parserConfig.name.to; k++) {
				if(typeof data[k] !== 'undefined') {
					if(data[k] < 32 || data[k] > 127) {
						char = 32;
					} else {
                        char = data[k];
                    }
					patchName += String.fromCharCode(char);
				}
			}

			var dna = this.createDNA(midiData, parserConfig.dna.from, parserConfig.dna.to);

			patchName = patchName.trim();
			if(patchName.length === 0) {
				patchName = 'no name';
			}
			return {name : patchName, dna : dna, data : data};
		},
		createDNA : function(midiData, from, to) {
			var dnaArray = midiData.slice(from, to);
			return md5.createHash(JSON.stringify(dnaArray) || '');
		},
		unpackMoogSysex : function(midiData, headerLength) {
			var resultData = [];
			var decodedByteCount = 0;  // Number of expanded bytes in result

			var cumulator = 0;
			var bitCount = 0;
			var patchName = '';

			for (var i = headerLength; // Skip header
				i < midiData.length - 1; // Omit EOF
				i++)
			{
				cumulator |= midiData[i] << bitCount;
				bitCount += 7;
				//console.log((i - 7) + ':' + byteToHex(midiData[i]) + ' (' + bitCount + ') ' + decimalToHex(cumulator) + '\n');
				if (bitCount >= 8)
				{
					var byte = cumulator & 0xFF;
					bitCount -= 8;
					cumulator >>= 8;
					resultData[decodedByteCount++] = byte;
					// console.log((i - 7) + ':' + byteToHex(midiData[i]) + ' (' + bitCount + ') ' + decimalToHex(cumulator) + ' > '  + byteToHex(byte) + '\n');

				}
			}

			return resultData;
		},
		packMoogSysex : function(midiData, header) {
			// var header = [0xF0, 0x04, 0x01, 0x00, 0x03, 0x00]; //Voyager Single Preset Dump. Device ID!
			// var header = [0xF0, 0x04, 0x01, 0x00, 0x02]; //Voyager Panel Dump. Device ID == 0x00

			var resultData = [];
			var packedByteCount = 0;
			var bitCount = 0;

			var thisByte;
			var packedByte;
			var nextByte = 0x0;


			for (var i = 0; i <= midiData.length; i++)
			{
				thisByte = midiData[i];
				packedByte = ((thisByte << bitCount) | nextByte) & 0x7F;
				nextByte = midiData[i] >> (7-bitCount);

				resultData[packedByteCount++] = packedByte;

				bitCount++;
				if(bitCount >= 7) {
					bitCount = 0;

					//Fill last byte
					packedByte = nextByte & 0x7F;
					resultData[packedByteCount++] = packedByte;
					nextByte = 0x0;
				}
			}

			resultData[packedByteCount++] = 0xF7;
			resultData = header.concat(resultData);

			return resultData;
		},
		unpackDSISysex : function(midiData) {

			midiData = midiData.concat([]); //Copy Array

			var unpackedData = [];
			var msb;
			var daveByte;
			var packets;
			var i;

			while (midiData.length > 0) {
				daveByte = midiData.splice(0, 1);
				packets = midiData.splice(0, 7);
				for(i=0; i<packets.length; i++) {
					msb = daveByte & 0x01;
					unpackedData.push(packets[i] | (msb << 7));
					daveByte = daveByte >> 1;
				}
			}
			return unpackedData;
		},
		packDSISysex : function(midiData) {

			midiData = midiData.concat([]); //Copy Array
			
			var packedData = [];
			var msb;
			var daveByte;
			var inputData;
			var i;

			while (midiData.length > 0) {
				inputData = midiData.splice(0, 7);
				daveByte = 0x00;
				for(i=0; i<inputData.length; i++) {
					msb = inputData[i] & 0x80;
					daveByte = daveByte | (msb >> (7-i));
				}
				packedData.push(daveByte);
				for(i=0; i<inputData.length; i++) {
					packedData.push(inputData[i] & 0x7F);
				}
			}
			return packedData;
		},
		sendSysex : function(midiData, outputPort) {
			this.dumpData(midiData, 16);
			outputPort.send( midiData, 0 ); // Data, timestamp
			// outputPort.send( midiData, window.performance.now() + 1000.0 ); // Data, timestamp
		},
		requestAllProgramsOfBank : function(bank, outputPort, header, patchesPerBank, transferPause) {
			var dumpRequest = [];
			var prgNo = 0;

			var delayedDumpRequest = function(prgNo) {
				$log.debug("Requesting Prg " + prgNo);
				dumpRequest = header.concat([bank, prgNo, 0xF7]);
				obj.sendSysex(dumpRequest, outputPort);

				if(prgNo < (patchesPerBank-1)) {
					prgNo++;
					$timeout(delayedDumpRequest, transferPause, true, prgNo);
				} else {
					$log.info("Bank dumpRequest completed");
				}
			};
			delayedDumpRequest(prgNo);
		},
		calculateRolandChecksum : function(midiData) {
			var sum = 0;
			for (var i = 0; i < midiData.length; i++) {
				sum += midiData[i];
			}
			sum &= 0x7f;
			if (sum !== 0) {
				sum = 0x80 - sum;
			}
			return sum;
		},
		calculateOberheimChecksum : function(midiData) {
			var sum = 0;
			for (var i = 0; i < midiData.length; i++) {
				sum += midiData[i];
			}
			sum &= 0x7f;
			return sum;
		},
		calculateAccessChecksum : function(midiData) {
			//Checksum (DeviceID + 10 + BankNumber + ProgramNumber + [256 single bytes]) AND 7F.
			var checksum = midiData[5] + 0x10 + midiData[7] + midiData[8];
			for(var i=9; i<=(midiData.length-3); i++) {
				checksum += midiData[i];
			}
			return checksum & 0x7F;
		},
        calculateWaldorfChecksum : function(midiData) {
            // Sum of all databytes truncated to 7 bits.
            // The addition is done in 8 bit format, the result is
            // masked to 7 bits (00h to 7Fh). A checksum of 7Fh is
            // always accepted as valid.
            // IMPORTANT: the MIDI status-bytes as well as the
            // ID's are not used for computing the checksum.

            var checksum = 0x00;
            for(var i=0; i<(midiData.length); i++) {
                checksum += midiData[i];
            }
            return checksum & 0x7F;
        },
		calculateStrymonChecksum : function(midiData) {
			var checksum = 0x00;
			for(var i=9; i<(midiData.length-2); i++) {
				checksum += 0x7F & midiData[i];
			}
			return 0x7F&checksum;
		},
		dumpData : function(data, lineLength) {
			this.appendToScreen("Found " + data.length + " bytes");

			var txt = '';
			for (var i = 0; i < data.length; i++) {
				var rd = data[i];
				if (rd > 31) {
					txt += String.fromCharCode(rd);
				} else {
					txt += '.';
				}

				this.appendToScreen(this.byteToHex(rd) + ' ', false);

				if ((i+1) % lineLength === 0) {
					this.appendToScreen(' ' + txt);
					txt = '';
				}
			}
			this.appendToScreen(' ' + txt);
		},
		nibbleToHex : function(halfByte) {
			return String.fromCharCode(halfByte < 10 ?
				halfByte + 48 : // 0 to 9
				halfByte + 55); // A to F
		},
		byteToHex : function(dec) {
			var h = (dec & 0xF0) >> 4;
			var l = dec & 0x0F;
			return this.nibbleToHex(h) + this.nibbleToHex(l);
		},
		decimalToHex : function(dec) {
			var result = '';
			do {
				result = this.byteToHex(dec & 0xFF) + result;
				dec >>= 8;
			} while (dec > 0);
			return result;
		},
		appendToScreen : function(msg, linebreak) {
			if(typeof linebreak == 'undefined') {
				linebreak =true;
			}
			this.params.logData += msg;
			if(linebreak === true) {
				this.params.logData += "\n";
			}
		},
		convertToUint8 : function(raw) {
			var rawLength = raw.length;
			var arrayUint8 = new Uint8Array(new ArrayBuffer(rawLength));
			for(var i=0; i < rawLength; i++) {
				// arrayUint8[i] = raw.charCodeAt(i);
				arrayUint8[i] = raw[i];
			}
			return arrayUint8;
		},
		objectToArray : function(obj) {
			var array = [];
			for (var prop in obj){
				if(obj.hasOwnProperty(prop)){
					array.push(obj[prop]);
				}
			}
			return array;
		},
		decodeDataURI : function(dataURI) {
			var BASE64_MARKER = ';base64,';
			var base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
			var base64 = dataURI.substring(base64Index);
			return window.atob(base64);
		},
		renameProgram : function(newName, nameLength, midiData, parserConfig) {
			var newNameArray = this.createPaddedNameArray(nameLength, newName);
			midiData = this.changeNameinMidiData(newNameArray, nameLength, midiData, parserConfig);
			return this.extractPatchName(midiData, parserConfig);
		},
		changeNameinMidiData : function(newNameArray, nameLength, midiData, parserConfig) {
			var args = [parserConfig.name.from, nameLength].concat(newNameArray);
			Array.prototype.splice.apply(midiData, args);
			return midiData;
		},
		createPaddedNameArray: function(nameLength, newName){
			var padding = Array(nameLength+1).join(' ');
			newName     = this.pad(padding,newName,false);

			return this.stringToAsciiByteArray(newName);
		},
		pad : function(pad, str, padLeft) {
		  if (typeof str === 'undefined') 
		    return pad;
		  if (padLeft) {
		    return (pad + str).slice(-pad.length);
		  } else {
		    return (str + pad).substring(0, pad.length);
		  }
		},
		stringToAsciiByteArray : function(str) {
			var bytes = [];
			for (var i = 0; i < str.length; ++i)
			{
				var charCode = str.charCodeAt(i);
		      if (charCode > 0xFF)  // char > 1 byte since charCodeAt returns the UTF-16 value
		      {
		      	throw new Error('Character ' + String.fromCharCode(charCode) + ' can\'t be represented by a US-ASCII byte.');
		      }
		      bytes.push(charCode);
		  }
		  return bytes;
		},
		denibble: function(midiData) {
			var denibbledData = [];
			for(var i=0;i<midiData.length; i=i+2) {
				var byte = (midiData[i+1] << 4) | midiData[i];
				denibbledData.push(byte);
			}
			return denibbledData;
		}
	};
	return obj;
}]);

angular.module('f0f7.devices', []).factory('Devices', 
	['Generic', 'AccessVirus', 'CasioCz1000', 'CasioCz5000', 'ClaviaNordlead', 'DSIEvolver', 'DSIMopho', 'DSIOb6', 'DSIPro2', 'DSIPro12', 'DSIProphet08', 'DSIProphetRev2', 'EnsoniqEsq1', 'IntellijelPlonk', 'IntellijelRainmaker', 'KorgDw8000', 'KorgMicrokorg', 'MoogVoyager', 'NovationBassstation2', 'OberheimMatrix6', 'OberheimOb8', 'RolandAlphajuno', 'RolandJd800', 'RolandJv80', 'SequentialProphet6', 'SequentialProphetX', 'StrymonTimeline', 'WaldorfBlofeld', 'WaldorfMicrowave', 'YamahaDx7', function(Generic, 
		AccessVirus,
		CasioCz1000,
        CasioCz5000,
		ClaviaNordlead,
		DSIEvolver,
		DSIMopho,
		DSIOb6,
		DSIPro2,
		DSIPro12,
		DSIProphet08,
		DSIProphetRev2,
		EnsoniqEsq1,
		// HypersynthXenophone,
		IntellijelPlonk,
		IntellijelRainmaker,
		KorgDw8000,
		KorgMicrokorg,
		MoogVoyager,
		NovationBassstation2,
		OberheimMatrix6,
		OberheimOb8,
		RolandAlphajuno,
		RolandJd800,
		RolandJv80,
		SequentialProphet6,
        SequentialProphetX,
		StrymonTimeline,
		WaldorfBlofeld,
        WaldorfMicrowave,
		YamahaDx7) {

		var deviceMap = {
			0xF0 : {
				0x00 : {
					0x00 : {
						0x2F : {
							0x04 : OberheimOb8 //Encore OB-8 
						}
						// 	0x21 : {
						// 		0x7F : {
						// 			0x03 : HypersynthXenophone
						// 		}
						// 	}
					},
					0x01 : {
						0x55 : {
							0x12: {
								0x01: StrymonTimeline,
								// 0x02: StrymonMobius
							}
						}
					},
					0x02 : {
						0x14 : {
							0x00 : IntellijelRainmaker,
							0x20 : IntellijelPlonk,
						}
					},
					0x20 : {
						0x29 : {
							0x00 : {
								0x33: NovationBassstation2
							}
						},
						0x33 : {
							0x01 : AccessVirus
						},
						// 0x3c : {
						// 	0x03 : device.elektron_monomachine
						// }
					},
				},
				0x01 : {
					0x20 : DSIEvolver,
					0x23 : DSIProphet08,
					0x25 : DSIMopho,
					0x2A : DSIPro12,
					0x2C : DSIPro2,
					0x2D : SequentialProphet6,
					0x2E : DSIOb6,
                    0x2F : DSIProphetRev2,
					0x30 : SequentialProphetX,
				},
				0x04 : {
					0x01 : MoogVoyager
				},
				0x0F : {
					0x02 : EnsoniqEsq1
				},
				0x10 : {
					0x01 : OberheimOb8,
					0x06 : OberheimMatrix6
				},
				0x33 : {
					0x04 : ClaviaNordlead
				},
				0x3E : {
					0x00 : WaldorfMicrowave,
                    0x13 : WaldorfBlofeld,
				},
				0x41 : {
					0x23 : RolandAlphajuno,
					0x3D : RolandJd800,
					0x46 : RolandJv80
				},
				0x42 : {
					0x03 : KorgDw8000,
					0x58 : KorgMicrokorg,
				},
				0x44 : {
					0x00 : CasioCz1000
				},
				0x43 : {
					0x00 : YamahaDx7
				}
			}
		};

		var deviceList = {
			'AccessVirus'          : AccessVirus,
			'CasioCz1000'          : CasioCz1000,
            'CasioCz5000'          : CasioCz5000,
			'ClaviaNordlead'       : ClaviaNordlead,
			'DSIEvolver'           : DSIEvolver,
			'DSIMopho'             : DSIMopho,
			'DSIOb6'               : DSIOb6,
			'DSIPro2'              : DSIPro2,
			'DSIPro12'             : DSIPro12,
			'DSIProphet08'         : DSIProphet08,
			'DSIProphetRev2'       : DSIProphetRev2,
			'EnsoniqEsq1'          : EnsoniqEsq1,
			// 'HypersynthXenophone'  : HypersynthXenophone,
			'IntellijelPlonk'  	   : IntellijelPlonk,
			'IntellijelRainmaker'  : IntellijelRainmaker,
			'KorgDw8000'  		   : KorgDw8000,
			'KorgMicrokorg'  	   : KorgMicrokorg,
			'MoogVoyager'          : MoogVoyager,
			'NovationBassstation2' : NovationBassstation2,
			'OberheimMatrix6'	   : OberheimMatrix6,
			'OberheimOb8'	       : OberheimOb8,
			'RolandAlphajuno'      : RolandAlphajuno,
			'RolandJd800'          : RolandJd800,
			'RolandJv80'           : RolandJv80,
            'SequentialProphet6'   : SequentialProphet6,
			'SequentialProphetX'   : SequentialProphetX,
			'StrymonTimeline'      : StrymonTimeline,
			'WaldorfBlofeld'       : WaldorfBlofeld,
            'WaldorfMicrowave'     : WaldorfMicrowave,
			'YamahaDx7'            : YamahaDx7,
		};

		return {
			detect : function(md) {
				var device = {};
				if(md[0] == 0xF0) {
					if(md[1] === 0x00) { //new school manufacturers
						if(typeof deviceMap[md[0]][md[1]][md[2]][md[3]][md[4]][md[5]] != 'undefined') {
							device = deviceMap[md[0]][md[1]][md[2]][md[3]][md[4]][md[5]];
							return device;
						}
						if(typeof deviceMap[md[0]][md[1]][md[2]][md[3]][md[4]] != 'undefined') {
							device = deviceMap[md[0]][md[1]][md[2]][md[3]][md[4]];
							return device;
						}
					} else { //old school manufacturers
						if(md[1] == 0x41 || md[1] == 0x33 || md[1] == 0x42) { //Roland, Korg and Clavia Special Case : skip device Id
							
							//Roland Alpha Juno Specialcase
							if(typeof deviceMap[md[0]][md[1]][md[4]] != 'undefined') {
								if(md[4] == 0x23) {
									device = deviceMap[md[0]][md[1]][md[4]];
									return device;
								}
							}

							if(typeof deviceMap[md[0]][md[1]][md[3]] != 'undefined') {
								device = deviceMap[md[0]][md[1]][md[3]];
								return device;
							}
						}

						if(typeof deviceMap[md[0]][md[1]] != 'undefined') {
							if(typeof deviceMap[md[0]][md[1]][md[2]] != 'undefined') {
								device = deviceMap[md[0]][md[1]][md[2]];
								return device;
							}
						}
					}
				}
			},
			getOptions : function() {
				var availableDevices = {};
				for (var prop in deviceList){
					if(deviceList.hasOwnProperty(prop)){
						availableDevices[prop] = deviceList[prop].manuf + ' ' + deviceList[prop].model;
					}
				}
				return availableDevices;
			},
			setDevice : function(device) {
				return typeof deviceList[device] != 'undefined' ? deviceList[device] : {};
			}
		};
	}]
);

angular.module('f0f7.restService', [])
.factory('RestService', ['$http', '$q', 'urls', '$log', function($http, $q, urls, $log) {
  return {
    listPrivateBanks: function(data, success, error) {
      $http({
        method : 'GET',
        url : urls.API_ENDPOINT + '/banks',
        params : data,
        cache : false
      }).success(success).error(error);
    },
    listPublicBanks: function(data, success, error) {
      $http({
        method : 'GET',
        url : urls.API_ENDPOINT + '/banks/public_index',
        params : data,
        cache : false
      }).success(success).error(error);
    },
    getBank: function(bankId, success, error) {
      $http({
        method : 'GET',
        url : urls.API_ENDPOINT + '/banks/' + bankId,
        cache : false
      }).success(success).error(error);
    },
    deleteBank: function(bankId, success, error) {
      $http({
        method : 'DELETE',
        url : urls.API_ENDPOINT + '/banks/' + bankId,
        cache : false
      }).success(success).error(error);
    },
    saveBank: function(data, success, error) {
      $http({
        method : 'POST',
        url : urls.API_ENDPOINT + '/banks/add',
        data : data,
        cache : false
      }).success(success).error(error);
    },
    updateProgramsInBank: function(data, success, error) {
      $http({
        method : 'PUT',
        url : urls.API_ENDPOINT + '/banks/update_programs/' +  data.bank.id,
        data : data,
        cache : false
      }).success(success).error(error);
    },
    editBank: function(data, success, error) {
      $http({
        method : 'PUT',
        url : urls.API_ENDPOINT + '/banks/' + data.id,
        data : data,
        cache : false
      }).success(success).error(error);
    },
  };
}])
.factory('Auth', ['$http', '$localStorage', 'urls', 'User', function ($http, $localStorage, urls, User) {
  function urlBase64Decode(str) {
    var output = str.replace('-', '+').replace('_', '/');
    switch (output.length % 4) {
      case 0:
      break;
      case 2:
      output += '==';
      break;
      case 3:
      output += '=';
      break;
      default:
      throw 'Illegal base64url string!';
    }
    return window.atob(output);
  }

  function getClaimsFromToken() {
    var token = $localStorage.token;
    var user = {};
    if (typeof token !== 'undefined') {
      var encoded = token.split('.')[1];
      user = JSON.parse(urlBase64Decode(encoded));
    }
    return user;
  }

  var tokenClaims = getClaimsFromToken();

  return {
    signup: function (data, success, error) {
      $http.post(urls.API_ENDPOINT + '/users/add', data).success(success).error(error);
    },
    editProfile: function (data, success, error) {
      $http.post(urls.API_ENDPOINT + '/users/edit/' + tokenClaims.sub, data).success(success).error(error);
    },
    deleteProfile: function (success, error) {
      $http({
        method : 'DELETE',
        url : urls.API_ENDPOINT + '/users/' + tokenClaims.sub,
        cache : false
      }).success(success).error(error);
    },
    login: function (data, success, error) {
      $http.post(urls.API_ENDPOINT + '/users/token', data).success(success).error(error);
    },
    getUserData: function(success, error) {
      tokenClaims = getClaimsFromToken();
      $http.get(urls.API_ENDPOINT + '/users/view/' + tokenClaims.sub).success(function(res) {
        User.username     = res.data.username;
        User.is_valid_pro = res.data.is_valid_pro;
        User.email        = res.data.email;

        success(res);
      }).error(error);
    },
    logout: function (success) {
      tokenClaims = {};
      User.username     = '';
      User.is_valid_pro = false;
      User.email        = '';

      delete $localStorage.token;
      success();
    },
    passwordRequest: function (data, success, error) {
      $http.post(urls.API_ENDPOINT + '/users/password_request', data).success(success).error(error);
    },
    setNewPassword: function (data, success, error) {
      $http.post(urls.API_ENDPOINT + '/users/password_reset', data).success(success).error(error);
    },
    optin: function (data, success, error) {
      $http.post(urls.API_ENDPOINT + '/users/optin', data).success(success).error(error);
    },
    getTokenClaims: function () {
      return getClaimsFromToken();
    },
    getBtClientToken: function(success, error) {
      $http.get(urls.API_ENDPOINT + '/braintree/get_client_token').success(success).error(error);
    },
    sendPaymentMethodNonceToServer : function(data, success, error) {
      $http.post(urls.API_ENDPOINT + '/braintree/payment_method_nonce_received', data).success(success).error(error);
    }
  };
}]
)
.factory('optinService', ['$location', 'Auth', '$route', '$localStorage', 'Flash', function ($location, Auth, $route, $localStorage, Flash) {
    return function () {
        var routeParams = $route.current.params;
        if(typeof routeParams.id != 'undefined' && typeof routeParams.hash != 'undefined') {
          var formData = {
            id   : routeParams.id,
            hash : routeParams.hash
          };

          Auth.optin(formData, function(res) {
            if(res.success) {
              $localStorage.token = res.data.token;
              Flash.create('success', '<strong>Success!</strong> ' + res.data.message, 0);
              $location.path('/welcome');
            } else {
              Flash.create('warning', '<strong>Mhh...</strong> ' + res.data.message, 0);
              $location.path('/login');
            }
          }, function (res) {
            Flash.create('warning', '<strong>Mhh...</strong> ' + res.data.message, 0);
            $location.path('/login');
          });
        } else {
          Flash.create('warning', '<strong>Mhh...</strong> stuff is missing.', 0);
          $location.path('/login');
        }
    };
}]);


/**
* Add potential validation fields to form-control form elements (CakePHP 3 with Bootstrap)
*
* Requirement: $scope.errors contains option (validation) errors in default CakePHP 3 JSON response
*/

angular.module('f0f7.directives', [])
.directive('formControl', ['$compile', function($compile) {
    return {
        restrict: 'C', //Match all elements with form-control class
        link: function(scope,element,attrs){

            //Get the model and fieldname (name.field)
            var elements = attrs.ngModel.split(".");
            
            //Create a placeholder for error(s)
            var template = "<span ng-repeat='(type, error) in errors." + elements[1] + "' class='help-block'><p class='text-danger'>{{error}}</p></span>";

            //Compile the template and append it to the parent of the form-control element
            element.parent().append($compile(template)(scope));
        }
    };
}]);

// http://www.ashley-bovan.co.uk/words/partsofspeech.html
angular.module('eloquent', [])
.factory('Eloquent', ['$window', '$q', '$log', function($window, $q, $log) {
	return {
		phrase : function(maxNameLength) {
			var adjective = this.adjectives[Math.floor(Math.random()*this.adjectives.length)];
			var noun = this.nouns[Math.floor(Math.random()*this.nouns.length)];
			var name = this.uppercaseWord(adjective) + this.uppercaseWord(noun);

			return name.substring(0,maxNameLength);
		},
		uppercaseWord : function(word) {
			return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
		},
		adjectives : [
			"abased","abject","able","abloom","ablush","abreast","abridged","abroach","abroad","abrupt","abscessed","absolved","absorbed","abstruse","absurd","abused","abuzz","accrete","accrued","accurst","acerb","aching","acock","acold","acorned","acred","acrid","acting","added","addle","addorsed","adept","adjunct","admired","adnate","adored","adrift","adroit","adscript","adult","adunc","adust","advised","aery","afeard","afeared","affine","affined","afire","aflame","afloat","afoot","afoul","afraid","after","aftmost","agape","agaze","aged","ageing","ageless","agelong","aggrieved","aghast","agile","aging","agleam","agley","aglow","agnate","ago","agog","agone","agreed","aground","ahead","ahorse","ahull","aidful","aidless","ailing","aimless","ain","air","airborne","airless","airsick","airtight","ajar","akin","alar","alate","alert","algal","algid","algoid","alien","alight","alike","alined","alive","alleged","allowed","alloyed","alone","aloof","alright","altered","altern","alvine","amazed","amber","amiss","amok","amort","ample","amuck","amused","android","angled","anguine","anguished","anile","announced","ansate","anti","antic","antique","antlered","antlike","antrorse","anxious","apart","apeak","apish","appalled","applied","appressed","arcane","arching","argent","arid","armchair","armless","armored","aroid","aroused","arranged","arrant","arrased","arrhythmic","artful","artless","arty","ashake","ashamed","ashen","ashy","askance","askant","askew","asking","aslant","asleep","aslope","asphalt","asprawl","asquint","assumed","assured","astir","astral","astute","aswarm","athirst","atilt","atrip","attached","attack","attent","attired","attrite","attuned","audile","aurous","austere","averse","avid","avowed","awake","aware","awash","away","aweless","awesome","awestruck","awful","awheel","awing","awkward","awnless","awry","axile","azure","babbling","baccate","backboned","backhand","backless","backmost","backstage","backstair","backstairs","backswept","backward","backwoods","baddish","baffling","baggy","bairnly","balanced","balding","baldish","baleful","balky","bally","balmy","banal","bandaged","banded","baneful","bangled","bankrupt","banner","bannered","baptist","bar","barbate","bardic","bardy","bareback","barebacked","barefaced","barefoot","barer","barest","baric","barish","barkless","barky","barmy","baroque","barrelled","baseless","baser","basest","bashful","basic","bassy","bastioned","bated","battered","battled","batty","bausond","bawdy","beaded","beady","beaky","beaming","beamish","beamless","beamy","beardless","bearish","bearlike","beastlike","beastly","beaten","beating","beauish","becalmed","bedded","bedfast","bedight","bedimmed","bedrid","beechen","beefy","beery","beetle","befogged","begrimed","beguiled","behind","bellied","belted","bemazed","bemused","bended","bending","bendwise","bendy","benign","benthic","benzal","bereft","berried","berserk","besieged","bespoke","besprent","bestead","bestial","betrothed","beveled","biased","bifid","biform","bigger","biggest","biggish","bijou","bilgy","bilious","billion","billionth","bilobed","binate","biped","birchen","birdlike","birken","bistred","bitchy","bitless","bitten","bitty","bivalve","bizarre","blackish","blameful","blameless","blaring","blasted","blasting","blatant","bleary","blended","blending","blindfold","blinding","blinking","blissful","blissless","blithesome","bloated","blockish","blocky","blooded","bloodied","bloodshot","bloodstained","blooming","bloomless","bloomy","blotchy","blotto","blotty","blowhard","blowsy","blowy","blowzy","blubber","bluer","bluest","bluish","blurry","blushful","blushless","boarish","boastful","boastless","bobtail","bodger","bodied","boding","boggy","bogus","bomb","bombproof","boneless","bonism","bonkers","bony","bonzer","bookish","bookless","boorish","booted","bootleg","bootless","boozy","bordered","boring","bosker","bosky","bosom","bosomed","bossy","botchy","bouffant","boughten","bouilli","bouncy","bounded","bounden","boundless","bousy","bovid","bovine","bowing","boxlike","boyish","bracing","brackish","bractless","braggart","bragging","braided","brainless","brainsick","brainy","brakeless","brambly","branching","branchless","branchlike","branny","brashy","brassy","brattish","bratty","braver","bravest","braving","brawny","brazen","breaking","breakneck","breasted","breathless","breathy","breechless","breeding","breezeless","breezy","brickle","bricky","bridgeless","briefless","brilliant","brimful","brimless","brimming","brinded","brindle","brindled","brinish","briny","bristly","brittle","broadband","broadcast","broadish","broadloom","broadside","broch","broguish","bronzy","broody","broomy","browless","brownish","browny","bruising","brumal","brumous","brunet","brunette","brushless","brushy","brutal","brute","brutelike","brutish","bubbly","buccal","buckish","buckram","buckshee","buckskin","bucktooth","bucktoothed","budless","buggy","bughouse","buirdly","bulbar","bulbous","bulgy","bulky","bullate","bullied","bullish","bumbling","bumptious","bumpy","bunchy","bunted","buoyant","burdened","burghal","buried","burlesque","burly","burry","bursal","bursting","bushy","busied","buskined","bustled","busty","buttocked","buxom","bygone","byssal","caboched","caboshed","caddish","cadenced","cadent","cadgy","cagey","cagy","caitiff","calcic","calfless","caller","callous","callow","calmy","campy","cancelled","cancrine","cancroid","candent","candied","canine","cankered","canny","canty","cany","capeskin","caprine","captious","captive","cardboard","carefree","careful","careless","careworn","caring","carking","carlish","carmine","carnose","carpal","carping","carsick","carven","casebook","casteless","castled","catching","catchweight","catchy","cattish","catty","caudate","cauline","causal","causeless","cautious","cayenned","ceaseless","cecal","cedarn","ceilinged","censured","centered","centred","centric","centrist","centum","cercal","cerise","cerous","certain","cervid","cervine","cestoid","chaffless","chaffy","chainless","chairborne","chaliced","chalky","chambered","chanceful","chanceless","chancy","changeful","changeless","changing","chapeless","chargeful","chargeless","charming","charmless","charry","chartered","chartless","chary","chasmal","chasmic","chasmy","chasseur","chaster","chastest","chastised","chatty","checkered","checky","cheeky","cheerful","cheerless","cheerly","cheery","cheesy","chelate","chemic","chequy","cherty","chestnut","chesty","chevroned","chewy","chichi","chiefless","chiefly","chiffon","childing","childish","childless","childlike","childly","chill","chilly","chin","chintzy","chipper","chippy","chirpy","chiseled","chiselled","chlorous","chocker","choicer","chokey","choking","choky","chondral","choosey","choosy","chopping","choppy","choral","chordal","chordate","choric","chrismal","chronic","chthonic","chubby","chuffy","chummy","chunky","churchless","churchly","churchward","churchy","churlish","churning","chymous","cichlid","cirrate","cirrose","cirsoid","cissoid","cissy","cisted","cistic","citrous","citrus","clamant","clammy","clankless","clannish","clasping","classless","classy","clastic","clathrate","clausal","claustral","clavate","clawless","clayey","clayish","cleanly","cleansing","clerkish","clerkly","cliffy","clingy","clinquant","clipping","cliquey","cliquish","cliquy","clitic","clockwise","cloddish","cloddy","clogging","cloggy","cloistered","cloistral","clonic","closer","closest","clotty","clouded","cloudless","cloudy","clovered","clownish","cloying","clubby","clucky","clueless","clumpy","clumsy","clustered","coaly","coarser","coarsest","coastal","coastward","coastwise","coated","coatless","coccal","coccoid","cockney","cocksure","cocky","coffered","cogent","cognate","coky","coldish","collapsed","collect","colloid","colly","coltish","columned","comal","comate","combined","combless","combust","comely","comfy","coming","commie","commo","comose","compact","compelled","compleat","complete","compo","composed","concave","conceived","concerned","conchal","conchate","concise","condemned","condign","conferred","confined","confirmed","confused","conjoined","conjoint","conjunct","connate","conoid","conscious","constrained","consumed","contained","contrate","contrite","contrived","controlled","contused","convex","convict","convinced","cooing","cooking","coolish","copied","coppiced","corbelled","cordate","corded","cordial","cordless","coreless","corking","corky","cormous","cornered","cornute","corny","correct","corrupt","corvine","cosher","costal","costate","costive","costly","costumed","cottaged","couchant","counter","countless","courant","couthie","couthy","coxal","coyish","cozy","crabbed","crabby","crablike","crabwise","crackbrained","crackers","cracking","crackjaw","crackle","crackling","crackly","crackpot","craftless","crafty","cragged","craggy","cranky","crannied","crashing","craven","crawling","crawly","creaky","creamlaid","creamy","creasy","credent","creedal","creepy","crenate","crescive","cressy","crestless","cricoid","crimeless","crimpy","crimson","crinal","cringing","crinite","crinkly","crinoid","crinose","crippling","crispate","crispy","crisscross","cristate","croaky","crookback","crooked","crosiered","crossbred","crosstown","crosswise","croupous","croupy","crowded","crowing","crowning","crownless","crucial","cruder","crudest","cruel","crumbly","crumby","crummy","crumpled","crunchy","crural","crushing","crustal","crusted","crustless","crusty","crying","cryptal","cryptic","ctenoid","cubbish","cubist","cuboid","cultic","cultish","cultrate","cumbrous","cunning","cupric","cuprous","curbless","curdy","cureless","curly","currish","cursed","cursing","cursive","curtate","curving","curvy","cushy","cuspate","cussed","custom","cutcha","cuter","cutest","cyan","cycloid","cyclone","cymoid","cymose","cystoid","cytoid","czarist","daedal","daffy","daimen","dainty","daisied","dam","damaged","damfool","damning","dampish","dancing","dangling","dapper","dapple","dappled","daring","darkish","darkling","darksome","dashing","dastard","dated","dateless","dauby","dauntless","daylong","daytime","deathful","deathless","deathlike","deathly","deathy","debased","debauched","deceased","decent","declared","decreed","decurved","dedal","deedless","defaced","defiled","defined","deflexed","deformed","defunct","deictic","deism","deject","deltoid","demure","dendroid","denser","densest","dentate","dentoid","deposed","depraved","depressed","deprived","deranged","dermal","dermic","dermoid","dernier","descant","described","desert","deserved","designed","desired","desmoid","despised","destined","detached","detailed","deuced","deviled","devoid","devout","dewlapped","dewy","dextral","dextrorse","dextrous","diarch","dicey","dickey","dicky","diet","diffuse","diffused","dighted","diglot","dilute","dimmest","dimming","dimply","dingbats","dingy","dinkies","dinky","diplex","diploid","dippy","direful","direr","direst","dirty","discalced","disclosed","discoid","discreet","discrete","diseased","disgraced","disguised","dishy","disjoined","disjoint","disjunct","disliked","dispensed","disperse","dispersed","displayed","displeased","disposed","dissolved","distal","distent","distilled","distinct","distrait","distraught","distressed","disturbed","distyle","disused","divers","diverse","divorced","dizzied","dizzy","docile","dockside","doddered","dodgy","dogged","dogging","doggish","doggone","doggoned","doggy","doglike","doited","doleful","dolesome","dollish","doltish","donnard","donnered","donnish","donsie","dopey","dopy","dormant","dormie","dormy","dorty","dotal","doting","dotted","doty","doubling","doubtful","doubting","doubtless","doughy","dovelike","dovetailed","dovish","dowdy","dowie","downbeat","downhill","downrange","downright","downstage","downstair","downstairs","downstate","downstream","downwind","dozen","dozenth","dozing","dozy","draffy","drafty","dragging","draggy","draining","drastic","dratted","draughty","dreadful","dreamful","dreamless","dreamlike","dreamy","dreary","dreggy","dressy","drier","driest","driftless","drifty","drippy","driven","drizzly","droning","dronish","droopy","dropping","dropsied","drossy","droughty","drouthy","drowsing","drowsy","drudging","drumly","drunken","dryer","ducal","duckbill","duckie","ducky","ductile","duddy","dudish","dulcet","dullish","dumbstruck","dumpish","dun","dungy","dural","duskish","dusky","dustless","dustproof","dwarfish","dyeline","dying","earnest","earthborn","earthbound","earthen","earthly","earthquaked","earthward","earthy","easeful","eastbound","eastmost","eastward","eaten","eating","ebon","eccrine","ecru","edgeless","edging","edgy","eely","eerie","eery","effete","effluent","effuse","egal","eighteen","eighteenth","eightfold","eighty","elapsed","elder","eldest","eldritch","elect","elfin","elfish","elite","elmy","elvish","embowed","emersed","emptied","enarched","enate","encased","enceinte","endarch","endless","endmost","endorsed","endways","enforced","engorged","engrailed","engrained","engraved","enhanced","enjambed","enlarged","enorm","enough","enow","enraged","enrapt","enrolled","enslaved","enthralled","entire","entranced","enured","enwrapped","equine","equipped","erased","erect","ermined","erose","errant","errhine","erring","ersatz","erstwhile","escaped","essive","estranged","estrous","eterne","ethic","ethmoid","ethnic","eustyle","evens","evoked","exact","exarch","exchanged","excused","exempt","exhaled","expert","expired","exposed","exsert","extant","extinct","extrorse","eyeless","fabled","faceless","facete","factious","faddish","faddy","faded","fadeless","fading","faecal","failing","faintish","fairish","faithful","faithless","falcate","falser","falsest","fameless","famished","famous","fancied","fanfold","fangled","fangless","farand","farci","farfetched","farming","farouche","farrow","farther","farthest","fatal","fated","fateful","fatigue","fatigued","fatless","fatter","fattest","fattish","faucal","faucial","faultless","faulty","faunal","favored","favoured","fearful","fearless","fearsome","feastful","feathered","featured","febrile","fecal","feckless","fecund","federalist","feeble","feeblish","feeling","feisty","feline","felon","felsic","fenny","feodal","feral","ferine","ferny","fervent","fervid","fesswise","festal","festive","fetching","fetial","fetid","feudal","fewer","fibered","fibroid","fibrous","fickle","fictile","fictive","fiddling","fiddly","fiendish","fiercer","fiercest","fifteen","fifteenth","fifty","filar","filial","filose","filthy","filtrable","financed","fineable","finer","finest","fingered","finished","finite","finless","finny","fireproof","firry","fishy","fissile","fistic","fitchy","fitful","fitted","fitter","fitting","fivefold","fizzy","flabby","flaccid","flagging","flaggy","flagrant","flameproof","flaming","flamy","flappy","flaring","flashy","flatling","flattest","flattish","flaunty","flawless","flawy","flaxen","fleckless","fledgeling","fledgling","fledgy","fleeceless","fleecy","fleeing","fleeting","fleshless","fleshly","fleshy","flexile","flightless","flighty","flimsy","flinty","flippant","flipping","flitting","floaty","floccose","floccus","flooded","floodlit","floppy","florid","flory","flossy","floury","flowered","flowing","fluent","fluffy","flukey","fluky","flurried","fluted","fluty","flyweight","foamless","foamy","focused","focussed","foetal","foetid","fogbound","foggy","fogless","folded","folkish","folklore","folksy","fontal","foodless","foolish","foolproof","footed","footless","footling","footsore","footworn","foppish","forceful","forceless","forehand","foremost","forenamed","foresaid","foreseen","forespent","foretold","forfeit","forky","former","formless","fornent","forspent","forte","forthright","fortis","forworn","foughten","fourfold","fourscore","foursquare","fourteenth","foxy","fozy","fractious","fractured","fragile","fragrant","frantic","fratchy","fraudful","frazzled","freakish","freaky","freckly","freebie","freeborn","freeing","freer","freest","frenzied","frequent","freshman","fretful","fretted","fretty","fribble","friended","friendless","frightened","frightful","frilly","fringeless","frisky","frizzly","frizzy","frockless","frolic","fronded","frontal","frontier","frontless","frosted","frostless","frostlike","frosty","frothy","froward","frowsty","frowsy","frowzy","frozen","fructed","frugal","fruited","fruitful","fruitless","fruity","frumpish","frumpy","frustrate","fubsy","fucoid","fugal","fulfilled","fulgent","fulgid","fulsome","fulvous","fumy","funded","funest","fungal","fungoid","fungous","funky","furcate","furry","furthest","furtive","furzy","fuscous","fusil","fusile","fussy","fustian","fusty","futile","fuzzy","gabbroid","gabled","gadoid","gadrooned","gaga","gainful","gainless","gainly","gaited","galliard","galling","gallooned","galore","gamer","gamesome","gamest","gamey","gamic","gammy","gamy","gangling","gangly","ganoid","gaping","gardant","garish","garni","gassy","gated","gateless","gaudy","gaumless","gauzy","gawky","gawsy","gearless","geegaw","gelded","gelid","gemel","gemmate","gemmy","genal","genial","genic","genteel","genty","georgic","germane","gestic","gewgaw","ghastful","ghastly","ghostly","ghoulish","gibbose","gibbous","giddied","giddy","gifted","giggly","gilded","gimcrack","gimlet","gimpy","girlish","girly","giving","glabrate","glabrous","glacial","gladsome","glaikit","glairy","glandered","glaring","glary","glasslike","glassy","gleeful","gleesome","gleety","glenoid","glial","glibber","glibbest","globate","globoid","globose","gloomful","glooming","gloomy","glossies","glossy","glottic","glowing","gluey","glummer","glummest","glumpy","glutted","glyphic","glyptic","gnarly","gnathic","gneissic","gneissoid","gnomic","gnomish","goalless","goateed","goatish","godless","godlike","godly","goitrous","goodish","goodly","gooey","goofy","goosey","goosy","gorgeous","gormless","gorsy","gory","gouty","gowaned","goyish","graceful","graceless","gracile","gracious","gradely","grainy","grapey","grapy","grasping","graspless","grassy","grateful","grating","gratis","grave","gravel","graveless","gravest","gravid","grayish","greening","greenish","greensick","greyish","griefless","grieving","grimmer","grimmest","grimy","gripping","gripple","grippy","grisly","gristly","gritty","grizzled","groggy","groovy","groping","grotesque","grotty","grouchy","groundless","grouty","grubby","grudging","gruesome","gruffish","grumbly","grummer","grummest","grumose","grumous","grumpy","gruntled","guardant","guarded","guardless","guideless","guiding","guileful","guileless","guiltless","guilty","gular","gulfy","gummous","gummy","gumptious","gunless","gushy","gusty","gutless","gutsy","gutta","guttate","gyral","gyrate","gyrose","habile","hackly","hackneyed","hadal","haemal","haemic","haggish","hairless","hairlike","halest","halftone","hallowed","haloid","halting","hamate","hammered","hammy","handed","handled","handless","handmade","handsome","handworked","handwrought","handy","hangdog","hapless","haploid","haptic","harassed","hardback","hardened","hardwood","harlot","harmful","harmless","harnessed","harried","hastate","hasty","hatching","hated","hateful","hatless","hatted","haughty","haunted","haunting","hawkish","hawklike","haywire","hazy","headed","headfirst","headless","headlong","headmost","headstrong","heady","healing","healthful","healthy","heaping","heapy","hearted","heartfelt","hearties","heartless","heartsome","hearty","heated","heathen","heathy","heating","heavies","heaving","hectic","hedgy","heedful","heedless","heelless","hefty","heinous","heirless","helmless","helpful","helpless","hemal","hempen","hempy","hennaed","herbaged","herbal","herbless","herby","here","hidden","highbrow","highest","hilding","hilly","hinder","hindmost","hindward","hipper","hippest","hippy","hircine","hirsute","hispid","hissing","histie","histoid","hitchy","hither","hiveless","hivelike","hobnail","hobnailed","hoggish","hoiden","holey","hollow","holmic","holstered","homebound","homeless","homelike","homely","homesick","homespun","homeward","homey","homy","honest","honeyed","honied","hoodless","hoofless","hooly","hopeful","hopeless","hopping","horal","hornish","hornless","hornlike","horny","horrent","horrid","horsey","horsy","hotfoot","hotshot","hotter","hottest","hotting","hottish","hourlong","hourly","housebound","houseless","hoven","howling","hoyden","hueless","huffish","huffy","huger","hugest","hulking","hulky","humbler","humdrum","humic","humid","hummel","humpbacked","humpy","hunchback","hunchbacked","hundredth","hungry","hunky","hunted","hurling","hurried","hurtful","hurtless","hurtling","husky","hydric","hydro","hydroid","hydrous","hymnal","hyoid","hyphal","hypnoid","icky","ictic","idem","idled","idlest","idling","iffy","ignored","imbued","immane","immense","immersed","immune","impel","impelled","impish","implied","imposed","improved","impure","inane","inapt","inboard","inborn","inbound","inbred","inbreed","inby","incased","incensed","incised","incog","increased","incrust","incult","incurved","incuse","indign","indoor","indrawn","inept","infect","infelt","infirm","inflamed","inflexed","inform","informed","ingrain","ingrained","ingrate","ingrown","inhaled","inhumed","injured","inky","inlaid","inmost","innate","inphase","inrush","insane","inscribed","inshore","insides","inspired","instinct","insured","intact","intense","intent","intern","interred","intime","intoed","intoned","intown","introrse","inured","involved","inward","inwrought","irate","ireful","irksome","itching","itchy","ivied","jaded","jadish","jagged","jaggy","jammy","jangly","jannock","japan","jarring","jasp","jaundiced","jazzy","jealous","jejune","jellied","jerky","jessant","jestful","jesting","jet","jetting","jetty","jeweled","jewelled","jiggered","jiggish","jiggly","jingly","jobless","jocose","jocund","jointed","jointless","jointured","joking","jolty","jouncing","jowly","joyful","joyless","joyous","jubate","jugal","jugate","juiceless","juicy","jumbled","jumpy","jungly","jural","jurant","jussive","jutting","kacha","kaput","karmic","karstic","kayoed","kerchiefed","keyless","khaki","kidnapped","killing","kilted","kindless","kindly","kindred","kingless","kinglike","kingly","kinky","kinless","kirtled","kittle","klephtic","klutzy","knaggy","knavish","kneeling","knickered","knifeless","knightless","knightly","knitted","knobby","knotless","knotted","knotty","knowing","knuckly","knurly","kookie","kooky","kosher","kutcha","labelled","labile","labored","laboured","labrid","labroid","lacking","lacy","laddish","laden","laggard","laic","lairy","laky","lambdoid","lambent","lamblike","lamer","lamest","laming","lanate","landed","landless","landscaped","landward","languid","lanky","lanose","lapelled","lapstrake","larboard","larger","largest","largish","larine","larkish","larky","larval","lashing","lasting","lated","lateen","later","latest","lathlike","lathy","latish","latter","latticed","laurelled","lavish","lawful","lawless","lawny","leachy","leaden","leadless","leady","leafless","leafy","leaky","leaning","leaping","learned","leary","leathern","ledgy","leery","leftward","legged","leggy","legit","legless","leisure","leisured","lengthways","lengthwise","lengthy","lenis","lenten","lentic","lento","lentoid","leprose","leprous","lettered","licenced","licensed","licit","lidded","lidless","liege","lifeful","lifeless","lifelike","lifelong","lighted","lightfast","lightful","lightish","lightless","lightsome","lightweight","lignite","likely","lilied","limbate","limbless","limey","limpid","limy","liney","lingual","linty","liny","lipless","lipoid","lippy","lissom","lissome","listless","lither","lithesome","lithest","lithic","litho","lithoid","litten","littler","littlest","livelong","lively","livid","loaded","loamy","loathful","loathly","loathsome","lobar","lobate","lobose","lofty","logy","lonesome","longer","longhand","longing","longish","longsome","longwall","longwise","looking","loonies","loopy","looser","loosest","lordless","lordly","losel","losing","lossy","lotic","loudish","lounging","louring","loury","lousy","loutish","louvered","louvred","loveless","lovelorn","lovely","lovesick","lovesome","lowly","loyal","lozenged","lubric","lucent","lucid","luckless","lukewarm","lumpen","lumpish","lunate","lupine","lurdan","lurid","luscious","lushy","lustful","lustral","lustred","lustrous","lusty","lying","lymphoid","lyrate","lyric","macled","madcap","maddest","madding","maigre","mainstream","maintained","makeless","makeshift","malar","male","malign","malty","mammoth","man","maneless","manful","mangey","mangy","manic","manky","manlike","mannered","mannish","mansard","mantic","many","marching","mardy","marish","maroon","married","marshy","masking","massive","massy","mastless","mastoid","matchless","mated","matey","matin","matted","mature","maudlin","maungy","mawkish","maxi","mazy","meager","meagre","meaning","measled","measly","measured","meaty","medley","melic","mellow","mensal","menseful","menseless","mere","merest","merging","mesarch","meshed","mesic","messier","messy","metalled","mettled","mickle","middling","midget","midi","midmost","midship","midships","miffy","mighty","migrant","milkless","million","millionth","millrun","mimic","mincing","minded","mindful","mindless","mingy","mini","minim","minion","minute","mirky","mirthful","mirthless","miry","mis","misformed","mislaid","misproud","missive","misty","mistyped","misused","mitered","mizzen","mnemic","moanful","mobbish","model","modeled","modest","modish","molal","molar","moldy","molten","monarch","moneyed","monger","mongrel","monied","monism","monkish","mono","monstrous","montane","monthly","mony","moody","moonish","moonless","moonlit","moonstruck","moony","moory","mopey","mopy","mordant","moreish","morish","morose","mossy","motey","mothy","motile","motored","mottled","mounted","mournful","mousey","mousy","mouthless","mouthy","moveless","mowburnt","mucid","mucking","muckle","mucky","mucoid","muddy","muggy","muley","mulish","mulley","mumchance","mundane","mural","murine","murky","murrey","muscid","muscly","museful","mushy","musing","musky","mussy","mustached","musty","mutant","muted","muzzy","mythic","nacred","nagging","naggy","naiant","naif","nailless","naissant","naive","nameless","naming","napless","napping","nappy","nary","nascent","nasty","natant","natty","naughty","nauseous","needful","needless","needy","negroid","neighbor","neighbour","nephric","nerval","nervate","nerveless","nervine","nervy","nescient","nested","nestlike","netted","nettly","neural","neuron","neuter","newborn","newish","newsless","newsy","nicer","nicest","nifty","niggard","niggling","nightless","nightlong","nightly","nimble","nimbused","ninefold","nineteen","ninety","nipping","nippy","nitid","nitty","nival","nobby","nocent","nodal","nodding","nodose","nodous","noiseless","noisette","noisome","noisy","nonplused","nonplussed","nonstick","northmost","northward","nosey","notal","notchy","noted","noteless","noticed","notour","novel","novice","noxious","nubbly","nubile","nudist","numbing","nuptial","nutant","nutlike","nutmegged","nutty","nymphal","oafish","oaken","oarless","oaten","obese","oblate","obliged","oblique","oblong","obscene","obscure","observed","obtect","obtuse","obverse","occult","ocher","ochre","ocker","oddball","offbeat","offhand","offish","offscreen","offshore","offside","often","oily","okay","olden","older","oldest","olid","only","onshore","onside","onstage","onward","oozing","oozy","ornate","orphan","ortho","oscine","osiered","osmic","osmous","otic","outback","outboard","outbound","outbred","outcast","outcaste","outdone","outdoor","outland","outlaw","outlined","outmost","outraged","outright","outsize","outsized","outspread","outworn","ovate","over","overt","ovine","ovoid","owing","owlish","owllike","packaged","padded","pagan","painful","painless","paler","palest","paling","palish","pallid","pally","palmar","palmate","palmy","palpate","palsied","paltry","paly","pan","paneled","panniered","panzer","papist","pappose","pappy","par","pardine","parklike","parky","parlous","parol","parotid","parted","partite","pass","passant","passless","pasteboard","pasted","pastel","pasties","pasty","patchy","patent","pathic","pathless","patient","paunchy","pausal","pauseful","pauseless","pavid","pawky","payoff","peaceful","peaceless","peachy","peaked","peaky","pearlized","peaty","pebbly","peccant","peckish","pedal","pedate","peddling","peeling","peerless","peevish","peewee","peltate","pelting","pencilled","pendant","pendent","pending","penile","pennate","pennied","pennoned","pensile","pensive","peppy","perceived","percent","percoid","perished","perjured","perky","perplexed","perverse","pesky","petalled","petite","petrous","pettish","pewter","phaseless","phasic","phasmid","phatic","phlegmy","phocine","phonal","phoney","phonic","phony","photic","phrenic","phthisic","phylloid","physic","piano","picked","pickled","picky","pictured","piddling","piebald","piecemeal","piercing","piggie","piggish","pillaged","pillared","pilose","pimpled","pimply","pinchbeck","piney","pinguid","pinkish","pinnate","pinpoint","piny","pious","pipeless","pipelike","piping","pipy","piquant","piscine","pitchy","pithy","pitted","placeless","placid","placoid","plagal","plaguey","plaguy","plaided","plaintive","plangent","plantar","plantless","plashy","plastered","plastics","plated","platy","plausive","playful","pleading","pleasing","plebby","pleural","pliant","plical","plicate","plodding","plosive","plotful","plotless","plucky","plumaged","plumate","plumbic","plumbless","plumbous","plummy","plumose","plumy","plusher","plushest","poachy","pockmarked","pocky","podgy","poignant","pointing","pointless","pokey","pokies","poky","polished","polite","pollened","poltroon","pompous","ponceau","pongid","poorly","poppied","porcine","porky","porous","porrect","portly","possessed","postern","postiche","postponed","potent","potted","potty","powered","practic","practiced","practised","praising","prayerful","prayerless","preachy","preborn","precast","precise","prefab","preggers","pregnant","premed","premier","premiere","premorse","prepared","prepense","preschool","prescribed","prescript","present","preserved","preset","pressing","pressor","presto","presumed","pretend","pretty","prewar","priceless","pricey","pricy","prideful","prideless","priestly","priggish","primal","primate","primsie","princely","printed","printless","prissy","pristine","privies","probing","produced","profane","profaned","professed","profound","profuse","prolate","prolix","pronounced","proposed","proscribed","prostate","prostrate","prostyle","prosy","proven","provoked","prowessed","proxy","prudent","prudish","prunted","prying","pseudo","psycho","pubic","pucka","puddly","pudgy","puffy","puggish","puggy","puisne","pukka","puling","pulpy","pulsing","punchy","punctate","punctured","pungent","punkah","puny","pupal","purblind","purer","purest","purging","purplish","purpure","pursued","pursy","pushing","pushy","pussy","putrid","pygmoid","pyknic","pyoid","quadrate","quadric","quaggy","quaky","qualmish","quantal","quartan","quartered","quartic","quartile","queasy","queenless","queenly","quenchless","quibbling","quickset","quiet","quilted","quinate","quinoid","quinsied","quintan","quintic","quippish","quirky","quondam","rabic","rabid","racemed","racing","racist","racy","raddled","raffish","raging","rainier","rainless","rainproof","raising","rakehell","rakish","ralline","ramal","rambling","rammish","ramose","rampant","ramstam","rancid","randie","randy","rangy","ranking","raploch","rarer","rarest","raring","rascal","rasping","raspy","ratite","ratlike","rattish","rattling","rattly","ratty","raucous","raunchy","ravaged","raving","rawboned","rawish","rayless","rearmost","rearward","reasoned","rebel","reborn","rebuked","reckless","recluse","record","rectal","recurved","redder","reddest","reddish","reedy","reeky","refer","refined","regal","regent","regnal","regnant","released","relieved","remiss","remnant","removed","rending","renowned","rental","repand","repent","replete","reproved","reptant","reptile","required","rescued","resigned","resolved","restful","resting","restive","restless","restored","retail","retained","retired","retral","retrorse","retuse","revealed","revered","reviled","revived","revolved","rheumy","rhinal","rhodic","rhomboid","rhotic","rhythmic","riant","ribald","ribless","riblike","ridden","rident","ridgy","riftless","righteous","rightful","rightish","rightist","rightward","rigid","riming","rimless","rimose","rimy","rindless","rindy","ringent","ringless","ripping","ripply","risen","risky","riteless","ritzy","rival","riven","roadless","roasting","robust","rodded","rodless","rodlike","roguish","roily","rollneck","rompish","roofless","rooky","roomy","rooted","rootless","rootlike","ropy","roseless","roselike","rostral","rosy","rotate","rotted","rotting","rotund","roughcast","roughish","rounded","rounding","roundish","roupy","rousing","routed","routine","rowdy","rubbly","rubied","rubric","rudish","rueful","ruffled","rufous","rugged","rugose","ruling","rumbly","rummy","rumpless","runic","runny","runtish","runty","rushing","rushy","russet","rustic","rustred","rusty","ruthful","ruthless","rutted","ruttish","rutty","saclike","sacral","sadist","sagging","said","sainted","saintly","saline","sallow","saltant","salted","saltier","saltish","saltless","salty","salving","sandalled","sanded","sandy","saner","sanest","sanguine","sapid","sapless","sappy","sarcoid","sarcous","sarky","sassy","sated","satem","saucy","saut","saving","savvy","scabby","scabrous","scaldic","scalelike","scalene","scalpless","scampish","scandent","scanty","scaphoid","scarcer","scarcest","scarless","scary","scatheless","scathing","scatty","scentless","sceptral","scheming","schistose","schizo","schizoid","schmaltzy","schmalzy","scientific","scincoid","scirrhoid","scirrhous","scissile","scleroid","sclerosed","sclerous","scombrid","scombroid","scopate","scornful","scraggly","scraggy","scrambled","scrannel","scrappy","scratchless","scratchy","scrawly","scrawny","screaky","screeching","screwy","scribal","scrimpy","scroddled","scroggy","scrotal","scrubbed","scrubby","scruffy","scrumptious","sculptured","scummy","scungy","scurrile","scurry","scurvy","scutate","seaboard","seaborne","seamless","seamy","searching","seasick","seatless","seaward","second","sectile","secund","secure","sedate","sedgy","seduced","seedless","seedy","seeing","seeking","seely","seeming","seemly","seismal","seismic","sejant","select","selfish","selfless","selfsame","semi","senile","sensate","senseless","septal","septate","sequent","sequined","seral","serene","serfish","serflike","serrate","serried","serviced","servo","setose","severe","sexism","sexist","sexless","sextan","sexy","shabby","shaded","shadeless","shadowed","shady","shaftless","shaken","shaky","shallow","shalwar","shamefaced","shameful","shameless","shapeless","shapely","shaping","shaven","shawlless","sheathy","sheepish","shellproof","shelly","shickered","shieldless","shieldlike","shier","shiest","shiftless","shifty","shingly","shining","shiny","shipboard","shipless","shipshape","shirtless","shirty","shocking","shoddy","shoeless","shopworn","shoreless","shoreward","shortcut","shortish","shorty","shotten","showy","shredded","shredless","shrewish","shrieval","shrinelike","shrouding","shroudless","shrubby","shrunken","shyer","shyest","sicker","sicklied","sickly","sideling","sidelong","sideward","sideways","sighful","sighted","sightless","sightly","sigmate","silenced","silken","silty","silvan","silvern","simplex","sincere","sinful","singing","singsong","sinless","sinning","sissy","sister","sixfold","sixteen","sixty","sizy","skaldic","sketchy","skewbald","skidproof","skilful","skillful","skimpy","skinking","skinless","skinny","skirtless","skittish","skyward","slaggy","slakeless","slangy","slantwise","slapstick","slashing","slaty","slavish","sleazy","sleekit","sleeky","sleepless","sleepwalk","sleepy","sleety","sleeveless","slender","slickered","slier","sliest","slighting","slimline","slimmer","slimmest","slimming","slimsy","slimy","slinky","slippy","slipshod","sloping","sloshy","slothful","slouchy","sloughy","sludgy","sluggard","sluggish","sluicing","slumbrous","slummy","slushy","sluttish","smacking","smallish","smarmy","smartish","smarty","smashing","smeary","smectic","smelly","smileless","smiling","smitten","smokeproof","smoking","smothered","smugger","smuggest","smutty","snafu","snaggy","snakelike","snaky","snappish","snappy","snarly","snatchy","snazzy","sneaking","sneaky","snider","snidest","sniffy","snippy","snobbish","snoopy","snooty","snoozy","snoring","snotty","snouted","snowless","snowlike","snubby","snuffly","snuffy","snugger","snuggest","snugging","soapless","soapy","soaring","sober","socko","sodden","softish","softwood","soggy","sola","solemn","soli","sollar","solus","solute","solvent","somber","sombre","sombrous","sometime","sonant","songful","songless","sonless","sonsie","sonsy","soothfast","soothing","sopping","soppy","sordid","sorer","sorest","sorry","sotted","sottish","soulful","soulless","soundless","soundproof","soupy","sourish","southmost","southpaw","southward","sovran","sozzled","spaceless","spacial","spacious","spadelike","spangly","spanking","sparid","sparing","sparkless","sparkling","sparoid","sparry","sparser","sparsest","spastic","spathic","spathose","spatial","spavined","specious","speckled","speckless","speechless","speedful","speeding","speedless","speedy","spellbound","spendthrift","spermic","spermous","sphagnous","sphenic","spheral","sphereless","spherelike","spheric","sphery","sphygmic","sphygmoid","spicate","spicy","spiffing","spiffy","spiky","spindling","spindly","spineless","spinose","spinous","spiral","spirant","spireless","spiroid","spiry","spiteful","splanchnic","splashy","spleenful","spleenish","spleeny","splendent","splendid","splendrous","splenic","splitting","splurgy","spoken","spokewise","spongy","spooky","spoony","sportful","sportive","sportless","sporty","spotless","spotty","spousal","spouseless","spouted","spoutless","spriggy","sprightful","sprightly","springing","springless","springlike","springtime","springy","sprucer","sprucest","sprucing","spryer","spryest","spunky","spurless","squabby","squalid","squally","squamate","squamous","squarish","squarrose","squashy","squeaky","squeamish","squiffy","squiggly","squirmy","squirting","squishy","stabbing","stabile","stagey","stagnant","stagy","stalkless","stalky","stalwart","stalworth","stannous","staple","starboard","starchy","staring","starless","starlight","starlike","starring","starry","starveling","starving","statant","stated","stateless","stateside","statewide","statist","stative","statued","steadfast","stealthy","steamtight","steamy","stedfast","steepled","stelar","stellar","stellate","stemless","stenosed","stepwise","steric","sterile","sternal","sternmost","sthenic","stickit","stiffish","stifling","stilly","stilted","stingless","stingy","stinko","stintless","stirless","stirring","stockinged","stockish","stockless","stocky","stodgy","stolen","stolid","stoneground","stoneless","stoneware","stonkered","stopless","stopping","store","storeyed","storied","stormbound","stormless","stormproof","stotious","stoutish","straining","strangest","strapless","strapping","stratous","strawless","strawlike","streaky","streaming","streamless","streamlined","streamy","stressful","stretchy","striate","stricken","strident","strifeful","strifeless","strigose","stringent","stringless","stringy","stripeless","stripy","strobic","strongish","strophic","stroppy","structured","strutting","strychnic","stubbled","stubbly","stubborn","stubby","studied","stuffy","stumbling","stumpy","stunning","stupid","sturdied","sturdy","stutter","stylar","styleless","stylised","stylish","stylized","styloid","subdued","subfusc","subgrade","sublimed","submerged","submersed","submiss","subscribed","subscript","subtile","subtle","succinct","suchlike","suffused","sugared","suited","sulcate","sulfa","sulkies","sulky","sullen","sullied","sultry","sunbaked","sunbeamed","sunburnt","sunfast","sunken","sunless","sunlike","sunlit","sunproof","sunrise","sunset","sunward","super","superb","supine","supple","supposed","sural","surbased","surer","surest","surfy","surgeless","surging","surgy","surly","surpliced","surplus","surprised","suspect","svelter","sveltest","swainish","swampy","swanky","swaraj","swarthy","sweated","sweaty","sweeping","sweetmeal","swelling","sweptwing","swindled","swingeing","swinish","swirly","swishy","swordless","swordlike","sylphic","sylphid","sylphish","sylphy","sylvan","systemless","taboo","tabu","tacit","tacky","tactful","tactile","tactless","tailing","tailless","taillike","tailored","taintless","taken","taking","talcose","talking","talky","taloned","tameless","tamer","tamest","taming","tandem","tangential","tangier","tangled","tangy","tannic","tapeless","tapelike","tardy","tarmac","tarnal","tarot","tarry","tarsal","tartish","tasseled","tasselled","tasteful","tasteless","tasty","tattered","tatty","taurine","tawdry","tawie","tearful","tearing","tearless","teary","teasing","techy","teeming","teenage","teensy","teeny","telic","telling","telltale","tempered","templed","tempting","tender","tenfold","tenor","tenseless","tenser","tensest","tensing","tensive","tented","tentie","tentless","tenty","tepid","terbic","terete","tergal","termless","ternate","terrene","tertial","tertian","testate","testy","tetchy","textbook","textile","textless","textured","thallic","thalloid","thallous","thankful","thankless","thatchless","thecal","thecate","theism","theist","themeless","thenar","thermic","theroid","thetic","thickset","thievish","thinking","thinnish","thirdstream","thirstless","thirsty","thirteen","thistly","thornless","thorny","thoughtful","thoughtless","thousandth","thowless","thrashing","threadbare","threadlike","thready","threatful","threefold","threescore","thriftless","thrifty","thrilling","throaty","throbbing","throbless","throneless","throwback","thudding","thuggish","thumbless","thumblike","thumping","thymic","thymy","thyrsoid","ticklish","tiddly","tideless","tidied","tightknit","timbered","timeless","timely","timeous","timid","tingly","tinhorn","tinkling","tinkly","tinny","tinsel","tintless","tiny","tippy","tiptoe","tiptop","tireless","tiresome","titled","toeless","toey","togaed","togate","toilful","toilsome","tombless","tonal","toneless","tongueless","tonguelike","tonish","tonnish","tony","toothless","toothlike","toothsome","toothy","topfull","topless","topmost","torose","torpid","torquate","torrent","tortile","tortious","tortured","tother","touching","touchy","toughish","touring","tourist","toward","towered","townish","townless","towy","toxic","toyless","toylike","traceless","trackless","tractile","tractrix","trainless","tranquil","transcribed","transient","transposed","traplike","trappy","trashy","traveled","travelled","traverse","treacly","treasured","treen","trembling","trembly","trenchant","trendy","tressured","tressy","tribal","tribeless","trichoid","trickish","trickless","tricksome","tricksy","tricky","tricorn","trident","trifid","trifling","triform","trillion","trillionth","trilobed","trinal","triploid","trippant","tripping","tristful","triter","tritest","triune","trivalve","trochal","trochoid","trodden","trophic","trophied","tropic","troppo","trothless","troublous","truant","truceless","truer","truffled","truncate","trunnioned","trustful","trusting","trustless","trusty","truthful","truthless","tryptic","tsarism","tsarist","tubal","tubate","tubby","tubeless","tumbling","tumid","tuneful","tuneless","turbaned","turbid","turdine","turfy","turgent","turgid","tuskless","tussal","tussive","tutti","twaddly","tweedy","twelvefold","twenty","twiggy","twinkling","twinning","twofold","typal","typhous","typic","ugsome","ullaged","umber","umbral","umbrose","umpteen","umpteenth","unaimed","unaired","unapt","unarmed","unasked","unawed","unbacked","unbagged","unbaked","unbarbed","unbarred","unbathed","unbegged","unbent","unbid","unblamed","unbleached","unblenched","unblent","unblessed","unblocked","unblown","unboned","unborn","unborne","unbought","unbound","unbowed","unbraced","unbranched","unbreached","unbreathed","unbred","unbreeched","unbridged","unbroke","unbruised","unbrushed","unburned","unburnt","uncaged","uncalled","uncapped","uncashed","uncaught","uncaused","unchained","unchanged","uncharge","uncharged","uncharmed","unchaste","unchecked","uncheered","unchewed","unclad","unclaimed","unclassed","unclean","uncleaned","uncleansed","unclear","uncleared","unclimbed","unclipped","unclogged","unclutched","uncocked","uncoined","uncombed","uncooked","uncouth","uncropped","uncross","uncrowned","unculled","uncurbed","uncured","uncursed","uncurved","uncut","undamped","undeaf","undealt","undecked","undimmed","undipped","undocked","undone","undrained","undraped","undrawn","undreamed","undreamt","undress","undressed","undried","undrilled","undrowned","undrunk","undubbed","undue","undug","undulled","undyed","unfair","unfanned","unfeared","unfed","unfelled","unfelt","unfenced","unfiled","unfilled","unfilmed","unfine","unfired","unfirm","unfished","unfit","unflawed","unfledged","unflushed","unfooled","unforced","unforged","unformed","unfought","unfound","unframed","unfraught","unfree","unfunded","unfurred","ungalled","ungauged","ungeared","ungilt","ungirthed","unglad","unglazed","unglossed","ungloved","ungored","ungorged","ungowned","ungraced","ungrassed","ungrazed","ungroomed","unground","ungrown","ungrudged","ungual","unguessed","unguled","ungummed","ungyved","unhacked","unhailed","unhanged","unharmed","unhatched","unhealed","unheard","unhelped","unhewn","unhinged","unhired","unhooped","unhorsed","unhung","unhurt","unhusked","unique","unjust","unkempt","unkenned","unkept","unkind","unkinged","unkissed","unknelled","unlaid","unlearned","unlearnt","unleased","unled","unlet","unlike","unlimed","unlined","unlit","unlooked","unlopped","unlost","unloved","unmade","unmailed","unmaimed","unmanned","unmarked","unmarred","unmasked","unmatched","unmeant","unmeet","unmet","unmilked","unmilled","unmissed","unmixed","unmoaned","unmourned","unmoved","unmown","unnamed","unoiled","unowned","unpaced","unpaged","unpaid","unpained","unpaired","unpared","unpaved","unpeeled","unpent","unperched","unpicked","unpierced","unplaced","unplagued","unplanked","unplayed","unpleased","unpledged","unploughed","unplucked","unplumb","unplumbed","unplumed","unpoised","unpolled","unposed","unpraised","unpreached","unpressed","unpriced","unprimed","unprized","unpropped","unproved","unpruned","unpurged","unquelled","unquenched","unraised","unraked","unreached","unread","unreaped","unreined","unrent","unrhymed","unribbed","unrigged","unrimed","unripe","unroped","unrouged","unroused","unrubbed","unrude","unruled","unsafe","unsaid","unsailed","unsapped","unsashed","unsaved","unscaled","unscanned","unscarred","unscathed","unschooled","unscorched","unscoured","unscratched","unscreened","unsealed","unsearched","unseen","unseized","unsensed","unsent","unset","unshamed","unshaped","unshared","unshaved","unsheathed","unshed","unshipped","unshocked","unshod","unshoed","unshorn","unshown","unshrived","unshunned","unshut","unsight","unsigned","unsized","unskilled","unskimmed","unskinned","unslain","unsliced","unsluiced","unslung","unsmirched","unsmooth","unsmoothed","unsnuffed","unsoaped","unsoft","unsoiled","unsold","unsolved","unsought","unsound","unsown","unspared","unsparred","unspelled","unspent","unspied","unspilled","unspilt","unspoiled","unspoilt","unsprung","unspun","unsquared","unstack","unstacked","unstaid","unstained","unstamped","unstarched","unstilled","unstirred","unstitched","unstocked","unstopped","unstrained","unstreamed","unstressed","unstringed","unstriped","unstripped","unstrung","unstuck","unstuffed","unsucked","unsung","unsure","unswayed","unswept","unsworn","untailed","untame","untamed","untanned","untapped","untarred","untaught","unteamed","unthanked","unthawed","unthought","untied","untiled","untilled","untinged","untinned","untired","untold","untombed","untoned","untorn","untouched","untraced","untracked","untrained","untrenched","untressed","untried","untrimmed","untrod","untrue","unturfed","unturned","unurged","unused","unversed","unvexed","unviewed","unvoiced","unwaked","unwarmed","unwarned","unwarped","unwashed","unwatched","unweaned","unwebbed","unwed","unweened","unweighed","unwell","unwept","unwet","unwhipped","unwilled","unwinged","unwiped","unwired","unwise","unwished","unwitched","unwon","unwooed","unworked","unworn","unwound","unwrapped","unwrought","unwrung","upbeat","upbound","upcast","upgrade","uphill","upmost","uppish","upraised","upset","upstage","upstaged","upstair","upstairs","upstart","upstate","upstream","uptight","uptown","upturned","upward","upwind","urbane","urdy","urgent","urnfield","useful","useless","utile","utmost","vadose","vagal","vagrant","vagrom","vaguer","vaguest","valanced","valgus","valiant","valid","valval","valvar","valvate","vambraced","vaneless","vanward","vapid","varied","varus","vassal","vasty","vatic","vaulted","vaulting","vaunted","vaunting","vaunty","veilless","veiny","velar","velate","vellum","venal","vengeful","venose","venous","ventose","verbless","verbose","verdant","verism","verist","vespine","vestral","vibrant","viceless","viewless","viewy","villose","villous","vinous","viral","virgate","virile","visaged","viscid","viscose","viscous","vitric","vivid","vivo","vixen","voetstoots","vogie","voiceful","voiceless","voided","volant","volar","volumed","volvate","vorant","voteless","votive","vulpine","vying","wacky","wageless","waggish","waggly","wailful","wailing","waisted","wakeful","wakeless","wakerife","waking","walnut","wambly","wandle","waney","waning","wanner","wannest","wanning","wannish","wanting","wanton","warded","warlike","warming","warmish","warning","warring","wartless","wartlike","warty","wary","washy","waspish","waspy","wasted","wasteful","watchful","waveless","wavelike","waving","wavy","waxen","waxing","waxy","wayless","wayward","wayworn","weakly","weaponed","wearied","wearing","wearish","weary","weathered","webby","wedded","wedgy","weedy","weekday","weekly","weeny","weepy","weer","weest","weighted","weighty","welcome","weldless","westbound","western","wetter","wettish","whacking","whacky","whapping","wheaten","wheezing","wheezy","wheyey","whilom","whining","whinny","whiny","whiplike","whirring","whiskered","whitish","whittling","whity","wholesale","wholesome","whopping","whoreson","whorish","wicked","wicker","wider","widespread","widest","widish","wieldy","wifeless","wifely","wiggly","wigless","wiglike","wilful","willful","willing","willyard","wily","wimpy","windburned","winded","windproof","windswept","windy","wingless","winglike","wintry","winy","wiretap","wiring","wiry","wiser","wisest","wising","wispy","wistful","witchy","withdrawn","withy","witless","witted","witting","witty","wizard","wizen","wizened","woaded","wobbling","woeful","woesome","wolfish","wonky","wonted","wooded","woodless","woodsy","woodwind","woolen","woollen","woozier","woozy","wordless","wordy","workless","worldly","worldwide","wormy","worried","worser","worshipped","worthless","worthwhile","worthy","wounded","woundless","woven","wrapround","wrathful","wrathless","wreathless","wreckful","wretched","wrier","wriest","wriggly","wrinkly","writhen","writhing","written","wrongful","xanthous","xerarch","xeric","xiphoid","xylic","xyloid","yarer","yarest","yawning","yclept","yearling","yearlong","yearly","yearning","yeastlike","yeasty","yester","yestern","yielding","yogic","yolky","yonder","younger","youthful","yttric","yuletide","zany","zealous","zebrine","zeroth","zestful","zesty","zigzag","zillion","zincky","zincoid","zincous","zincy","zingy","zinky","zippy","zonate","zoning"
		],
		nouns : [
			"aardvark","abyssinian","accelerator","accordion","account","accountant","acknowledgment","acoustic","acrylic","act","action","activity","actor","actress","adapter","addition","address","adjustment","adult","advantage","advertisement","aftermath","afternoon","aftershave","afterthought","age","agenda","agreement","air","airbus","airmail","airplane","airport","airship","alarm","albatross","alcohol","algebra","algeria","alibi","alley","alligator","alloy","almanac","alphabet","alto","aluminium","aluminum","ambulance","america","amount","amusement","anatomy","anethesiologist","anger","angle","angora","animal","anime","ankle","answer","ant","anteater","antelope","anthony","anthropology","apartment","apology","apparatus","apparel","appeal","appendix","apple","appliance","approval","april","aquarius","arch","archaeology","archeology","archer","architecture","area","argentina","argument","aries","arithmetic","arm","armadillo","armchair","army","arrow","art","ash","ashtray","asia","asparagus","asphalt","asterisk","astronomy","athlete","ATM","atom","attack","attempt","attention","attic","attraction","august","aunt","australia","australian","author","authority","authorization","avenue","baboon","baby","back","backbone","bacon","badge","badger","bag","bagel","bagpipe","bail","bait","baker","bakery","balance","balinese","ball","balloon","bamboo","banana","band","bandana","bangle","banjo","bank","bankbook","banker","bar","barbara","barber","barge","baritone","barometer","base","baseball","basement","basin","basket","basketball","bass","bassoon","bat","bath","bathroom","bathtub","battery","battle","bay","beach","bead","beam","bean","bear","beard","beast","beat","beautician","beauty","beaver","bed","bedroom","bee","beech","beef","beer","beet","beetle","beggar","beginner","begonia","behavior","belgian","belief","bell","belt","bench","bengal","beret","berry","bestseller","betty","bibliography","bicycle","bike","bill","billboard","biology","biplane","birch","bird","birth","birthday","bit","bite","black","bladder","blade","blanket","blinker","blizzard","block","blouse","blow","blowgun","blue","board","boat","bobcat","body","bolt","bomb","bomber","bone","bongo","bonsai","book","bookcase","booklet","boot","border","botany","bottle","bottom","boundary","bow","bowl","box","boy","bra","brace","bracket","brain","brake","branch","brand","brandy","brass","brazil","bread","break","breakfast","breath","brian","brick","bridge","british","broccoli","brochure","broker","bronze","brother","brother-in-law","brow","brown","brush","bubble","bucket","budget","buffer","buffet","bugle","building","bulb","bull","bulldozer","bumper","bun","burglar","burma","burn","burst","bus","bush","business","butane","butcher","butter","button","buzzard","cabbage","cabinet","cable","cactus","cafe","cake","calculator","calculus","calendar","calf","call","camel","camera","camp","can","cancer","candle","cannon","canoe","canvas","cap","capital","cappelletti","capricorn","captain","caption","car","caravan","carbon","card","cardboard","cardigan","care","carnation","carol","carp","carpenter","carriage","carrot","cart","cartoon","case","cast","castanet","cat","catamaran","caterpillar","cathedral","catsup","cattle","cauliflower","cause","caution","cave","c-clamp","cd","ceiling","celery","celeste","cell","cellar","cello","celsius","cement","cemetery","cent","centimeter","century","ceramic","cereal","certification","chain","chair","chalk","chance","change","channel","character","chard","charles","chauffeur","check","cheek","cheese","cheetah","chef","chemistry","cheque","cherry","chess","chest","chick","chicken","chicory","chief","child","children","chill","chime","chimpanzee","chin","china","chinese","chive","chocolate","chord","christmas","christopher","chronometer","church","cicada","cinema","circle","circulation","cirrus","citizenship","city","clam","clarinet","class","claus","clave","clef","clerk","click","client","climb","clipper","cloakroom","clock","close","closet","cloth","cloud","clover","club","clutch","coach","coal","coast","coat","cobweb","cockroach","cocktail","cocoa","cod","coffee","coil","coin","coke","cold","collar","college","collision","colombia","colon","colony","color","colt","column","columnist","comb","comfort","comic","comma","command","commission","committee","community","company","comparison","competition","competitor","composer","composition","computer","condition","condor","cone","confirmation","conga","congo","conifer","connection","consonant","continent","control","cook","copper","copy","copyright","cord","cork","cormorant","corn","cornet","correspondent","cost","cotton","couch","cougar","cough","country","course","court","cousin","cover","cow","cowbell","crab","crack","cracker","craftsman","crate","crawdad","crayfish","crayon","cream","creator","creature","credit","creditor","creek","crib","cricket","crime","criminal","crocodile","crocus","croissant","crook","crop","cross","crow","crowd","crown","crush","cry","cub","cuban","cucumber","cultivator","cup","cupboard","cupcake","curler","currency","current","curtain","curve","cushion","custard","customer","cut","cuticle","cycle","cyclone","cylinder","cymbal","dad","daffodil","dahlia","daisy","damage","dance","dancer","danger","daniel","dash","dashboard","database","date","daughter","david","day","dead","deadline","deal","death","deborah","debt","debtor","decade","december","decimal","decision","decrease","dedication","deer","defense","deficit","degree","delete","delivery","den","denim","dentist","deodorant","department","deposit","description","desert","design","desire","desk","dessert","destruction","detail","detective","development","dew","diamond","diaphragm","dibble","dictionary","dietician","difference","digestion","digger","digital","dill","dime","dimple","dinghy","dinner","dinosaur","diploma","dipstick","direction","dirt","disadvantage","discovery","discussion","disease","disgust","dish","distance","distribution","distributor","division","dock","doctor","dog","dogsled","doll","dollar","dolphin","domain","donald","donkey","donna","door","double","doubt","downtown","dragon","dragonfly","drain","drake","drama","draw","drawbridge","drawer","dream","dredger","dress","dresser","drill","drink","drive","driver","drizzle","drop","drug","drum","dry","dryer","duck","duckling","dugout","dungeon","dust","eagle","ear","earth","earthquake","ease","edge","edger","editor","editorial","education","edward","eel","effect","egg","eggnog","eggplant","egypt","eight","elbow","element","elephant","elizabeth","ellipse","emery","employee","employer","encyclopedia","end","enemy","energy","engine","engineer","english","enquiry","entrance","environment","epoch","epoxy","equinox","equipment","era","error","estimate","ethernet","ethiopia","euphonium","europe","evening","event","examination","example","exchange","exclamation","exhaust","ex-husband","existence","expansion","experience","expert","explanation","ex-wife","eye","eyebrow","eyelash","eyeliner","face","fact","factory","fahrenheit","fall","family","fan","fang","farm","farmer","fat","father","father-in-law","faucet","fear","feast","feather","feature","february","fedelini","feedback","feeling","feet","felony","female","fender","ferry","ferryboat","fertilizer","fiber","fiberglass","fibre","fiction","field","fifth","fight","fighter","file","find","fine","finger","fir","fire","fired","fireman","fireplace","firewall","fish","fisherman","flag","flame","flare","flat","flavor","flax","flesh","flight","flock","flood","floor","flower","flugelhorn","flute","fly","foam","fog","fold","font","food","foot","football","footnote","force","forecast","forehead","forest","forgery","fork","form","format","fortnight","foundation","fountain","fowl","fox","foxglove","fragrance","frame","france","freckle","freeze","freezer","freighter","french","freon","friction","Friday","fridge","friend","frog","front","frost","frown","fruit","fuel","fur","furniture","galley","gallon","game","gander","garage","garden","garlic","gas","gasoline","gate","gateway","gauge","gazelle","gear","gearshift","geese","gemini","gender","geography","geology","geometry","george","geranium","german","germany","ghana","ghost","giant","giraffe","girdle","girl","gladiolus","glass","glider","glockenspiel","glove","glue","goal","goat","gold","goldfish","golf","gondola","gong","good-bye","goose","gore-tex","gorilla","gosling","government","governor","grade","grain","gram","granddaughter","grandfather","grandmother","grandson","grape","graphic","grass","grasshopper","gray","grease","great-grandfather","great-grandmother","greece","greek","green","grenade","grey","grill","grip","ground","group","grouse","growth","guarantee","guatemalan","guide","guilty","guitar","gum","gun","gym","gymnast","hacksaw","hail","hair","haircut","half-brother","half-sister","halibut","hall","hallway","hamburger","hammer","hamster","hand","handball","handicap","handle","handsaw","harbor","hardboard","hardcover","hardhat","hardware","harmonica","harmony","harp","hat","hate","hawk","head","headlight","headline","health","hearing","heart","heat","heaven","hedge","height","helen","helicopter","helium","hell","helmet","help","hemp","hen","heron","herring","hexagon","hill","himalayan","hip","hippopotamus","history","hockey","hoe","hole","holiday","home","honey","hood","hook","hope","horn","horse","hose","hospital","hot","hour","hourglass","house","hovercraft","hub","hubcap","humidity","humor","hurricane","hyacinth","hydrant","hydrofoil","hydrogen","hyena","hygienic","ice","icebreaker","icicle","icon","idea","ikebana","illegal","imprisonment","improvement","impulse","inch","income","increase","index","india","indonesia","industry","ink","innocent","input","insect","instruction","instrument","insulation","insurance","interactive","interest","internet","interviewer","intestine","invention","inventory","invoice","iran","iraq","iris","iron","island","israel","italian","italy","jacket","jaguar","jail","jam","james","january","japan","japanese","jar","jasmine","jason","jaw","jeep","jelly","jellyfish","jennifer","jet","jewel","join","joke","joseph","journey","judge","judo","juice","july","jumbo","jump","jumper","june","jury","justice","jute","kale","kamikaze","kangaroo","karate","karen","kayak","kendo","kenneth","kenya","ketchup","kettle","kettledrum","kevin","key","keyboard","kick","kidney","kilogram","kilometer","kimberly","kiss","kitchen","kite","kitten","kitty","knee","knife","knight","knot","knowledge","kohlrabi","korean","laborer","lace","ladybug","lake","lamb","lamp","lan","land","landmine","language","larch","lasagna","latency","latex","lathe","laugh","laundry","laura","law","lawyer","layer","lead","leaf","leather","leek","leg","legal","lemonade","lentil","leo","leopard","letter","lettuce","level","libra","library","license","lier","lift","light","lightning","lilac","lily","limit","linda","line","linen","link","lion","lip","lipstick","liquid","liquor","lisa","list","literature","litter","liver","lizard","llama","loaf","loan","lobster","lock","locket","locust","look","loss","lotion","love","low","lumber","lunch","lunchroom","lung","lunge","lute","luttuce","lycra","lynx","lyocell","lyre","lyric","macaroni","machine","macrame","magazine","magic","magician","maid","mail","mailbox","mailman","makeup","malaysia","male","mall","mallet","man","manager","mandolin","manicure","manx","map","maple","maraca","marble","march","margaret","margin","maria","marimba","mark","market","married","mary","mascara","mask","mass","match","math","mattock","may","mayonnaise","meal","measure","meat","mechanic","medicine","meeting","melody","memory","men","menu","mercury","message","metal","meteorology","meter","methane","mexican","mexico","mice","michael","michelle","microwave","middle","mile","milk","milkshake","millennium","millimeter","millisecond","mimosa","mind","mine","minibus","mini-skirt","minister","mint","minute","mirror","missile","mist","mistake","mitten","moat","modem","mole","mom","Monday","money","monkey","month","moon","morning","morocco","mosque","mosquito","mother","mother-in-law","motion","motorboat","motorcycle","mountain","mouse","moustache","mouth","move","multi-hop","multimedia","muscle","museum","music","musician","mustard","myanmar","nail","name","nancy","napkin","narcissus","nation","neck","need","needle","neon","nepal","nephew","nerve","nest","net","network","newsprint","newsstand","nic","nickel","niece","nigeria","night","nitrogen","node","noise","noodle","norwegian","nose","note","notebook","notify","novel","november","number","numeric","nurse","nut","nylon","oak","oatmeal","objective","oboe","observation","occupation","ocean","ocelot","octagon","octave","october","octopus","odometer","offence","offer","office","oil","okra","olive","onion","open","opera","operation","ophthalmologist","opinion","option","orange","orchestra","orchid","order","organ","organisation","organization","ornament","ostrich","otter","ounce","output","outrigger","oval","oven","overcoat","owl","owner","ox","oxygen","oyster","package","packet","page","pail","pain","paint","pair","pajama","pakistan","palm","pamphlet","pan","pancake","pancreas","panda","pansy","panther","pantry","pair of pants","panty","pantyhose","paper","paperback","parade","parallelogram","parcel","parent","parenthesis","park","parrot","parsnip","part","particle","partner","partridge","party","passbook","passenger","passive","pasta","paste","pastor","pastry","patch","path","patient","patio","patricia","paul","payment","pea","peace","peak","peanut","pear","pedestrian","pediatrician","peen","peer-to-peer","pelican","pen","penalty","pencil","pendulum","pentagon","peony","pepper","perch","perfume","period","periodical","peripheral","permission","persian","person","peru","pest","pet","pharmacist","pheasant","philosophy","phone","physician","piano","piccolo","pickle","picture","pie","pig","pigeon","pike","pillow","pilot","pimple","pin","pine","ping","pink","pint","pipe","pisces","pizza","place","plain","plane","planet","plant","plantation","plaster","plasterboard","plastic","plate","platinum","play","playground","playroom","pleasure","plier","plot","plough","plow","plywood","pocket","poet","point","poison","poland","police","policeman","polish","politician","pollution","polo","polyester","pond","popcorn","poppy","population","porch","porcupine","port","porter","position","possibility","postage","postbox","pot","potato","poultry","pound","powder","power","precipitation","preface","prepared","pressure","price","priest","print","printer","prison","probation","process","produce","product","production","professor","profit","promotion","propane","property","prose","prosecution","protest","protocol","pruner","psychiatrist","psychology","ptarmigan","puffin","pull","puma","pump","pumpkin","punch","punishment","puppy","purchase","purple","purpose","push","pvc","pyjama","pyramid","quail","quality","quart","quarter","quartz","queen","question","quicksand","quiet","quill","quilt","quince","quit","quiver","quotation","rabbi","rabbit","radar","radiator","radio","radish","raft","rail","railway","rain","rainbow","raincoat","rainstorm","rake","ramie","random","range","rat","rate","raven","ravioli","ray","rayon","reaction","reading","reason","receipt","recess","record","recorder","rectangle","red","reduction","refrigerator","refund","regret","reindeer","relation","relative","religion","relish","reminder","repair","replace","report","representative","request","resolution","respect","responsibility","rest","restaurant","result","retailer","revolve","revolver","reward","rhinoceros","rhythm","rice","richard","riddle","rifle","ring","rise","risk","river","riverbed","road","roadway","roast","robert","robin","rock","rocket","rod","roll","romania","romanian","ronald","roof","room","rooster","root","rose","rotate","route","router","rowboat","rub","rubber","rugby","rule","run","russia","russian","rutabaga","ruth","sack","sagittarius","sail","sailboat","sailor","salad","salary","sale","salesman","salmon","salt","sampan","samurai","sand","sandra","sandwich","Santa","sardine","satin","Saturday","sauce","sausage","save","saw","saxophone","scale","scallion","scanner","scarecrow","scarf","scene","scent","schedule","school","science","scissor","scooter","scorpio","scorpion","scraper","screen","screw","screwdriver","sea","seagull","seal","seaplane","search","seashore","season","seat","second","secretary","secure","security","seed","seeder","segment","select","selection","self","semicircle","semicolon","sense","sentence","separated","september","servant","server","session","sex","shade","shadow","shake","shallot","shame","shampoo","shape","share","shark","sharon","shear","sheep","sheet","shelf","shell","shield","shingle","ship","shirt","shock","shoe","shoemaker","shop","pair of shorts","shoulder","shovel","show","shrimp","shrine","siamese","siberian","side","sideboard","sidecar","sidewalk","sign","signature","silica","silk","silver","sing","singer","single","sink","sister","sister-in-law","size","skate","ski","skill","skin","skirt","sky","slash","slave","sled","sleep","sleet","slice","slime","slip","slipper","slope","smash","smell","smile","smoke","snail","snake","sneeze","snow","snowboard","snowflake","snowman","snowplow","snowstorm","soap","soccer","society","sociology","sock","soda","sofa","softball","softdrink","software","soil","soldier","son","song","soprano","sort","sound","soup","sousaphone","soy","soybean","space","spade","spaghetti","spain","spandex","spark","sparrow","spear","specialist","speedboat","sphere","sphynx","spider","spike","spinach","spleen","sponge","spoon","spot","spring","sprout","spruce","spy","square","squash","squid","squirrel","stage","staircase","stamp","star","start","starter","state","statement","station","statistic","steam","steel","stem","step","step-aunt","step-brother","stepdaughter","step-daughter","step-father","step-grandfather","step-grandmother","stepmother","step-mother","step-sister","stepson","step-son","step-uncle","steven","stew","stick","stinger","stitch","stock","stocking","stomach","stone","stool","stop","stopsign","stopwatch","store","storm","story","stove","stranger","straw","stream","street","streetcar","stretch","string","structure","study","sturgeon","submarine","substance","subway","success","sudan","suede","sugar","suggestion","suit","summer","sun","Sunday","sundial","sunflower","sunshine","supermarket","supply","support","surfboard","surgeon","surname","surprise","susan","sushi","swallow","swamp","swan","sweater","sweatshirt","sweatshop","swedish","swim","swing","swiss","switch","sword","swordfish","sycamore","syrup","system","table","tablecloth","tabletop","tachometer","tadpole","tail","tailor","taiwan","talk","tank","tanker","tanzania","target","taste","taurus","tax","taxi","taxicab","tea","teacher","team","technician","teeth","television","teller","temper","temperature","temple","tempo","tendency","tennis","tenor","tent","territory","test","text","textbook","texture","thailand","theater","theory","thermometer","thing","thistle","thought","thread","thrill","throat","throne","thumb","thunder","thunderstorm","Thursday","ticket","tie","tiger","tile","timbale","time","timer","timpani","tin","tip","tire","titanium","title","toad","toast","toe","toenail","toilet","tomato","tom-tom","ton","tongue","tooth","toothbrush","toothpaste","top","tornado","tortellini","tortoise","touch","tower","town","toy","tractor","trade","traffic","trail","train","tramp","transaction","transmission","transport","trapezoid","tray","treatment","tree","trial","triangle","trick","trigonometry","trip","trombone","trouble","trouser","trout","trowel","truck","trumpet","trunk","t-shirt","tsunami","tub","tuba","Tuesday","tugboat","tulip","tuna","tune","turkey","turkish","turn","turnip","turnover","turret","turtle","tv","twig","twilight","twine","twist","typhoon","tyvek","uganda","umbrella","uncle","undercloth","underpant","undershirt","underwear","unit","unshielded","use","utensil","vacation","vacuum","valley","value","van","vase","vault","vegetable","vegetarian","veil","vein","velvet","verdict","vermicelli","verse","vessel","vest","veterinarian","vibraphone","Vietnam","view","vinyl","viola","violet","violin","virgo","viscose","vise","vision","visitor","voice","volcano","volleyball","voyage","vulture","waiter","waitress","walk","wall","wallaby","wallet","walrus","war","wash","washer","wasp","waste","watch","watchmaker","water","waterfall","wave","wax","way","wealth","weapon","weasel","weather","wedge","Wednesday","weed","weeder","week","weight","whale","wheel","whip","whiskey","whistle","white","wholesaler","whorl","wilderness","william","willow","wind","windchime","window","windscreen","windshield","wine","wing","winter","wire","wish","witch","withdrawal","witness","wolf","woman","women","wood","wool","woolen","word","work","workshop","worm","wound","wrecker","wren","wrench","wrinkle","wrist","writer","xylophone","yacht","yak","yam","yard","yarn","year","yellow","yew","yogurt","yoke","yugoslavian","zebra","zephyr","zinc","zipper","zone","zoo","zoology"
		]
	};
}]);

angular.module('f0f7.devices').factory('Generic', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var obj = {
		id		: 'Generic',
		manuf	: 'Zumbitsu',
		model	: 'Generic',
	};
	return obj;
}]);

// F0	:Start of System Exclusive
// 00	:Manufacturer ID 1		(Access Music Electronics)
// 20	:Manufacturer ID 2		(Access Music Electronics)
// 33	:Manufacturer ID 3		(Access Music Electronics)
// 01	:Product ID 			(Virus)
// dd	:Device ID			00..0F: individual; 10: omni.
// [message]
// F7	:End of System Exclusive


angular.module('f0f7.devices').factory('AccessVirus', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'AccessVirus',
		manuf 		: 'Access',
		model 		: 'Virus',
		hints 		: [
			'In menu SYSTEM/MULTI EDIT set DeviceId to <strong>Omni</strong>',
		],
		params : {
			deviceId	: 0x10, //DeviceId omni
			banks : {
				bank1 : {label : 'Virus Bank A', address : 0x01},
				bank2 : {label : 'Virus Bank B', address : 0x02},
				bank3 : {label : 'Virus Bank C', address : 0x03},
				bank4 : {label : 'Virus Bank D', address : 0x04}
			},
			patchesPerBank : 128,
			transferPause : 300,
			parserConfig : {
				name : {
					from : 249,
					to : 258
				},
				dna : {
					from : 9,
					to : 265
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			// Sysex.dumpData(midiData, 16);

			midiData[5] = this.params.deviceId;
			midiData[7] = 0x00; //BankNumber
			midiData[8] = 0x40; //ProgramNumber Single buffer in Single Mode (40)
			midiData[midiData.length-2] = Sysex.calculateAccessChecksum(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[5] = this.params.deviceId;
			midiData[7] = bank; //BankNumber
			midiData[8] = prgNo; //Prg No
			midiData[midiData.length-2] = Sysex.calculateAccessChecksum(midiData);

			return midiData;
		},
		retrieveBank : function(bank) {
			var midiData = [0xF0, 0x00, 0x20, 0x33, 0x01, this.params.deviceId, 0x32, bank, 0xF7];
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			this.setDnaParams(midiData);
			return [Sysex.extractPatchName(midiData, this.params.parserConfig)];
		},
		setDnaParams : function(midiData) {
			this.params.parserConfig.dna.to = midiData.length-2; //different dna for virus a,b,c and ti
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 10; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			// var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);
			// var midiData = Sysex.changeNameinMidiData(newNameArray, nameLength, program.data, this.params.parserConfig);
			// // midiData[midiData.length-2] = Sysex.calculateAccessChecksum(midiData);
			// return Sysex.extractPatchName(midiData, this.params.parserConfig);

			var midiData = Sysex.objectToArray(program.data);
			this.setDnaParams(midiData);
			return Sysex.renameProgram(newName, nameLength, midiData, this.params.parserConfig);
		}
	};
	return obj;
}]);

// $F0 System Exclusive
// $33 Manufacturer ID (clavia)
// <Device ID> = Global MIDI Channel, 0-15
// $04 Model ID for Nord Lead
// <Message Type> See each type of message, below.
// <Message Specification> See each type of message, below.
// <Data 1> This and following bytes depend on the Message Type and Message
// Specification. Some messages have no data bytes at all.
// <Data 2>
// <Data 3>
// <etc.>
// $F7 End Of Exclusive


angular.module('f0f7.devices').factory('ClaviaNordlead', ['$log', 'Sysex', '$timeout', 'WebMIDI', function($log, Sysex, $timeout, WebMIDI) {
	var obj = {
		id 			: 'ClaviaNordlead',
		manuf 		: 'Clavia',
		model 		: 'Nordlead',
		params : {
			deviceId	: 0x02, //= Global MIDI Channel. 0 to 15 ($0-$F)
			banks : {
				bank1 : {label : 'Nordlead Patch Slot A', address : 0x01},
				// bank2 : {label : 'Nordlead Patch Slot B', address : 0x02},
				// bank3 : {label : 'Nordlead Patch Slot C', address : 0x03},
				// bank4 : {label : 'Nordlead Patch Slot D', address : 0x04}
			},
			patchesPerBank : 40,//99 rom, 40 ram
			transferPause : 200,
			parserConfig : {
				dna : {
					from : 6,
					to : 139
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			// Sysex.dumpData(midiData, 16);

			midiData[2] = this.params.deviceId;
			midiData[4] = 0x00; //$00 to $04 0 to 4 Message Type specifies the Bank. 0=Edit Buffer, 1 to 4=Bank 1 to 4

			// midiData[5] = 0x00; //If Message Type = 0 (Edit Buffer), the Message Specification can be 0 to 3, corresponding to Patch Slot buttons A to D.
			// Sysex.dumpData(midiData, 16);

			var msgSpecAddress = [0x00,0x01,0x02,0x03]; //If Message Type = 0 (Edit Buffer), the Message Specification can be 0 to 3, corresponding to Patch Slot buttons A to D.

			//We dump to all buffers!
			var dumpDataToBuffer = function(bufferSlot) {
				midiData[5] = bufferSlot;
				Sysex.sendSysex(midiData, WebMIDI.midiOut);

				if(msgSpecAddress.length > 0) {
					bufferSlot = msgSpecAddress.splice(0,1);
					$timeout(dumpDataToBuffer, obj.params.transferPause, true, bufferSlot);
				} else {
					$log.info("Buffer dump completed");
				}
			};
			dumpDataToBuffer(msgSpecAddress.splice(0,1));
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[2] = this.params.deviceId;
			// midiData[4] = bank; //BankNumber
			midiData[4] = 0x01; //BankNumber
			midiData[5] = prgNo; //Prg No

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x33, this.params.deviceId, 0x04];
			var requestBank = 0x0B;// Message Type specifies the Bank. $A=Edit Buffer, $B to $E=Bank 1 to 4
			// switch(bank) {
			// 	case 0x01 : requestBank = 0x0B;break;
			// 	case 0x02 : requestBank = 0x0C;break;
			// 	case 0x03 : requestBank = 0x0D;break;
			// 	case 0x04 : requestBank = 0x0E;break;
			// }
			// console.log('requestBank ' , requestBank);

			Sysex.requestAllProgramsOfBank(requestBank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			var data = Sysex.objectToArray(midiData);
			var dna = Sysex.createDNA(data, this.params.parserConfig.dna.from, this.params.parserConfig.dna.to);
			return [{name : 'Vikings need no name', dna : dna, data : data}];
		},
		getMaxNameLength : function() {
			return 0;
		},
		renameProgram : function(newName, program) {
			return program;
		}
	};
	return obj;
}]);

// REMOTE PROGRAMMING
// ------------------
// The send request 1 and receive request 1 messages.

// These dump a lot of data across the MIDI, which is the same for both
// messages, except that the data go the other way. The exchanges are:

// Send request
// Computer:       F0 44 00 00 70+channel 10 program
// CZ101/1000:     F0 44 00 00 70+channel 30
// Computer:       70+channel 31
// CZ101/1000:     <tone data> F7
// Computer:       F7

// Receive request
// Computer:       F0 44 00 00 70+channel 20 program
// CZ101/1000:     F0 44 00 00 70+channel 30
// Computer:       <tone data> F7
// Cz101/1000:     F7

// The program byte is the same as that set by the PROGRAM CHANGE
// function, with the addition that you can request the temporary sound
// area as well ( number is 60 ). This is the area that is used if you
// have altered a preset and not saved it into internal memory.

angular.module('f0f7.devices').factory('CasioCz1000', ['$log', 'Sysex', '$timeout', 'WebMIDI', function($log, Sysex, $timeout, WebMIDI) {
	var obj = {
		id 			: 'CasioCz1000',
		manuf 		: 'Casio',
		model 		: 'CZ-101 / CZ-1000',
		hints 		: [
			'Set <strong>Midi Channel</strong> to <strong>1</strong>',
			'Programs will be transferred from and to the <strong>Internal Memory Tones</strong> section'
		],
		params : {
			deviceId : 0x70, //70+channel
			banks : {
				bank1 : {label : 'Casio User Memory Bank', address : 0x01},
			},
			patchesPerBank : 16,
			transferPause : 200,
			parserConfig : {
				dna : {
					from : 9,
					to : 264
				}
			}
		},
		sendToBuffer : function(program) {
			
			var midiData = Sysex.objectToArray(program.data);
			// Sysex.dumpData(midiData, 16);
			midiData[4] = this.params.deviceId;
			midiData[5] = 0x20; // Receive request
			midiData[6] = 0x60; //Edit buffer

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[4] = this.params.deviceId;
			midiData[5] = 0x20; // Receive request
			midiData[6] = prgNo + 0x20; //Prg No, 0x20 is start of internal

			return midiData;
		},
		retrieveBank : function(bank) {
			// Send request
			// Computer:       F0 44 00 00 70+channel 10 program
			// CZ101/1000:     F0 44 00 00 70+channel 30
			// Computer:       70+channel 31
			// CZ101/1000:      F7
			// Computer:       F7
			
			var header = [0xF0, 0x44, 0x00, 0x00, this.params.deviceId, 0x10];
			this.requestAllProgramsOfBank(header, this.params.patchesPerBank, this.params.transferPause);

		},
		requestAllProgramsOfBank : function(header, patchesPerBank, transferPause) {
			var dumpRequest = [];
			var prgNo = 0;

			var delayedDumpRequest = function(prgNo) {
				$log.debug("Requesting internal Prg " + prgNo);

				// Note that the preset tones are given numbers :
				// CZ101/1000

				// 0..0F   Preset sounds 1..16
				// 20..2F  Internal sounds 1..16
				// 40..4F  Cartridge sounds 1..16

				// CZ5000

				// 00..1F  Preset sounds A1,A2,A3....D6,D7,D8
				// 20..3F  Internal sounds A1,A2.....D6,D7,D8

				dumpRequest = header.concat([(prgNo + 0x20), obj.params.deviceId, 0x31, 0xF7]);
				// console.log('dumpRequest ' , dumpRequest);
				Sysex.sendSysex(dumpRequest, WebMIDI.midiOut);

				if(prgNo < (patchesPerBank-1)) {
					prgNo++;
					$timeout(delayedDumpRequest, transferPause, true, prgNo);
				} else {
					$log.info("Bank dumpRequest completed");
				}
			};
			delayedDumpRequest(prgNo);
		},
		extractPatchNames : function(midiData) {
			if(midiData[6] == 0xF7) {
				$log.info("Ignore strange response from Casio");
				return [];
			} else {
				var data = Sysex.objectToArray(midiData);
				if(data.length == 263) {
					$log.info("Request data from Casio is missing a byte");
					data.splice(5,0, 0x60);
					console.log(data);
				}
				var dna = Sysex.createDNA(data, this.params.parserConfig.dna.from, this.params.parserConfig.dna.to);
				return [{name : 'Casio Internal', dna : dna, data : data}];
			}
		},
		getMaxNameLength : function() {
			return 0;
		},
		renameProgram : function(newName, program) {
			return program;
		}
	};
	return obj;
}]);

// REMOTE PROGRAMMING
// ------------------
// The send request 1 and receive request 1 messages.

// These dump a lot of data across the MIDI, which is the same for both
// messages, except that the data go the other way. The exchanges are:

// Send request
// Computer:       F0 44 00 00 70+channel 10 program
// CZ101/1000:     F0 44 00 00 70+channel 30
// Computer:       70+channel 31
// CZ101/1000:     <tone data> F7
// Computer:       F7

// Receive request
// Computer:       F0 44 00 00 70+channel 20 program
// CZ101/1000:     F0 44 00 00 70+channel 30
// Computer:       <tone data> F7
// Cz101/1000:     F7

// The program byte is the same as that set by the PROGRAM CHANGE
// function, with the addition that you can request the temporary sound
// area as well ( number is 60 ). This is the area that is used if you
// have altered a preset and not saved it into internal memory.

angular.module('f0f7.devices').factory('CasioCz5000', ['$log', 'Sysex', '$timeout', 'WebMIDI', function($log, Sysex, $timeout, WebMIDI) {
	var obj = {
		id 			: 'CasioCz5000',
		manuf 		: 'Casio',
		model 		: 'CZ-5000 [alpha]',
		hints 		: [
			'Set <strong>Midi Channel</strong> to <strong>1</strong>',
			'Programs will be transferred from and to the <strong>Internal Memory Tones</strong> section'
		],
		params : {
			deviceId : 0x70, //70+channel
			banks : {
				bank1 : {label : 'Casio User Memory Bank', address : 0x01},
			},
			patchesPerBank : 32,
			transferPause : 200,
			parserConfig : {
				dna : {
					from : 9,
					to : 264
				}
			}
		},
		sendToBuffer : function(program) {
			
			var midiData = Sysex.objectToArray(program.data);
			// Sysex.dumpData(midiData, 16);
			midiData[4] = this.params.deviceId;
			midiData[5] = 0x20; // Receive request
			midiData[6] = 0x60; //Edit buffer

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[4] = this.params.deviceId;
			midiData[5] = 0x20; // Receive request
			midiData[6] = prgNo + 0x20; //Prg No, 0x20 is start of internal

			return midiData;
		},
		retrieveBank : function(bank) {
			// Send request
			// Computer:       F0 44 00 00 70+channel 10 program
			// CZ101/1000:     F0 44 00 00 70+channel 30
			// Computer:       70+channel 31
			// CZ101/1000:      F7
			// Computer:       F7
			
			var header = [0xF0, 0x44, 0x00, 0x00, this.params.deviceId, 0x10];
			this.requestAllProgramsOfBank(header, this.params.patchesPerBank, this.params.transferPause);

		},
		requestAllProgramsOfBank : function(header, patchesPerBank, transferPause) {
			var dumpRequest = [];
			var prgNo = 0;

			var delayedDumpRequest = function(prgNo) {
				$log.debug("Requesting internal Prg " + prgNo);

				// Note that the preset tones are given numbers :
				// CZ101/1000

				// 0..0F   Preset sounds 1..16
				// 20..2F  Internal sounds 1..16
				// 40..4F  Cartridge sounds 1..16

				// CZ5000

				// 00..1F  Preset sounds A1,A2,A3....D6,D7,D8
				// 20..3F  Internal sounds A1,A2.....D6,D7,D8

				dumpRequest = header.concat([(prgNo + 0x20), obj.params.deviceId, 0x31, 0xF7]);
				// console.log('dumpRequest ' , dumpRequest);
				Sysex.sendSysex(dumpRequest, WebMIDI.midiOut);

				if(prgNo < (patchesPerBank-1)) {
					prgNo++;
					$timeout(delayedDumpRequest, transferPause, true, prgNo);
				} else {
					$log.info("Bank dumpRequest completed");
				}
			};
			delayedDumpRequest(prgNo);
		},
		extractPatchNames : function(midiData) {
			if(midiData[6] == 0xF7) {
				$log.info("Ignore strange response from Casio");
				return [];
			} else {
				var data = Sysex.objectToArray(midiData);
				if(data.length == 263) {
					$log.info("Request data from Casio is missing a byte");
					data.splice(5,0, 0x60);
					console.log(data);
				}
				var dna = Sysex.createDNA(data, this.params.parserConfig.dna.from, this.params.parserConfig.dna.to);
				return [{name : 'Casio Internal', dna : dna, data : data}];
			}
		},
		getMaxNameLength : function() {
			return 0;
		},
		renameProgram : function(newName, program) {
			return program;
		}
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('DSIEvolver', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var specialEvolverProgramData = {};
	var dna;
	var obj = {
		id 			: 'DSIEvolver',
		manuf 		: 'Dave Smith Instruments',
		model 		: 'Evolver',
		hints 		: [
			'Should work with <strong>Mono</strong> and <strong>Poly Evolver</strong> and all Evolvers that support patch names',
			'In <strong>Global</strong> menu set <strong>MIDI SysEx</strong> to <strong>on</strong>',
		],
		params : {
			banks : {
				bank1 : {label : 'Evolver Bank 1', address : 0x00},
				bank2 : {label : 'Evolver Bank 2', address : 0x01},
				bank3 : {label : 'Evolver Bank 3', address : 0x02},
				bank4 : {label : 'Evolver Bank 4', address : 0x03}
			},
			patchesPerBank : 128,
			transferPause : 300,
			parserConfig : {
				name : {
					from : 7,
					to : 22
				},
				dna : {
					from : 7,
					to : 227
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);
			midiData[4] = 0x03; 	// Edit Buffer Data Dump
			midiData.splice(5, 2);	// Unset Bank and Prg
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			//Program Data Dump
			var programData = Sysex.objectToArray(program.data);

			programData[4] = 0x02; 	// Program Data Dump
			programData[5] = bank;		// 0-3
			programData[6] = prgNo;	// 0-127

			//Program Name Data Dump
			var header = [0xF0, 0x01, 0x20, 0x01, 0x11, bank, prgNo];
			var paddedNameArray = Sysex.createPaddedNameArray(this.getMaxNameLength(), program.name);
			var programNameData = header.concat(paddedNameArray, 0xF7);

			var midiData = programData.concat(programNameData);

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x01, 0x20, 0x01, 0x05];
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			var mode = 'unknown';
			switch(midiData[4]) {
				case 0x02 : mode = 'Program Data Dump'; break;
				case 0x11 : mode = 'Program Name Data Dump'; break;
				default: mode = 'unknown';
			}

			if (mode == 'Program Data Dump') {
				specialEvolverProgramData = new Uint8Array(midiData.length);
				specialEvolverProgramData = midiData;
				dna = Sysex.createDNA(specialEvolverProgramData, this.params.parserConfig.dna.from, this.params.parserConfig.dna.to);

			} else if (mode == 'Program Name Data Dump') {
				var data = angular.fromJson(angular.toJson(midiData));
				var patchName = '';

				for(var k=this.params.parserConfig.name.from; k<=this.params.parserConfig.name.to; k++) {
					patchName += String.fromCharCode(data[k]);
				}

				return [{name : patchName.trim(),  dna : dna, data : specialEvolverProgramData}];
			}
			return [];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return ?; 
		},
		renameProgram : function(newName, program) {
			return {name : newName.trim(),  dna : program.dna, data : program.data};
		}
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('DSIMopho', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'DSIMopho',
		manuf 		: 'Dave Smith Instruments',
		model 		: 'Mopho [beta]',
		hints 		: [
			'In <strong>Global</strong> menu set <strong>MIDI SysEx</strong> to <strong>on</strong>',
			'To edit the Global parameters, hold down the PROGRAM MODE switch until Global Parameter is displayed.'
		],
		params : {
			banks : {
				bank1 : {label : 'Mopho / Bank 1', address : 0x00},
				bank2 : {label : 'Mopho / Bank 2', address : 0x01},
				bank3 : {label : 'Mopho / Bank 3', address : 0x02},
			},
			patchesPerBank : 128,
			transferPause : 300,
			parserConfig : {
				rawData : {
					from: 6,
					to: 299 //256 bytes expanded to 293 MIDI bytes in “packed MS bit” format
				},
				name : {
					from : 184,
					to : 199
				},
				dna : {
					from : 0,
					to : 256
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);
			midiData[3] = 0x03; 	// Edit Buffer Data
			midiData.splice(4, 2);	// Unset Bank and Prg
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[3] = 0x02; 	// Program Data Dump
			midiData[4] = bank;		// 0x00, 0x01 or 0x02
			midiData[5] = prgNo;	// 0-127

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x01, 0x25, 0x05];
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			midiData = Sysex.objectToArray(midiData);
			var unpackedData = this.unpackMidiData(midiData);
			var program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			program.data = midiData;
			return [program];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 16; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, this.params.parserConfig.rawData.from);
			var unpackedData = this.unpackMidiData(midiData);

			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);
			program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			
			var packedData = Sysex.packDSISysex(unpackedData);
			program.data = header.concat(packedData, 0xF7);
			return program;
		},
		unpackMidiData : function(midiData) {
			var packedData = midiData.slice(this.params.parserConfig.rawData.from, this.params.parserConfig.rawData.to);
			var unpackedData = Sysex.unpackDSISysex(packedData);
			// Sysex.dumpData(unpackedData, 16);
			return unpackedData;
		}
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('DSIOb6', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'DSIOb6',
		manuf 		: 'Dave Smith Instruments',
		model 		: 'OB-6 [beta]',
		hints 		: [
			'This profile is not well tested. Please report errors.',
			'In <strong>GLOBALS</strong> menu set <strong>MIDI SysEx</strong> to <strong>Mid</strong>i or <strong>USb</strong> depending on which connection you use.',
			'You can only transfer programs from bank 0 to bank 4, higher banks are readable only (ROM).'
		],
		params : {
			banks : {
				bank1 : {label : 'OB-6 / Bank 0', address : 0x00},
				bank2 : {label : 'OB-6 / Bank 1', address : 0x01},
				bank3 : {label : 'OB-6 / Bank 2', address : 0x02},
				bank4 : {label : 'OB-6 / Bank 3', address : 0x03},
				bank5 : {label : 'OB-6 / Bank 4', address : 0x04},
			},
			patchesPerBank : 100,
			transferPause : 400,
			parserConfig : {
				rawData : {
					from: 6,
					to: 1177 //1171 bytes packed, 1024 expanded
				},
				name : {
					from : 107,
					to : 126
				},
				dna : {
					from : 0,
					to : 1024
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);
			midiData[3] = 0x03; 	// Edit Buffer Data
			midiData.splice(4, 2);	// Unset Bank and Prg
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[3] = 0x02; 	// Program Data Dump
			midiData[4] = bank;		// 0 - 9
			midiData[5] = prgNo;	// 0 - 99

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x01, 0x2E, 0x05];
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			midiData = Sysex.objectToArray(midiData);
			var unpackedData = this.unpackMidiData(midiData);
			var program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			program.data = midiData;
			return [program];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 22; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, this.params.parserConfig.rawData.from);
			var unpackedData = this.unpackMidiData(midiData);

			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);
			program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			
			var packedData = Sysex.packDSISysex(unpackedData);
			program.data = header.concat(packedData, 0xF7);
			return program;
		},
		unpackMidiData : function(midiData) {
			var packedData = midiData.slice(this.params.parserConfig.rawData.from, this.params.parserConfig.rawData.to);
			var unpackedData = Sysex.unpackDSISysex(packedData);
			// Sysex.dumpData(unpackedData, 16);
			return unpackedData;
		}
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('DSIPro2', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'DSIPro2',
		manuf 		: 'Dave Smith Instruments',
		model 		: 'Pro 2 [beta]',
		hints 		: [
			'To prepare the Pro 2 to receive system exclusive messages:',
			'Press <strong>Global</strong>.',
			'From the Global menu, select <strong>MIDI Sysex Enable</strong> and make sure it\'s set to <strong>On</strong>.',
			'From the Global menu, select <strong>MIDI Sysex Cable.</strong>',
			'If you\'re using USB, choose <strong>USB</strong>. If you\'re using a MIDI interface, choose <strong>MIDI</strong>.',
			'Press <strong>Global</strong> again to exit the Global menu.',
		],
		params : {
			banks : {
				bank1 : {label : 'Pro 2 / User Bank 0', address : 0x00},
				bank2 : {label : 'Pro 2 / User Bank 1', address : 0x01},
				bank3 : {label : 'Pro 2 / User Bank 2', address : 0x02},
				bank4 : {label : 'Pro 2 / User Bank 3', address : 0x03},
				// bank5 : {label : 'Pro 2 / Bank 4', address : 0x04},
				// bank6 : {label : 'Pro 2 / Bank 5', address : 0x05},
				// bank7 : {label : 'Pro 2 / Bank 6', address : 0x06},
				// bank8 : {label : 'Pro 2 / Bank 7', address : 0x07},
			},
			patchesPerBank : 99,
			transferPause : 400,
			parserConfig : {
				rawData : {
					from: 6,
					to: 1177 //1171 bytes packed, 1024 expanded
				},
				name : {
					from : 378,
					to : 397
				},
				dna : {
					from : 0,
					to : 1024
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);
			midiData[3] = 0x03; 	// Edit Buffer Data
			midiData.splice(4, 2);	// Unset Bank and Prg
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[3] = 0x02; 	// Program Data Dump
			midiData[4] = bank;		// 0 - 7
			midiData[5] = prgNo;	// 0 - 98

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x01, 0x2C, 0x05];
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			midiData = Sysex.objectToArray(midiData);
			var unpackedData = this.unpackMidiData(midiData);
			var program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			program.data = midiData;
			return [program];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 20; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, this.params.parserConfig.rawData.from);
			var unpackedData = this.unpackMidiData(midiData);

			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);
			program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			
			var packedData = Sysex.packDSISysex(unpackedData);
			program.data = header.concat(packedData, 0xF7);
			return program;
		},
		unpackMidiData : function(midiData) {
			var packedData = midiData.slice(this.params.parserConfig.rawData.from, this.params.parserConfig.rawData.to);
			var unpackedData = Sysex.unpackDSISysex(packedData);
			// Sysex.dumpData(unpackedData, 16);
			return unpackedData;
		}
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('DSIPro12', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'DSIPro12',
		manuf 		: 'Dave Smith Instruments',
		model 		: 'Pro 12 [beta]',
		hints 		: [
			'To prepare the Pro 12 to receive system exclusive messages:',
			'Press <strong>Global</strong>.',
			'From the Global menu, select <strong>MIDI Sysex Enable</strong> and make sure it\'s set to <strong>On</strong>.',
			'From the Global menu, select <strong>MIDI Sysex Cable.</strong>',
			'If you\'re using USB, choose <strong>USB</strong>. If you\'re using a MIDI interface, choose <strong>MIDI</strong>.',
			'Press <strong>Global</strong> again to exit the Global menu.',
		],
		params : {
			banks : {
				bank1 : {label : 'Pro 12 / User Bank 0', address : 0x00},
				bank2 : {label : 'Pro 12 / User Bank 1', address : 0x01},
				bank3 : {label : 'Pro 12 / User Bank 2', address : 0x02},
				bank4 : {label : 'Pro 12 / User Bank 3', address : 0x03},
				// bank5 : {label : 'Pro 2 / Bank 4', address : 0x04},
				// bank6 : {label : 'Pro 2 / Bank 5', address : 0x05},
				// bank7 : {label : 'Pro 2 / Bank 6', address : 0x06},
				// bank8 : {label : 'Pro 2 / Bank 7', address : 0x07},
			},
			patchesPerBank : 99,
			transferPause : 400,
			parserConfig : {
				rawData : {
					from: 6,
					to: 1177 //1171 bytes packed, 1024 expanded
				},
				name : {
					from : 402,
					to : 421,
				},
				dna : {
					from : 0,
					to : 1024
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);
			midiData[3] = 0x03; 	// Edit Buffer Data
			midiData.splice(4, 2);	// Unset Bank and Prg
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[3] = 0x02; 	// Program Data Dump
			midiData[4] = bank;		// 0 - 7
			midiData[5] = prgNo;	// 0 - 98

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x01, 0x2A, 0x05];
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			midiData = Sysex.objectToArray(midiData);
			var unpackedData = this.unpackMidiData(midiData);
			
			// Sysex.dumpData(unpackedData, 16);

			var program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			program.data = midiData;
			return [program];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 20; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, this.params.parserConfig.rawData.from);
			var unpackedData = this.unpackMidiData(midiData);

			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);
			program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			
			var packedData = Sysex.packDSISysex(unpackedData);
			program.data = header.concat(packedData, 0xF7);
			return program;
		},
		unpackMidiData : function(midiData) {
			var packedData = midiData.slice(this.params.parserConfig.rawData.from, this.params.parserConfig.rawData.to);
			var unpackedData = Sysex.unpackDSISysex(packedData);
			// Sysex.dumpData(unpackedData, 16);
			return unpackedData;
		}
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('DSIProphet08', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'DSIProphet08',
		manuf 		: 'Dave Smith Instruments',
		model 		: 'Prophet 08',
		hints 		: [
			'In <strong>Global</strong> menu set <strong>MIDI SysEx</strong> to <strong>on</strong>',
		],
		params : {
			banks : {
				bank1 : {label : 'Prophet 08 / Bank 1', address : 0x00},
				bank2 : {label : 'Prophet 08 / Bank 2', address : 0x01},
			},
			patchesPerBank : 128,
			transferPause : 300,
			parserConfig : {
				rawData : {
					from: 6,
					to: 445 //439 bytes total raw data, 384 unpacked
				},
				name : {
					from : 184,
					to : 199
				},
				dna : {
					from : 0,
					to : 384
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);
			midiData[3] = 0x03; 	// Edit Buffer Data
			midiData.splice(4, 2);	// Unset Bank and Prg
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[3] = 0x02; 	// Program Data Dump
			midiData[4] = bank;		// 0x00 or 0x01
			midiData[5] = prgNo;	// 0-127

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x01, 0x23, 0x05];
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			midiData = Sysex.objectToArray(midiData);
			var unpackedData = this.unpackMidiData(midiData);
			var program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			program.data = midiData;
			return [program];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 16; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, this.params.parserConfig.rawData.from);
			var unpackedData = this.unpackMidiData(midiData);

			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);
			program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			
			var packedData = Sysex.packDSISysex(unpackedData);
			program.data = header.concat(packedData, 0xF7);
			return program;
		},
		unpackMidiData : function(midiData) {
			var packedData = midiData.slice(this.params.parserConfig.rawData.from, this.params.parserConfig.rawData.to);
			var unpackedData = Sysex.unpackDSISysex(packedData);
			// Sysex.dumpData(unpackedData, 16);
			return unpackedData;
		}
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('DSIProphetRev2', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'DSIProphetRev2',
		manuf 		: 'Dave Smith Instruments',
		model 		: 'Prophet Rev2 [beta]',
		hints 		: [
			'Press the Global button on your Rev2.',
			'Use the Parameter knob to select <strong>MIDI sysex cable</strong> then use the Value knob to choose either <strong>USB</strong> or <strong>MIDI</strong>, depending on which port you are using to connect to your computer.',
			'You can only transfer programs from bank U1 to bank U4, higher banks are readable only (ROM).',
			'You can only change the name of Layer A of a program.'
		],
		params : {
			banks : {
				bank1 : {label : 'Rev2 / Bank U1', address : 0x00},
				bank2 : {label : 'Rev2 / Bank U2', address : 0x01},
				bank3 : {label : 'Rev2 / Bank U3', address : 0x02},
				bank4 : {label : 'Rev2 / Bank U4', address : 0x03},
			},
			patchesPerBank : 128,
			transferPause : 1000,
			parserConfig : {
				rawData : {
					from: 6,
					to: 2345 //2339 bytes packed, 2046 expanded
				},
				name : {
					from : 235,
					to : 254,
				},
				dna : {
					from : 0,
					to : 2046
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);
			midiData[3] = 0x03; 	// Edit Buffer Data
			midiData.splice(4, 2);	// Unset Bank and Prg
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[3] = 0x02; 	// Program Data Dump
			midiData[4] = bank;		// 0 - 9
			midiData[5] = prgNo;	// 0 - 99

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x01, 0x2F, 0x05];
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			midiData = Sysex.objectToArray(midiData);
			var unpackedData = this.unpackMidiData(midiData);
			var program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			program.data = midiData;
			return [program];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 20; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, this.params.parserConfig.rawData.from);

			var unpackedData = this.unpackMidiData(midiData);
			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);
			program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			
			var packedData = Sysex.packDSISysex(unpackedData);
			program.data = header.concat(packedData, 0xF7);
			return program;
		},
		unpackMidiData : function(midiData) {
			var packedData = midiData.slice(this.params.parserConfig.rawData.from, this.params.parserConfig.rawData.to);
			var unpackedData = Sysex.unpackDSISysex(packedData);
			// Sysex.dumpData(unpackedData, 16);
			return unpackedData;
		}
	};
	return obj;
}]);

/**
 * ESQ-1 SysEx Header
 * 
 * 11110000 Statusbyte
 * 00001111	Ensoniq ID
 * 00000010	ESQ-1
 * 0000nnnn MIDI Channel
 * 
 * 0xF0 0x0F 0x02 0x00
 *
 * Single Program Dump
 * 
 */


angular.module('f0f7.devices').factory('EnsoniqEsq1', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'EnsoniqEsq1',
		manuf 		: 'Ensoniq',
		model 		: 'ESQ-1',
		hints 		: [
			'ESQ-1 must be in program select mode (i.e. one of the program bank pages must be displayed)',
			'Set MIDI Channel to <strong>1</strong>',
			'MIDI enable parameter on the MIDI page must be set to <strong>ENABLE=KEYS+CT+PC+SS+SX</strong>',
			'This has to be done everytime you switch the ESQ-1 on.',
			'You have to add at least <strong>40</strong> programs to a bank or <strong>Overwrite Bank</strong> will not work',
			'Every time you prelisten a program on the ESQ-1 you have to press <strong>EXIT</strong>, else the next program will not be received'
		],
		params : {
			singleProgramsCannotBeTransmitted : true,
			banks : {
				bank1 : {label : 'ESQ-1 Bank 1', address : 0x00},
			},
			patchesPerBank : 40,
			transferPause : 15,
			buffer : [],
			parserConfig : {
				name : {
					from : 0,
					to : 5
				},
				dna : {
					from : 0,
					to : 204
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			Sysex.dumpData(midiData, 16);

			midiData[4] = 0x01; //Single Program Dump Code

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			if(midiData.length > 0) {
				Sysex.sendSysex(midiData, WebMIDI.midiOut);
			}
		},
		reorderProgram : function(program, bank, prgNo) {
			if(prgNo === 0) {
				this.params.buffer = [];
			}
			var rawData = this.getRawData(Sysex.objectToArray(program.data));
			this.params.buffer = this.params.buffer.concat(rawData);

			if(prgNo == (this.params.patchesPerBank-1)) {
				var header = [0xF0, 0x0F, 0x02, 0x00, 0x02];
				var eox = [0xF7];
				var midiData = header.concat(this.params.buffer, eox);
				this.params.buffer = [];
				return midiData;
			}
			return [];
		},
		retrieveBank : function(bank) {
			var midiData = [0xF0, 0x0F, 0x02, 0x00, 0x0A, 0xF7]; //All Program Dump Request
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			var program = [];

			midiData = Sysex.objectToArray(midiData);

			switch(midiData[4]) {
				case 0x01 : program = this.extractSinglePatch(midiData); break;
				case 0x02 : program = this.extractMultiDump(midiData); break;
			}
			
			return program;
		},
		extractSinglePatch : function(midiData) {
			if(midiData.length == 210) {
				var rawData = this.getRawData(midiData);
				var unpackedData = this.deNibbleData(rawData);

				// Sysex.dumpData(unpackedData, 16);

				var program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
				program.data = midiData;
				return [program];
			} else {
				return [];
			}
		},
		extractMultiDump : function(midiData) {
			var rawData = midiData.slice(5, (midiData.length-1));
			var unpackedData = this.deNibbleData(rawData);

			var chunk = 102;
			var i,j,data;
			var patchNames = [];
			var program = [];
			var header = [0xF0, 0x0F, 0x02, 0x00, 0x01];

			for (i=0,j=unpackedData.length; i<j; i+=chunk) {
				data = unpackedData.slice(i,i+chunk);

				// Sysex.dumpData(data, 16);

				program = Sysex.extractPatchName(data, this.params.parserConfig);
				program.data = header.concat(this.nibbleData(data), [0xF7]);
				
				patchNames.push(program);
			}
			return patchNames;
		},
		getRawData : function(midiData) {
			return midiData.slice(5, 209);
		},
		nibbleData : function(midiData) {
			//converts 1 to 2 bytes
			// xx yy   data, sent nybblewise: 
			//         a data byte dd is sent as xx=dd&0x0F; yy=dd>>4
			
			var nibbledData = [];
			for(var i=0; i<midiData.length; i=i+1) {
				var xx = midiData[i] & 0x0F;
				var yy = midiData[i] >> 4;
				nibbledData.push(xx);
				nibbledData.push(yy);
			}

			return nibbledData;
		},
		deNibbleData : function(midiData) {
			var denibbledData = [];
			for(var i=0; i<midiData.length; i=i+2) {
				var xx = midiData[i] & 0x0F;
				var yy = midiData[i+1] & 0x0F;
				var dd = (yy << 4) | xx;
				denibbledData.push(dd);
			}

			return denibbledData;
		},
		getMaxNameLength : function() {
			return 6;
		},
		renameProgram : function(newName, program) {			
			newName = newName.toUpperCase();
			var nameLength = this.getMaxNameLength();

			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);
			var rawData = this.getRawData(Sysex.objectToArray(program.data));
			var unpackedData = this.deNibbleData(rawData);

			var renamedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);

			var nibbledData = this.nibbleData(renamedData);
			var header = [0xF0, 0x0F, 0x02, 0x00, 0x01];
			midiData = header.concat(nibbledData, [0xF7]);

			program = Sysex.extractPatchName(renamedData, this.params.parserConfig);
			program.data = midiData;
			return program;
		},
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('IntellijelPlonk', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'IntellijelPlonk',
		manuf 		: 'Intellijel',
		model 		: 'Plonk',
		hints 		: [
			'Click the <strong>Request Programs</strong> button below to receive all presets from Plonk.',
			'or',
			'Click the <strong>CONFIG</strong> button and turn the <strong>ENCODER</strong> till the display reads "Send Presets".',
			'Click the <strong>ENCODER</strong>. If you haven’t yet connected the USB cable to the back of the module the display will read "Connect USB". Once the USB cable is connected it will read "Click ENC to begin".',
			'Click the <strong>ENCODER</strong> to begin SysEx transmission.',
			'Once LASER Mammoth has received 128 messages and the display reads "Presets Sent" the preset transfer process is complete. You can now push any button to exit this menu.'
		],
		params : {
			banks : {
				bank1 : {label : 'Plonk Bank', address : 0x01},
			},
			patchesPerBank : 128,
			transferPause : 350,
			parserConfig : {
				rawData : {
					from: 7,
					to: 117
				},
				name : {
					from : 1,
					to : 11
				},
				dna : {
					from : 7,
					to : 117
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);
			midiData[5] = 0x03; 	// Edit Buffer Data
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			// To upload a single preset (from the computer to the Plonk module):
			// 0xF0, 0x00, 0x02, 0x14, 0x00, 0xNN, 0xDD ... 0xCC 0xF7
			// where 0xDD ... are the data bytes containing the preset parameter values.
			var midiData = Sysex.objectToArray(program.data);
			midiData[6] = prgNo;

			var unpackedData = this.unpackMidiData(midiData);
			midiData[midiData.length-2] = Sysex.calculateRolandChecksum(unpackedData);

			return midiData;
		},
		retrieveBank : function(bank) {
			//To download all 128 presets at once: 0xF0, 0x00, 0x02, 0x14, 0x02, 0xF7
			//There will be a series of 128 responses as when downloading a single preset. 
			var midiData = [0xF0, 0x00, 0x02, 0x14, 0x20, 0x02, 0xF7];
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			midiData = Sysex.objectToArray(midiData);
			var unpackedData = this.unpackMidiData(midiData);
			var program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			program.data = midiData;
			return [program];
		},
		getMaxNameLength : function() {
			return 11;
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, this.params.parserConfig.rawData.from);
			var unpackedData = this.unpackMidiData(midiData);

			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);

			var packedData = Sysex.packDSISysex(unpackedData);

			var checksum = Sysex.calculateRolandChecksum(unpackedData);
			var eox = [checksum, 0xF7];

			midiData = header.concat(packedData, eox);
			program = this.extractPatchNames(midiData);

			return program[0];
		},
		unpackMidiData : function(midiData) {
			this.setParseConfig(midiData);
			var packedData = midiData.slice(this.params.parserConfig.rawData.from, this.params.parserConfig.rawData.to);
			return Sysex.unpackDSISysex(packedData);
		},
		setParseConfig : function(midiData) {
			this.params.parserConfig.rawData.to = this.params.parserConfig.dna.to = midiData.length - 2;
		}
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('IntellijelRainmaker', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'IntellijelRainmaker',
		manuf 		: 'Intellijel Cylonix',
		model 		: 'Rainmaker',
		hints 		: [
			'Enable <strong>USB/MIDI</strong> mode first by pushing the <strong>SAVE & LOAD</strong> buttons on the front panel simultaneously'
		],
		params : {
			banks : {
				bank1 : {label : 'Rainmaker Bank', address : 0x01},
			},
			patchesPerBank : 128,
			transferPause : 150,
			parserConfig : {
				rawData : {
					from: 6,
					to: 153
				},
				name : {
					from : 116,
					to : 126
				},
				dna : {
					from : 6,
					to : 153
				}
			}
		},
		rmCharset : [
				' ',
				'A',
				'B',
				'C',
				'D',
				'E',
				'F',
				'G',
				'H',
				'I',
				'J',
				'K',
				'L',
				'M',
				'N',
				'O',
				'P',
				'Q',
				'R',
				'S',
				'T',
				'U',
				'V',
				'W',
				'X',
				'Y',
				'Z',
				'a',
				'b',
				'c',
				'd',
				'e',
				'f',
				'g',
				'h',
				'i',
				'j',
				'k',
				'l',
				'm',
				'n',
				'o',
				'p',
				'q',
				'r',
				's',
				't',
				'u',
				'v',
				'w',
				'x',
				'y',
				'z',
				'0',
				'1',
				'2',
				'3',
				'4',
				'5',
				'6',
				'7',
				'8',
				'9',
				'!',
				'"',
				'#',
				'$',
				'%',
				'&',
				'\'',
				'(',
				')',
				'*',
				'+',
				',',
				'-',
				'.',
				'/',
				':',
				';',
				'<',
				'=',
				'>',
				'?',
				'@',
				'[',
				'\\',
				']',
				'^',
				'_',
				'`',
				'{',
				'|',
				'}',
				'~',
				' ',
		],
		sendToBuffer : function(program) {
			$log.log("not possible to send programs to the edit buffer on Rainmaker");
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			// To upload a single preset (from the computer to the Rainmaker module):
			// 0xF0, 0x00, 0x02, 0x14, 0x00, 0xNN, 0xDD ... 0xCC 0xF7
			// where 0xDD ... are the data bytes containing the preset parameter values.
			var midiData = Sysex.objectToArray(program.data);
			midiData[5] = prgNo;

			var unpackedData = this.unpackMidiData(midiData);
			midiData[midiData.length-2] = Sysex.calculateRolandChecksum(unpackedData);

			return midiData;
		},
		retrieveBank : function(bank) {
			//To download all 128 presets at once: 0xF0, 0x00, 0x02, 0x14, 0x02, 0xF7
			//There will be a series of 128 responses as when downloading a single preset. 
			var midiData = [0xF0, 0x00, 0x02, 0x14, 0x02, 0xF7];
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			var parserConfig = this.params.parserConfig;
			midiData = Sysex.objectToArray(midiData);
			
			var unpackedData = this.unpackMidiData(midiData);
			var nameArray = unpackedData.slice(parserConfig.name.from, parserConfig.name.to);

			var patchName = this.nameArrayToString(nameArray);

			var dna = Sysex.createDNA(midiData, parserConfig.dna.from, parserConfig.dna.to);
			return [{name : patchName, dna : dna, data : midiData}];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from; //return 10; 
		},
		nameArrayToString : function(nameArray) {
			var patchName = '';
			for(var i=0; i<nameArray.length; i++) {
			  patchName += this.mapRainmakerHexToASCII(nameArray[i]);
			}

			patchName = patchName.trim();
			if(patchName.length === 0) {
				patchName = 'no name';
			}

			return patchName;
		},
		mapRainmakerHexToASCII : function(hex) {
			if(typeof this.rmCharset[hex] !== 'undefined') {
				return this.rmCharset[hex];	
			} else {
				return ' ';
			}
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = this.createPaddedRMArray(newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, this.params.parserConfig.rawData.from);
			var unpackedData = this.unpackMidiData(midiData);

			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);
			
			var packedData = Sysex.packDSISysex(unpackedData);

			var checksum = Sysex.calculateRolandChecksum(unpackedData);
			var eox = [checksum, 0xF7];

			midiData = header.concat(packedData, eox);
			program = this.extractPatchNames(midiData);

			return program[0];
		},
		unpackMidiData : function(midiData) {
			var packedData = midiData.slice(this.params.parserConfig.rawData.from, this.params.parserConfig.rawData.to);
			return Sysex.unpackDSISysex(packedData);
		},
		createPaddedRMArray : function (str) {
			var nameLength = this.getMaxNameLength();
			var bytes = [];
			var charCode, index;
			
			for (var i = 0; i < nameLength; i++) {
			  if(typeof str[i] === 'undefined') {
			  	index = 0; //index of space
			  } else {
			  	index = this.rmCharset.indexOf(str[i]);
			  }
			  bytes.push(index);
			}
		  	return bytes;
		},
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('KorgDw8000', ['$log', '$timeout', 'Sysex', 'WebMIDI', function($log, $timeout, Sysex, WebMIDI) {
	var patchData = {};
	var dna;
	var obj = {
		id 			: 'KorgDw8000',
		manuf 		: 'Korg',
		model 		: 'DW-8000',
		hints 		: [
			'Make sure parameter 84 (MIDI channel) is set to 1.',
			'Make sure parameter 85 (Enable) is set to 2 (All).',
			'The Write switch on the back panel must be in the enable position.',
		],
		params : {
			deviceId	: 0x30, //= Format ID 3*H 0 to 15 
			banks : {
				bank1 : {label : 'Internal', address : 0x00},
			},
			patchesPerBank : 64,
			transferPause : 600,
			parserConfig : {
				dna : {
					from : 5,
					to : 57
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data.DataDump);
			midiData[2] = obj.params.deviceId; 	// DeviceId

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			//Send Program Change
			var prgChange = [0xC0, prgNo & 0x7F]; //1100nnnn 0ppppppp
			Sysex.sendSysex(prgChange, WebMIDI.midiOut);

			//Send Bulk Data
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);

			//Send Write Request
			var writeRequest = [0xF0, 0x42, this.params.deviceId, 0x03, 0x11, prgNo, 0xF7];
			Sysex.sendSysex(writeRequest, WebMIDI.midiOut);			
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data.DataDump);
			midiData[2] = this.params.deviceId;
			return midiData;
		},
		retrieveBank : function(bank) {
			this.requestAllProgramsOfBank(this.params.patchesPerBank, this.params.transferPause);
		},
		requestAllProgramsOfBank : function(patchesPerBank, transferPause) {
			var dumpRequest = [];
			var prgChange = [];
			var prgNo = 0;

			var delayedDumpRequest = function(prgNo) {
				$log.debug("Requesting Prg " + prgNo);

				//Send Program Change
				prgChange = [0xC0, prgNo & 0x7F]; //1100nnnn 0ppppppp
				Sysex.sendSysex(prgChange, WebMIDI.midiOut);
				$log.log('prgChange ' , prgChange);

				//Data Save Request
				dumpRequest = [0xF0, 0x42, obj.params.deviceId, 0x03, 0x10, 0xF7];
				Sysex.sendSysex(dumpRequest, WebMIDI.midiOut);
				$log.log('dumpRequest ' , dumpRequest);

				if(prgNo < (patchesPerBank-1)) {
					prgNo++;
					$timeout(delayedDumpRequest, transferPause, true, prgNo);
				} else {
					$log.info("Bank dumpRequest completed");
				}
			};
			delayedDumpRequest(prgNo);
		},
		extractPatchNames : function(midiData) {
			var program = [];
			var data;
			var dna;

			switch(midiData[4]) {
				case 0x40 : //DATA Dump
					data = Sysex.objectToArray(midiData);
					dna = Sysex.createDNA(data, this.params.parserConfig.dna.from, this.params.parserConfig.dna.to);
					program = [{name : 'DW-8000 Program', dna : dna, data : { DataDump : data}}];
					break;
			}
			return program;
		},
		getMaxNameLength : function() {
			return 0;
		}
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('KorgMicrokorg', ['$log', '$timeout', 'Sysex', 'WebMIDI', function($log, $timeout, Sysex, WebMIDI) {
	var patchData = {};
	var dna;
	var obj = {
		id 			: 'KorgMicrokorg',
		manuf 		: 'Korg',
		model 		: 'microKORG / MS2000',
		hints 		: [
			'<strong>Set MIDI CH to 1</strong>',
			'<strong>Turn off write protect</strong>',
			'Hold down the <em>SHIFT</em> key and press the <em>8</em> key. The display will indicate <strong>ūtP<strong>',
			'Turn knob <em>1/Cutoff</em> to switch write protect',
			'<strong>Set the MIDI FILTER setting SYSTEM EXCLUSIVE to Enable (E-E).</strong>',
			'Hold down <em>SHIFT</em> and press Program Number 4. Display shows <strong>FLt.</strong>',
			'Turn knob <em>4/EG Release</em> to select <strong>E-E</strong>',
			'Click <strong>Request Programs</strong> below to transfer programs from the microKorg',
			'Alternatively you can send programs from the microKorg by executing a MIDI DATA DUMP',
			'To do that hold down the <em>SHIFT</em> key and press the <em>6</em> key.',
			'Select <strng>Pr9</strong> (all programs) by turning the <em>1/Cutoff</em> knob',
			'Dump the data by pressing the <em>6</em> key'
		],
		params : {
			// singleProgramsCannotBeTransmitted : true,
			deviceId	: 0x30, //= Format ID 3*H 0 to 15 
			banks : {
				bank1 : {label : 'Internal', address : 0x00},
			},
			patchesPerBank : 128,
			transferPause : 600,
			parserConfig : {
				name : {
					from : 0,
					to : 11
				},
				dna : {
					from : 0,
					to : 254
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);
			var header = [0xF0, 0x42, this.params.deviceId, 0x58, 0x40];//Microkorg CURRENT PROGRAM DATA DUMP
			var eox = [0xF7];
			
			midiData = Sysex.packDSISysex(midiData);
			midiData = header.concat(midiData, eox);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			//Send Program Change
			// var prgChange = [0xC0, prgNo & 0x7F]; //1100nnnn 0ppppppp
			// Sysex.sendSysex(prgChange, WebMIDI.midiOut);

			//Send Bulk Data
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);

			// //Send Write Request Microkorg
			// var writeRequest = [0xF0, 0x42, this.params.deviceId, 0x58, 0x11, 0x00, prgNo, 0xF7];
			// Sysex.sendSysex(writeRequest, WebMIDI.midiOut);			
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);
			
			var header = [0xF0, 0x42, this.params.deviceId, 0x58, 0x4C];//Microkorg PROGRAM DATA DUMP
			// var header = [0xF0, 0x42, this.params.deviceId, 0x58, 0x40];//Microkorg CURRENT PROGRAM DATA DUMP
			var eox = [0xF7];
			midiData = Sysex.packDSISysex(midiData);
			midiData = header.concat(midiData, eox);

			var writeRequest = [0xF0, 0x42, this.params.deviceId, 0x58, 0x11, 0x00, prgNo, 0xF7];
			midiData = midiData.concat(writeRequest);

			return midiData;
		},
		retrieveBank : function(bank) {
			//Microkorg PROGRAM DATA DUMP REQUEST
			var midiData = [0xF0, 0x42, this.params.deviceId, 0x58, 0x1C, 0xF7];
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			var program = [];
			var data;
			var dna;

			midiData = Sysex.objectToArray(midiData);

			switch(midiData[4]) {
				//CURRENT PROGRAM DATA DUMP
				case 0x40 : program = this.extractSinglePatch(midiData); break;
				//PROGRAM DATA DUMP
				case 0x4C : program = this.extractMultiDump(midiData); break;
				//ALL DATA DUMP
				case 0x50 : program = this.extractMultiDump(midiData); break;
			}
			return program;
		},
		extractMultiDump : function(midiData) {
			var packedData = midiData.slice(5, 37391);
			var unpackedData = Sysex.unpackDSISysex(packedData);

			var chunk = 254;
			var i,j,data;
			var patchNames = [];

			for (i=0,j=unpackedData.length; i<j; i+=chunk) {
				data = unpackedData.slice(i,i+chunk);
				if(data.length == 254) {
					patchNames.push(Sysex.extractPatchName(data, this.params.parserConfig));
				}
			}
			return patchNames;
		},
		extractSinglePatch : function(midiData) {
			var packedData = midiData.slice(5, 296);
			var unpackedData = Sysex.unpackDSISysex(packedData);

			return [Sysex.extractPatchName(unpackedData, this.params.parserConfig)];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 12; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			return Sysex.renameProgram(newName, nameLength, program.data, this.params.parserConfig);
		}
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('MoogVoyager', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'MoogVoyager',
		manuf 		: 'Moog',
		model 		: 'Voyager',
		hints 		: [
			'In <strong>Master</strong> menu set <strong>SysEx Device ID</strong> to <strong>1</strong>',
		],
		params : {
			deviceId	: 0x00,
			banks : {
				bank1 : {label : 'Switch Banks on the Voyager!', address : 0x00},
			},
			patchesPerBank : 128,
			transferPause : 300,
			parserConfig : {
				name : {
					from : 84,
					to : 107
				},
				dna : {
					from : 0,
					to : 128
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);
			midiData = Sysex.packMoogSysex(midiData, [0xF0, 0x04, 0x01, this.params.deviceId, 0x02]); //Voyager Panel Dump.

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);
			midiData = Sysex.packMoogSysex(midiData, [0xF0, 0x04, 0x01, this.params.deviceId, 0x03, prgNo]); //Voyager Single Preset Dump.

			return midiData;
		},
		retrieveBank : function(bank) {
			var midiData = [0xF0, 0x04, 0x01, this.params.deviceId, 0x04, 0xF7]; //All Presets Dump Request
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			var headerLength;
			var parserConfig = this.params.parserConfig;
			if(midiData[4] == 0x03) { //Voyager Single Preset Dump. Multiple programs in dump.
				headerLength = 6;
				midiData = Sysex.unpackMoogSysex(midiData, headerLength);
				return [Sysex.extractPatchName(midiData, parserConfig)];

			} else {
				headerLength = 5;
				var ignoreLeadingBytes = 3;
				var chunk = 128;

				midiData = Sysex.unpackMoogSysex(midiData, headerLength);

				var i,j,data;
				var patchNames = [];

				for (i=ignoreLeadingBytes,j=midiData.length; i<j; i+=chunk) {
					data = midiData.slice(i,i+chunk);
					patchNames.push(Sysex.extractPatchName(data, parserConfig));
				}
				return patchNames;
			}
		},
		getDeviceId : function(midiData) {
			return midiData[3];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 24; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			return Sysex.renameProgram(newName, nameLength, program.data, this.params.parserConfig);
		}
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('NovationBassstation2', ['$log', 'Sysex', '$timeout', 'WebMIDI', function($log, Sysex, $timeout, WebMIDI) {
	var obj = {
		id 			: 'NovationBassstation2',
		manuf 		: 'Novation',
		model 		: 'Bass Station 2',
		hints 		: [
			'Be sure to have a current firmware on your <em>Bass Station 2</em>',
			'Devices with older firmware cannot store program names'
		],
		params : {
			deviceId	: 0x00, 
			banks : {
				bank1 : {label : 'Bank A', address : 0x01},
			},
			patchesPerBank : 128,
			transferPause : 150,
			parserConfig : {
				name : {
					from : 137,
					to : 152
				},
				dna : {
					from : 11,
					to : 153
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			// midiData[5] = this.params.deviceId;
			midiData[7] = 0x00; //Buffermode?
			midiData[8] = 0x00; //ProgramNumber Single buffer

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			// midiData[5] = this.params.deviceId;
			midiData[7] = 0x01; //Dumpmode?
			midiData[8] = prgNo; //Prg No

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x00, 0x20, 0x29, 0x00, 0x33, 0x00, 0x41];

			this.requestAllProgramsOfBank(header, this.params.patchesPerBank, this.params.transferPause);
		},
		requestAllProgramsOfBank : function(header, patchesPerBank, transferPause) {
			var dumpRequest = [];
			var prgNo = 0;

			var delayedDumpRequest = function(prgNo) {
				$log.debug("Requesting Prg " + prgNo);
				dumpRequest = header.concat([prgNo, 0xF7]);
				Sysex.sendSysex(dumpRequest, WebMIDI.midiOut);

				if(prgNo < (patchesPerBank-1)) {
					prgNo++;
					$timeout(delayedDumpRequest, transferPause, true, prgNo);
				} else {
					$log.info("Bank dumpRequest completed");
				}
			};
			delayedDumpRequest(prgNo);
		},
		extractPatchNames : function(midiData) {
			return [Sysex.extractPatchName(midiData, this.params.parserConfig)];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return ?; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var midiData = Sysex.objectToArray(program.data);
			return Sysex.renameProgram(newName, nameLength, midiData, this.params.parserConfig);
		}
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('OberheimOb8', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'OberheimOb8',
		manuf 		: 'Oberheim',
		model 		: 'OB-8',
		hints 		: [
			'Enter <strong>Page 2</strong> mode and activate <strong>Group A</strong> button to enable <em>program dump</em>.',
			'In <strong>Page 2</strong> mode hold <strong>WRITE</strong> to dump the current STORED program.',
			'Or just click <strong>Request Programs</strong> to receive all programs.',
			'It\'s not possible to send programs to the edit buffer of OB-8',
			'Data from an OB-8 retrofitted with an Encore interface currently can only be received but not sent.'
		],
		params : {
			banks : {
				bank1 : {label : 'OB-8 Bank', address : 0x00},
			},
			patchesPerBank : 120,
			transferPause : 500,
			parserConfig : {
				dna : {
					from : 5,
					to : 59
				}
			}
		},
		sendToBuffer : function(program) {
			//Does not work on OB-8?
			var midiData = Sysex.objectToArray(program.data);

			Sysex.dumpData(midiData, 16);

			// midiData[3] = 0x0D; //Single Patch Data to Edit Buffer
			// midiData[4] = 0x00;

			// Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[3] = 0x01; //Command Byte 1: Transmit a Single Patch
			midiData[4] = prgNo; //Command Byte 2: Patch Number to transmit

			return midiData;
		},
		retrieveBank : function(bank) {		
			var header = [0xF0, 0x10, 0x01];
			bank = 0x00; // this is a fake bank. the "program data dump request" value
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			var headerEncoreInterface = [0xF0, 0x00, 0x00, 0x2F, 0x04];
			var headerOberheimInterface = [0xF0, 0x10, 0x01];

			var data = Sysex.objectToArray(midiData);
			var receivedHeader = data.slice(0, 5);

			if(angular.equals(receivedHeader, headerEncoreInterface)) {
				console.log("Data includes headerdata of Encore MIDI interface.");
				data.splice(0,5);
				data = headerOberheimInterface.concat(data);
				// console.log("Normalized OB8 data", data);
			}
			var dna = Sysex.createDNA(data, this.params.parserConfig.dna.from, this.params.parserConfig.dna.to);
			return [{name : 'OB-8 preset', dna : dna, data : data}];
		}
	};
	return obj;
}]);

// Single Patch Data Format
// Statistics:     134 Bytes/Single Patch
// = 268 nybbles transmitted + 5 bytes Header + 1 byte Checksum + 1
// byte EOX
// = 275 total transmitted bytes/Single Patch
// 
// 
// * single patch
// 0x01    opcode
// pp      patch number, 0-99.
// xx yy   data, sent nybblewise: 
//         a data byte dd is sent as xx=dd&0x0F; yy=dd>>4
// cc      checksum:
//         the sum of the original data bytes dd, masked with 0x7F.

// Here I don't see why they chop up the data bytes; none exceeds 7 bits.

// * patch data format
// byte    param   bits    description
//  0-7    n/a     6 each  patch name: ASCII clipped to 6 bits.
//  
//  request all                  F0 10 06 04 0 0 F7
// request single               F0 10 06 04 1 FD F7   
// request split patch          F0 10 06 04 2 FD F7
// request master parameters    F0 10 06 04 3 0 F7 

angular.module('f0f7.devices').factory('OberheimMatrix6', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'OberheimMatrix6',
		manuf 		: 'Oberheim',
		model 		: 'Matrix 6 / 6R / 1000',
		hints 		: [
			'Matrix 1000 only transfers 100 programs a time.',
			'The program # you select on the Matrix 1000 defines which block of 100 programs will be transferred',
			'E.g. if you want to transfer program 300 - 399, first select program 300 on the device',
			'You can copy Matrix 6 programs to Matrix 1000 and via versa',
			'LASER Mammoths rename function has no effect on Matrix 1000. Don\'t use it.'
		],
		params : {
			banks : {
				bank1 : {label : 'Matrix Bank', address : 0x00},
			},
			patchesPerBank : 100,
			transferPause : 40,
			parserConfig : {
				name : {
					from : 0,
					to : 8
				},
				dna : {
					from : 5,
					to : 273
				}
			}
		},
		sixBitCharset : [
			'@','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','[','\\',']','^','_',' ','!','"','#','$','%','&','\'','(',')','*','+',',','-','.','/','0','1','2','3','4','5','6','7','8','9',':',';','<','=','>','?'
		],
		sendToBuffer : function(program) {
			// 0DH - Single Patch Data to Edit Buffer
			// F0H 10H 06H 0DH 0 <patch data> <checksum> F7H
			// <data>     = patch data unpacked to two nibbles per byte 
			//              see patch format listing
			// <checksum> = sum of packed (not transmitted) <data> 
			// NOTE: On receipt, this data will be stored into the edit buffer.
			// NOTE: Wait at least ten msec after sending a patch to the Matrix-1000.
			var midiData = Sysex.objectToArray(program.data);

			// Sysex.dumpData(midiData, 16);

			midiData[3] = 0x0D; //Single Patch Data to Edit Buffer
			midiData[4] = 0x00;

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			// 01H-SINGLE PATCH DATA
			// F0H 10H 06H 01H <num> <patch data> <checksum> F7H

			var midiData = Sysex.objectToArray(program.data);

			midiData[3] = 0x01; //Opcode 1: Transmit a Single Patch
			midiData[4] = prgNo; //pp     Patch Number to transmit, 0 P 99 for Single

			return midiData;
		},
		retrieveBank : function(bank) {		
			// Byte     Function

			//     04H     Opcode
			//     xx     Code indicating what to transmit:
			//               0: Transmit all Single Patches, Splits, and Master parameters
			//               1: Transmit a Single Patch
			//               2: Transmit a Split
			//               3: Transmit Master parameters
			//     pp     Patch Number to transmit, 0 P 99 for Single
			// Patches, 0 P 49 for Splits.

			// This byte is ignored for Transmit Master Parameters and Transmit All
			// requests, but but must be      included to pad out the fixed-length message.

			//  Transmit all 
			// var midiData = [0xF0, 0x10, 0x06, 0x04, 0x00, 0x00, 0xF7];
			// Sysex.sendSysex(midiData, WebMIDI.midiOut);

			var header = [0xF0, 0x10, 0x06, 0x04];
			bank = 0x01; // this is a fake bank. the "type" value
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			var parserConfig = this.params.parserConfig;

			midiData = Sysex.objectToArray(midiData);
			if(midiData.length == 275) {
				var rawData = this.getRawData(midiData);
				var unpackedData = this.deNibbleData(rawData);

				var nameArray = unpackedData.slice(parserConfig.name.from, parserConfig.name.to);
				var patchName = this.nameArrayToString(nameArray);

				var dna = Sysex.createDNA(midiData, parserConfig.dna.from, parserConfig.dna.to);
				return [{name : patchName, dna : dna, data : midiData}];
			} else {
				return [];
			}
		},
		getRawData : function(midiData) {
			return midiData.slice(5, 273);
		},
		nibbleData : function(midiData) {
			//converts 1 to 2 bytes
			// xx yy   data, sent nybblewise: 
			//         a data byte dd is sent as xx=dd&0x0F; yy=dd>>4
			
			var nibbledData = [];
			for(var i=0; i<midiData.length; i=i+1) {
				var xx = midiData[i] & 0x0F;
				var yy = midiData[i] >> 4;
				nibbledData.push(xx);
				nibbledData.push(yy);
			}

			return nibbledData;
		},
		deNibbleData : function(midiData) {
			var denibbledData = [];
			for(var i=0; i<midiData.length; i=i+2) {
				var xx = midiData[i] & 0x0F;
				var yy = midiData[i+1] & 0x0F;
				var dd = (yy << 4) | xx;
				denibbledData.push(dd);
			}

			return denibbledData;
		},
		getMaxNameLength : function() {
			return 8; 
		},
		nameArrayToString : function(nameArray) {
			var patchName = '';
			for(var i=0; i<nameArray.length; i++) {
			  patchName += this.mapOberheimHexToASCII(nameArray[i]);

			  // if(typeof nameArray[i] !== 'undefined') {
			  // 	nameArray[i] = (nameArray[i] | 0x40);
			  // 	if(nameArray[i] > 95) {
			  // 		nameArray[i] = nameArray[i] & 0x3f;
			  // 	}
			  // 	if(nameArray[i] < 32 || nameArray[i] > 127) {
			  // 		nameArray[i] = 32;
			  // 	}
			  // 	patchName += String.fromCharCode(nameArray[i]);
			  // }
			}

			patchName = patchName.trim();
			// patchName = patchName.replace(/\s+$/,""); //rtrim
			if(patchName.length === 0) {
				patchName = 'no name';
			}

			return patchName;
		},
		mapOberheimHexToASCII : function(hex) {
			if(typeof this.sixBitCharset[hex] !== 'undefined') {
				return this.sixBitCharset[hex];
			} else {
				return ' ';
			}
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = this.createPaddedArrayFromCharset(newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, 5);

			var rawData = this.getRawData(midiData);
			var unpackedData = this.deNibbleData(rawData);
			// console.log('unpackedData ' , unpackedData);
			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);

			// console.log('newNameArray ' , newNameArray);

			var packedData = this.nibbleData(unpackedData);

			var checksum = Sysex.calculateOberheimChecksum(unpackedData);
			var eox = [checksum, 0xF7];

			midiData = header.concat(packedData, eox);

			program = this.extractPatchNames(midiData);

			return program[0];
		},
		createPaddedArrayFromCharset : function (str) {
			var nameLength = this.getMaxNameLength();
			var bytes = [];
			var charCode, index;
			
			for (var i = 0; i < nameLength; i++) {
			  if(typeof str[i] === 'undefined') {
			  	index = 32; //index of space
			  } else {
			  	index = this.sixBitCharset.indexOf(str[i].toUpperCase());
			  }
			  bytes.push(index);
			}
		  	return bytes;
		}
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('RolandAlphajuno', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'RolandAlphajuno',
		manuf 		: 'Roland',
		model 		: 'Alpha Juno [Experimental]',
		hints 		: [
			'Set the MEMORY PROTECT SWITCH on the back of the keyboard to the OFF position',
			'Press [ MIDI ] and the display will read MIDI CH = #',
			'Keep pressing [ MIDI ] and turn the α-Dial until all of the settings look like this:',
			'MIDI CHANNEL= 1 / MIDI OMNI = OFF / MIDI LOCAL = ON / MIDI EXCL =ON / MIDI PROG C = ON',
			'Press [ DATA TRANSFER ] - [ WRITE ] - [ BULK LOAD ] all at the same time until the display reads Bulk Load MIDI.o (Notice the lowercase "o" !)'
		],
		params : {
			deviceId	: 0x00, //Unit # = MIDI Base Ch 0 - 15
			singleProgramsCannotBeTransmitted : true,
			banks : {
				bank1 : {label : 'Memory', address : 0x01},
			},
			patchesPerBank : 64,
			transferPause : 300,
			buffer 		: [],
			toneNo : 0,
			parserConfig : {
				name : {
					from : 21,
					to : 31
				},
				dna : {
					from : 0,
					to : 31
				}
			}
		},
		rolandCharset : [
			'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
			'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
			'0','1','2','3','4','5','6','7','8','9',' ','-'
		],
		sendToBuffer : function(program) {
			var header = [0xF0, 0x41, 0x35, this.params.deviceId, 0x23, 0x20, 0x01];
			var rawData = Sysex.objectToArray(program.data);
			var expandedData = this.expand(rawData);
			var eox = [0xF7];
			var midiData = header.concat(expandedData, eox);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			if(midiData.length > 0) {
				Sysex.sendSysex(midiData, WebMIDI.midiOut);
			}
		},
		reorderProgram : function(program, bank, prgNo) {
			if(prgNo === 0) {
				this.params.buffer = [];
				this.params.toneNo = 0;
			}
			var rawData = Sysex.objectToArray(program.data);
			this.params.buffer = this.params.buffer.concat(rawData);
			this.params.toneNo = this.params.toneNo + 1;

			if(this.params.toneNo % 4 === 0) { //Alpha Juno groups 4 tones in one dump
				var header = [0xF0, 0x41, 0x37, this.params.deviceId, 0x23, 0x20, 0x01, 0x00, (this.params.toneNo - 4)];
				var eox = [0xF7];
				
				var packedData = this.nibbleData(this.params.buffer);

				var midiData = header.concat(packedData, eox);
				this.params.buffer = [];
				return midiData;
			}
			return [];
		},
		retrieveBank : function(bank) {
			var midiData = [0xF0, 0x41, 0x41, this.params.deviceId, 0x23, 0xF7]; //Request a file RQF
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			var program = [];
			midiData = Sysex.objectToArray(midiData);

			switch(midiData[2]) {
				case 0x40 : this.sendACK(); break; //WSF : Want to sendfile
				case 0x37 : program = this.extractMultiDump(midiData); break;
				case 0x42 : program = this.extractMultiDump(midiData); 
					this.sendACK();
					break; //DAT
				case 0x45 : this.sendACK(); break; //EOF
			}
			return program;
		},
		extractMultiDump : function(midiData) {
			var parserConfig = this.params.parserConfig;
			if(midiData.length == 266) {
				//Bulk Dump contains 4 32 Bit tones
				var rawData = this.getRawData(midiData);
				var unpackedData = this.deNibbleData(rawData);

				var chunk = 32;
				var tone = [];
				var tones = [];

				for (i=0,j=unpackedData.length; i<j; i+=chunk) {
					tone = unpackedData.slice(i,i+chunk);
					var nameArray = tone.slice(parserConfig.name.from, parserConfig.name.to);
					var patchName = this.nameArrayToString(nameArray);
					var dna = Sysex.createDNA(tone, parserConfig.dna.from, parserConfig.dna.to);

					tones.push({name : patchName, dna : dna, data : tone});
				}
				return tones;

			} else {
				return [];
			}
		},
		sendACK : function() {
			console.log("Send ACK");
			var ack = [0xF0, 0x41, 0x43, this.params.deviceId, 0x23, 0xF7]; //ACK
			Sysex.sendSysex(ack, WebMIDI.midiOut);
		},
		getRawData : function(midiData) {
			return midiData.slice(9, 265);
		},
		nibbleData : function(midiData) {
			//converts 1 to 2 bytes
			// xx yy   data, sent nybblewise: 
			//         a data byte dd is sent as xx=dd&0x0F; yy=dd>>4
			
			var nibbledData = [];
			for(var i=0; i<midiData.length; i=i+1) {
				var xx = midiData[i] & 0x0F;
				var yy = midiData[i] >> 4;
				nibbledData.push(xx);
				nibbledData.push(yy);
			}

			return nibbledData;
		},
		deNibbleData : function(midiData) {
			var denibbledData = [];
			for(var i=0; i<midiData.length; i=i+2) {
				var xx = midiData[i] & 0x0F;
				var yy = midiData[i+1] & 0x0F;
				var dd = (yy << 4) | xx;
				denibbledData.push(dd);
			}

			return denibbledData;
		},
		setDnaParams : function(midiData) {
			this.params.parserConfig.dna.to = midiData.length-2; //different dna for virus a,b,c and ti
		},
		getMaxNameLength : function() {
			return 10;
		},
		nameArrayToString : function(nameArray) {
			var patchName = '';
			for(var i=0; i<nameArray.length; i++) {
				patchName += this.mapRolandHexToASCII(nameArray[i] & 0x3F);
			}
			patchName = patchName.trim();
			if(patchName.length === 0) {
				patchName = 'no name';
			}
			return patchName;
		},
		mapRolandHexToASCII : function(hex) {
			if(typeof this.rolandCharset[hex] !== 'undefined') {
				return this.rolandCharset[hex];
			} else {
				return ' ';
			}
		},
		renameProgram : function(newName, program) {
			var parserConfig = this.params.parserConfig;
			var nameLength = this.getMaxNameLength();
			var newNameArray = this.createPaddedArrayFromCharset(newName);

			var midiData = Sysex.objectToArray(program.data);

			for (var i = 0; i < newNameArray.length; i++) {
				midiData[i+21] = (midiData[i+21] & 0xC0) | (newNameArray[i] & 0x3F);
			}
			
			var renamedPrg = {};

			renamedPrg.name = this.nameArrayToString(newNameArray);
			renamedPrg.dna = Sysex.createDNA(midiData, parserConfig.dna.from, parserConfig.dna.to);
			renamedPrg.data = midiData;

			return renamedPrg;
		},
		createPaddedArrayFromCharset : function (str) {
			var nameLength = this.getMaxNameLength();
			var bytes = [];
			var charCode, index;
			
			for (var i = 0; i < nameLength; i++) {
			  if(typeof str[i] === 'undefined') {
			  	index = 62; //index of space
			  } else {
			  	index = this.rolandCharset.indexOf(str[i]);
			  }
			  bytes.push(index);
			}
		  	return bytes;
		},
		expand : function(midiData) {
			var expandedData = [];
			
			var b01 = (midiData[5] & 0x80) >> 6;
			var b02 = (midiData[6] & 0x80) >> 7;
			expandedData.push(b01 | b02); //DCO Env Mode

			var b03 = (midiData[7] & 0x80) >> 6;
			var b04 = (midiData[8] & 0x80) >> 7;
			expandedData.push(b03 | b04); //VCF Env Mode

			var b05 = (midiData[9] & 0x80) >> 6;
			var b06 = (midiData[10] & 0x80) >> 7;
			expandedData.push(b05 | b06); //VCA Env Mode

			var b13 = (midiData[17] & 0x80) >> 6;
			var b14 = (midiData[18] & 0x80) >> 7;
			expandedData.push(b13 | b14); //DCO W Pulse

			var b10 = (midiData[14] & 0x80) >> 5;
			var b11 = (midiData[15] & 0x80) >> 6;
			var b12 = (midiData[16] & 0x80) >> 7;
			expandedData.push(b10 | b11 | b12); //DCO W Saw

			var b07 = (midiData[11] & 0x80) >> 5;
			var b08 = (midiData[12] & 0x80) >> 6;
			var b09 = (midiData[13] & 0x80) >> 7;
			expandedData.push(b07 | b08 | b09); //DCO W Sub

			var b17 = (midiData[21] & 0x80) >> 6;
			var b18 = (midiData[22] & 0x80) >> 7;
			expandedData.push(b17 | b18); //DCO Range

			var b19 = (midiData[23] & 0x80) >> 6;
			var b20 = (midiData[24] & 0x80) >> 7;
			expandedData.push(b19 | b20); //DCO Sub level

			var b21 = (midiData[25] & 0x80) >> 6;
			var b22 = (midiData[26] & 0x80) >> 7;
			expandedData.push(b21 | b22); //DCO Noise level

			var b15 = (midiData[19] & 0x80) >> 6;
			var b16 = (midiData[20] & 0x80) >> 7;
			expandedData.push(b15 | b16); //HPF Cutoff Freq

			var b00 = (midiData[4] & 0x80) >> 7;
			expandedData.push(b00); //Chorus

			expandedData.push(midiData[3] & 0x7F); //DCO LFO Mod Depth
			expandedData.push(midiData[4] & 0x7F); //DCO Env Mod Depth
			expandedData.push((midiData[0] & 0xF0) >> 4); //DCO After Depth
			expandedData.push(midiData[5] & 0x7F); //DCO PWM Depth
			expandedData.push(midiData[6] & 0x7F); //DCO PWM Rate

			expandedData.push(midiData[7] & 0x7F); //VCF Cut Freq
			expandedData.push(midiData[8] & 0x7F); //VCF Res
			expandedData.push(midiData[10] & 0x7F); //VCF LFO MOD Depth
			expandedData.push(midiData[9] & 0x7F); //VCF Env MOD Depth
			expandedData.push(midiData[0] & 0x0F); //VCF Key Follow
			expandedData.push((midiData[1] & 0xF0) >> 4); //VCF After Depth

			expandedData.push(midiData[11] & 0x7F); //VCA Level
			expandedData.push(midiData[1] & 0x0F); //VCA After Depth
			expandedData.push(midiData[12] & 0x7F); //LFO Rate
			expandedData.push(midiData[13] & 0x7F); //LFO Delay

			expandedData.push(midiData[14] & 0x7F); //Env
			expandedData.push(midiData[15] & 0x7F); //Env
			expandedData.push(midiData[16] & 0x7F); //Env
			expandedData.push(midiData[17] & 0x7F); //Env
			expandedData.push(midiData[18] & 0x7F); //Env
			expandedData.push(midiData[19] & 0x7F); //Env
			expandedData.push(midiData[20] & 0x7F); //Env

			expandedData.push((midiData[2] & 0xF0) >> 4); //Env Key Follow

			var c0 = (midiData[27] & 0x40) >> 6;
			var c1 = (midiData[27] & 0x80) >> 6;
			var c2 = (midiData[28] & 0x40) >> 4;
			var c3 = (midiData[28] & 0x80) >> 4;
			var c4 = (midiData[29] & 0x40) >> 2;
			var c5 = (midiData[29] & 0x80) >> 2;
			var c6 = (midiData[30] & 0x40);
			var c7 = (midiData[30] & 0x80);
			expandedData.push(c0 | c1 | c2 | c3 | c4 | c5 | c6 | c7); //Chorus Rate

			expandedData.push(midiData[2] & 0x0F); //Bender Range

			return expandedData;
		},
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('RolandJd800', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'RolandJd800',
		manuf 		: 'Roland',
		model 		: 'JD 800',
		hints 		: [
			'In <strong>MIDI</strong> menu set <strong>Unit Number</strong> to <strong>17</strong> and <strong>Rx exclusive</strong> to <strong>ON-1</strong>',
			'Alternatively just set <strong>Rx exclusive</strong> to <strong>ON-2</strong>, that should ignore the <strong>Unit Number</strong>',
		],
		params : {
			deviceId	: 0x10, //JD deviceId 0x10 == 17
			banks : {
				bank1 : {label : 'Bank 1', address : 0x00},
			},
			patchesPerBank : 64,
			transferPause : 300,
			bulkQty 	: 0,
			buffer 		: [],
			parserConfig : {
				name : {
					from : 0,
					to : 15
				},
				dna : {
					from : 0,
					to : 384
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			//first part
			var temporaryAddress, checksum, eox, firstPart, secPart;
			var header = [0xF0, 0x41, this.params.deviceId, 0x3d, 0x12];
			temporaryAddress = [0x00, 0x00, 0x00]; //temporary area address
			midiData = Array.from(midiData);

			firstPart = midiData.slice(0,256);
			firstPart = temporaryAddress.concat(firstPart);

			checksum = Sysex.calculateRolandChecksum(firstPart);
			eox = [checksum, 0xF7];

			firstPart = header.concat(firstPart, eox);
			firstPart = Sysex.convertToUint8(firstPart);

			Sysex.sendSysex(firstPart, WebMIDI.midiOut);

			//second part
			temporaryAddress = [0x00, 0x02, 0x00];
			secPart = midiData.slice(256, midiData.length);
			secPart = temporaryAddress.concat(secPart);

			checksum = Sysex.calculateRolandChecksum(secPart);
			eox = [checksum, 0xF7];

			secPart = header.concat(secPart, eox);
			midiData = Sysex.convertToUint8(secPart);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			//first part
			var address, checksum, eox, firstPart, secPart;
			var header = [0xF0, 0x41, this.params.deviceId, 0x3d, 0x12];

			var bytesInTheMiddle = 3*prgNo;
			var msb = (bytesInTheMiddle >> 7) + 0x05;

			address = [msb, bytesInTheMiddle & 0x7F, 0x00]; //address

			midiData = Array.from(midiData);

			firstPart = midiData.slice(0,256);
			firstPart = address.concat(firstPart);

			checksum = Sysex.calculateRolandChecksum(firstPart);
			eox = [checksum, 0xF7];

			firstPart = header.concat(firstPart, eox);

			//second part
			bytesInTheMiddle = (3*prgNo) + 0x02;
			msb = (bytesInTheMiddle >> 7) + 0x05;

			address = [msb, bytesInTheMiddle & 0x7F, 0x00]; //address

			secPart = midiData.slice(256, midiData.length);
			secPart = address.concat(secPart);

			checksum = Sysex.calculateRolandChecksum(secPart);
			eox = [checksum, 0xF7];

			secPart = header.concat(secPart, eox);

			midiData = firstPart.concat(secPart);

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x41, this.params.deviceId, 0x3D, 0x11];
			var address = [0x05, 0x00, 0x00, 0x01, 0x40, 0x00]; //address and weight for Patch Memory Area
			var checksum = Sysex.calculateRolandChecksum(address);

			var eox = [checksum, 0xF7];

			var midiData = header.concat(address, eox);
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			if(midiData[4] == 0x12 && midiData[5] === 0x00) { //Single Patch
				this.combineRolandSysex(midiData);

				if(this.params.bulkQty == 2) {
					return this.processBulk();
				}
			}
			if(midiData[4] == 0x12 && midiData[5] > 0x04) { //'Patch Memory Area'
				this.combineRolandSysex(midiData);

				if(this.params.bulkQty == 96) {
					return this.processBulk();
				}
			}
			return [];
		},
		combineRolandSysex : function(midiData) {
			this.params.bulkQty++;
			for (var i = 8; // Skip header
				i < midiData.length - 2; // Omit EOF and Checksum
				i++)
			{
				this.params.buffer.push(midiData[i]);
			}
		},
		processBulk : function () {
			var midiData = this.params.buffer;

			var i,j,data;
			var patchNames = [];
			var chunk = 384;

			for (i=0,j=midiData.length; i<j; i+=chunk) {

				data = midiData.slice(i,i+chunk);
				patchNames.push(Sysex.extractPatchName(data, this.params.parserConfig));
			}

			//Reset params
			this.params.bulkQty = 0;
			this.params.buffer = [];

			return patchNames;
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 16; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			return Sysex.renameProgram(newName, nameLength, program.data, this.params.parserConfig);
		}
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('RolandJv80', ['$log', '$timeout', 'Sysex', 'WebMIDI', function($log, $timeout, Sysex, WebMIDI) {
	var patchData = {};
	var dna;
	var obj = {
		id 			: 'RolandJv80',
		manuf 		: 'Roland',
		model 		: 'JV-80',
		hints 		: [
			'Press <strong>WRITE</strong>.',
			'Use the <strong>CURSOR RIGHT</strong> button to select <strong>PROTECT</strong> and press <strong>ENTER</strong>',
			'Use <strong>PARAMETER SLIDER 1</strong> to change the <strong>WRITE PROTECT</strong> memory protect value to <strong>OFF</strong>',
			'Press <strong>EXIT</strong> twice',
			'Press <strong>MIDI</strong>',
			'Use the <strong>DOWN</strong> <strong>ARROW</strong> button to select the <strong>SYS-EX</strong> display screen.',
			'Use <strong>PARAMETER SLIDER 2</strong> to set the Unit # to <strong>17</strong>.'
		],
		params : {
			deviceId	: 0x10, //JV deviceId 0x10 == 17
			banks : {
				bank1 : {label : 'Internal', address : 0x00},
			},
			patchesPerBank : 64,
			transferPause : 40,
			parserConfig : {
				name : {
					from : 9,
					to : 20
				},
				dna : {
					from : 7,
					to : 124
				}
			}
		},
		sendToBuffer : function(program) {
			var programKeys = ['PatchCommon', 'PatchTone1', 'PatchTone2', 'PatchTone3', 'PatchTone4'];
			// var programKeys = Object.keys(program.data);

			var dumpProgramParts = function(programKey) {
				programKey = programKey[0];
				// $log.log('programKey ' , programKey);
				var midiData = Sysex.objectToArray(program.data[programKey]);

				var offset = 0x00;
				switch(programKey) {
					case 'PatchCommon' : offset = 0x00; break;
					case 'PatchTone1'  : offset = 0x08; break;
					case 'PatchTone2'  : offset = 0x09; break;
					case 'PatchTone3'  : offset = 0x0A; break;
					case 'PatchTone4'  : offset = 0x0B; break;
				}
				
				// midiData = Array.from(midiData);
				midiData[2] = obj.params.deviceId; 	// DeviceId
				midiData[4] = 0x12; 				// Command ID DT1
				midiData[5] = 0x00; 				// [0x00, 0x08, 0x20, 0x00]; //Patch Mode Temporary Patch
				midiData[6] = 0x08; 	
				midiData[7] = 0x20 + offset; 	
				midiData[8] = 0x00;

				var checksum = Sysex.calculateRolandChecksum(midiData.slice(5, (midiData.length-2)));
				midiData[midiData.length-2] = checksum;

				Sysex.sendSysex(midiData, WebMIDI.midiOut);

				if(programKeys.length > 0) {
					programKey = programKeys.splice(0,1);
					$timeout(dumpProgramParts, obj.params.transferPause, true, programKey);
				} else {
					$log.info("Buffer dump completed");
				}
			};
			dumpProgramParts(programKeys.splice(0,1));
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var programData = program.data;
			var reorderedData = [];

			for(var programKey in programData) { 
			   if (programData.hasOwnProperty(programKey)) {
			       var midiData = programData[programKey];
					midiData = Sysex.objectToArray(midiData);

					var offset = 0x00;
					switch(programKey) {
						case 'PatchCommon' : offset = 0x00; break;
						case 'PatchTone1'  : offset = 0x08; break;
						case 'PatchTone2'  : offset = 0x09; break;
						case 'PatchTone3'  : offset = 0x0A; break;
						case 'PatchTone4'  : offset = 0x0B; break;
					}

					midiData[2] = obj.params.deviceId; 	// DeviceId
					midiData[4] = 0x12; 				// Command ID DT1
					midiData[5] = 0x01; 				
					midiData[6] = 0x40 + prgNo; 	
					midiData[7] = 0x20 + offset; 	
					midiData[8] = 0x00;

					midiData[midiData.length-2] = Sysex.calculateRolandChecksum(midiData.slice(5, (midiData.length-2)));	
					reorderedData = reorderedData.concat(midiData);
			   }
			}
			return reorderedData;
		},
		retrieveBank : function(bank) {
			var dumpRequest = [0xF0, 0x41, this.params.deviceId, 0x46, 0x11, 0x01, 0x40, 0x20, 0x00, 0x00, 0x40, 0x00, 0x00, 0x5F, 0xF7];
			// var checksum = Sysex.calculateRolandChecksum(dumpRequest.slice(5, (dumpRequest.length-2)));
			// dumpRequest[dumpRequest.length-2] = checksum;
			Sysex.sendSysex(dumpRequest, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			var program = [];
			switch(midiData[7]) {
				case 0x20 : 
					this.processPatchData('PatchCommon', midiData);
					break;
				case 0x28 : 
					this.processPatchData('PatchTone1', midiData);
					break;
				case 0x29 :
					this.processPatchData('PatchTone2', midiData);
					break;
				case 0x2A :
					this.processPatchData('PatchTone3', midiData);
					break;
				case 0x2B :
					this.processPatchData('PatchTone4', midiData);
					var patchName = this.getPatchName(patchData.PatchCommon);
					
					var dna = this.createDNA(patchData);

					program = [{name : patchName, dna : dna, data : patchData}];
					patchData = {};
					break;
			}
			return program;
		},
		processPatchData : function(patchKey, midiData) {
			patchData[patchKey] = new Uint8Array(midiData.length);
			patchData[patchKey] = midiData;
		},
		getPatchName : function(midiData) {
			var data = angular.fromJson(angular.toJson(midiData));
			var patchName = '';

			for(var k=this.params.parserConfig.name.from; k<=this.params.parserConfig.name.to; k++) {
				patchName += String.fromCharCode(data[k]);
			}
			return patchName.trim();
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return ?; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var midiData = Sysex.objectToArray(program.data.PatchCommon);

			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);
			midiData = Sysex.changeNameinMidiData(newNameArray, nameLength, midiData, this.params.parserConfig);
			midiData[midiData.length-2] = Sysex.calculateRolandChecksum(midiData.slice(5, (midiData.length-2)));

			program.data.PatchCommon = midiData;

			return {name : newName, dna : this.createDNA(program.data), data : program.data};
		},
		getPatchData : function() {
			return patchData;
		},
		createDNA : function(patchData) {
			var from = this.params.parserConfig.dna.from;
			var to = this.params.parserConfig.dna.to;
			var dnaArray = [];

			dnaArray = dnaArray.concat(patchData.PatchCommon.slice(7, patchData.PatchCommon.length-2));
			dnaArray = dnaArray.concat(patchData.PatchTone1.slice(from, to));
			dnaArray = dnaArray.concat(patchData.PatchTone2.slice(from, to));
			dnaArray = dnaArray.concat(patchData.PatchTone3.slice(from, to));
			dnaArray = dnaArray.concat(patchData.PatchTone4.slice(from, to));

			return Sysex.createDNA(dnaArray, 0, dnaArray.length);
		},
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('SequentialProphet6', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'SequentialProphet6',
		manuf 		: 'Sequential',
		model 		: 'Prophet 6',
		hints 		: [
			'In <strong>GLOBALS</strong> menu set <strong>MIDI SysEx</strong> to <strong>Midi</strong> or <strong>USB</strong> depending on which connection you use.',
            'You should only save programs to user bank 0-4.',
            'The factory banks 5-9 are meant to be *read only*. However LASER Mammoth lets you overwrite them too. Do this only if you really need to.',
		],
		params : {
			banks : {
				bank1 : {label : 'P~6 / User Bank 0', address : 0x00},
				bank2 : {label : 'P~6 / User Bank 1', address : 0x01},
				bank3 : {label : 'P~6 / User Bank 2', address : 0x02},
				bank4 : {label : 'P~6 / User Bank 3', address : 0x03},
				bank5 : {label : 'P~6 / User Bank 4', address : 0x04},
				bank6 : {label : 'P~6 / Factory Bank 5', address : 0x05},
				bank7 : {label : 'P~6 / Factory Bank 6', address : 0x06},
				bank8 : {label : 'P~6 / Factory Bank 7', address : 0x07},
				bank9 : {label : 'P~6 / Factory Bank 8', address : 0x08},
				bank10 : {label : 'P~6 / Factory Bank 9', address : 0x09},
			},
			patchesPerBank : 100,
			transferPause : 400,
			parserConfig : {
				rawData : {
					from: 6,
					to: 1177 //1171 bytes packed, 1024 expanded
				},
				name : {
					from : 107,
					to : 126
				},
				dna : {
					from : 0,
					to : 1024
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);
			midiData[3] = 0x03; 	// Edit Buffer Data
			midiData.splice(4, 2);	// Unset Bank and Prg
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[3] = 0x02; 	// Program Data Dump
			midiData[4] = bank;		// 0 - 9
			midiData[5] = prgNo;	// 0 - 99

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x01, 0x2D, 0x05];
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			midiData = Sysex.objectToArray(midiData);
			var unpackedData = this.unpackMidiData(midiData);
			var program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			program.data = midiData;
			return [program];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 22; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, this.params.parserConfig.rawData.from);
			var unpackedData = this.unpackMidiData(midiData);

			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);
			program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			
			var packedData = Sysex.packDSISysex(unpackedData);
			program.data = header.concat(packedData, 0xF7);
			return program;
		},
		unpackMidiData : function(midiData) {
			var packedData = midiData.slice(this.params.parserConfig.rawData.from, this.params.parserConfig.rawData.to);
			var unpackedData = Sysex.unpackDSISysex(packedData);
			// Sysex.dumpData(unpackedData, 16);
			return unpackedData;
		}
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('SequentialProphetX', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'SequentialProphetX',
		manuf 		: 'Sequential',
		model 		: 'Prophet X [beta]',
		hints 		: [
			'Press the Global button on your X.',
            'Use Soft Knob 1 to select <strong>MIDI sysex cable</strong> then use the Soft Knob 3 to choose either <strong>USB</strong> or <strong>MIDI</strong>, depending on which port you are using to connect to your computer.',
			'You can only transfer programs from bank U1 to bank U4, higher banks are readable only (ROM).',
			'You can only change the name of Layer A of a program.'
		],
		params : {
			banks : {
				bank1 : {label : 'Prophet X / Bank U1', address : 0x00},
				bank2 : {label : 'Prophet X / Bank U2', address : 0x01},
				bank3 : {label : 'Prophet X / Bank U3', address : 0x02},
				bank4 : {label : 'Prophet X / Bank U4', address : 0x03},
			},
			patchesPerBank : 128,
			transferPause : 1000,
			parserConfig : {
				rawData : {
					from: 6,
					to: 4696 //4690 bytes packed, 4096 expanded
				},
				name : {
					from : 418,
					to : 437,
				},
				dna : {
					from : 0,
					to : 4096
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);
			midiData[3] = 0x03; 	// Edit Buffer Data
			midiData.splice(4, 2);	// Unset Bank and Prg
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[3] = 0x02; 	// Program Data Dump
			midiData[4] = bank;		// 0 - 9
			midiData[5] = prgNo;	// 0 - 99

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x01, 0x30, 0x05];
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			midiData = Sysex.objectToArray(midiData);
			var unpackedData = this.unpackMidiData(midiData);
			var program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			program.data = midiData;
			return [program];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 20; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, this.params.parserConfig.rawData.from);

			var unpackedData = this.unpackMidiData(midiData);
			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);
			program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			
			var packedData = Sysex.packDSISysex(unpackedData);
			program.data = header.concat(packedData, 0xF7);
			return program;
		},
		unpackMidiData : function(midiData) {
			var packedData = midiData.slice(this.params.parserConfig.rawData.from, this.params.parserConfig.rawData.to);
			var unpackedData = Sysex.unpackDSISysex(packedData);
			//Sysex.dumpData(unpackedData, 16);
			return unpackedData;
		}
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('StrymonTimeline', ['$log', 'Sysex', '$timeout', 'WebMIDI', function($log, Sysex, $timeout, WebMIDI) {
	var obj = {
		id 			: 'StrymonTimeline',
		manuf 		: 'Strymon',
		model 		: 'Timeline',
		params : {
			banks : {
				bank1 : {label : 'Strymon Bank', address : 0x00},
			},
			patchesPerBank : 200,
			transferPause : 200,
			parserConfig : {
				name : {
					from : 632,
					to : 647
				},
				dna : {
					from : 9,
					to : 648
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[6] = 0x62;
			midiData[7] = 0x01; //BankNumber
			midiData[8] = 0x48; //ProgramNumber Single buffer in Single Mode 
			midiData[midiData.length-2] = Sysex.calculateStrymonChecksum(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			if(prgNo > 0x7F) {
				prgNo = prgNo - 0x80;
				bank = bank + 1;
			}

			midiData[6] = 0x62; //MIDI_WRITE_SINGLE_PATCH
			midiData[7] = bank; //BankNumber
			midiData[8] = prgNo; //Prg No
			midiData[midiData.length-2] = Sysex.calculateStrymonChecksum(midiData);

			return midiData;
		},
		retrieveBank : function(bank) {
			//Damage Control Engineering 10 bytes	F0 00 01 55 12 01 63 00 00 F7
			var header = [0xF0, 0x00, 0x01, 0x55, 0x12, 0x01, 0x63];
			this.requestAllProgramsOfBank(bank, header, this.params.patchesPerBank, this.params.transferPause);
		},
		requestAllProgramsOfBank : function(bank, header, patchesPerBank, transferPause) {
			var dumpRequest = [];
			var prgNo = 0;
			var prgNoHex = 0x00;

			var delayedDumpRequest = function(prgNo) {
				$log.debug("Requesting Prg " + prgNo);
				dumpRequest = header.concat([bank, prgNoHex, 0xF7]);
				Sysex.sendSysex(dumpRequest, WebMIDI.midiOut);

				if(prgNo < (patchesPerBank-1)) {
					prgNo++;
					prgNoHex++;

					if(prgNoHex> 0x7F) { //Hex cannot be greater than 127. Set to 0x00 and increase bank.
						bank++;
						prgNoHex = 0x00;
					}

					$timeout(delayedDumpRequest, transferPause, true, prgNo);
				} else {
					$log.info("Bank dumpRequest completed");
				}
			};
			delayedDumpRequest(prgNo);
		},
		extractPatchNames : function(midiData) {
			//Damage Control Engineering 10 bytes	F0 00 01 55 12 01 01 48 45 F7
			if(midiData[9] == 0xF7) {
				$log.info("Respond to strange response from Strymon with even stranger response");
				Sysex.sendSysex([0xF0, 0x00, 0x01, 0x55, 0x12, 0x01, 0x26, 0xF7], WebMIDI.midiOut);
				return [];
			} else {
				return [Sysex.extractPatchName(midiData, this.params.parserConfig)];
			}
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 10; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var midiData = Sysex.objectToArray(program.data);
			newName = newName.toUpperCase();
			return Sysex.renameProgram(newName, nameLength, midiData, this.params.parserConfig);
		}
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('WaldorfMicrowave', ['$log', '$timeout', 'Sysex', 'WebMIDI', function($log, $timeout, Sysex, WebMIDI) {
	var obj = {
		id 			: 'WaldorfMicrowave',
		manuf 		: 'Waldorf Music',
		model 		: 'MicroWave',
		hints 		: [
			'In <strong>Device Param.</strong> menu set <strong>Dev. Number</strong> to <strong>000</strong>',
            'Works with OS V1.25 and V2.0 on a MicroWave 1'
		],
		params : {
            deviceId    : 0x00,
            singleProgramsCannotBeTransmitted : true,
			banks : {
				bank1 : {label : 'Internal Bank (A+B)', address : 0x10}
			},
			patchesPerBank : 64,
			transferPause : 15,
			parserConfig : {
				name : {
					from : 153, //pos with header
					to : 168
				},
				dna : {
					from : 5,
					to : 185
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			// Sysex.dumpData(midiData, 16);

			midiData[3] = this.params.deviceId;
            midiData[4] = 0x42; //BPRD Dump

            var rawData = this.getRawData(midiData);
			midiData[midiData.length-2] = Sysex.calculateWaldorfChecksum(rawData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
        sendProgramToBank : function(program, bank, prgNo) {
            var midiData = this.reorderProgram(program, bank, prgNo);
            // Sysex.dumpData(midiData, 16);

            if(midiData.length > 0) {
                Sysex.sendSysex(midiData, WebMIDI.midiOut);
            }
        },
        reorderProgram : function(program, bank, prgNo) {
            if(prgNo === 0) {
                this.params.buffer = [];
            }
            var rawData = this.getRawData(Sysex.objectToArray(program.data));
            this.params.buffer = this.params.buffer.concat(rawData);

            if(prgNo == (this.params.patchesPerBank-1)) {
                var header = [0xF0, 0x3E, 0x00, this.params.deviceId, 0x50];

                var checksum = Sysex.calculateWaldorfChecksum(this.params.buffer);
                var eox = [checksum, 0xF7];
                
                var midiData = header.concat(this.params.buffer, eox);
                // console.log(checksum);

                this.params.buffer = [];
                return midiData;
            }
            return [];
        },
		retrieveBank : function(bank) {
            //Microwave Sound-Program Bank Dump request
            var midiData = [0xF0, 0x3E, 0x00, this.params.deviceId, bank, 0x00, 0xF7];
            Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
        extractPatchNames : function(midiData) {
            var program = [];

            midiData = Sysex.objectToArray(midiData);

            switch(midiData[4]) {
                case 0x42 : program = this.extractSinglePatch(midiData); break; //42  BPRD BPR Dump
                case 0x50 : program = this.extractMultiDump(midiData); break; //50  BPBD BPR Bank Dump
            }
            
            return program;
        },
        extractSinglePatch : function(midiData) {
            if(midiData.length == 187) {
                var program = Sysex.extractPatchName(midiData, this.params.parserConfig);
                return [program];
            } else {
                return [];
            }
        },
        extractMultiDump : function(midiData) {
            var rawData = midiData.slice(5, (midiData.length-2));

            var chunk = 180;
            var i,j,data,checksum,eox;
            var patchNames = [];
            var program = [];
            var header = [0xF0, 0x3E, 0x00, this.params.deviceId, 0x42];

            for (i=0,j=rawData.length; i<j; i+=chunk) {
                data = rawData.slice(i,i+chunk);

                // Sysex.dumpData(data, 16);

                checksum = Sysex.calculateWaldorfChecksum(data);
                eox = [checksum, 0xF7];

                data = header.concat(data, eox);
                program = Sysex.extractPatchName(data, this.params.parserConfig);

                patchNames.push(program);
            }
            return patchNames;
        },
        getRawData : function(midiData) {
            return midiData.slice(5, 185);
        },
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return ?; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			return Sysex.renameProgram(newName, nameLength, program.data, this.params.parserConfig);
		}
	};
	return obj;
}]);

angular.module('f0f7.devices').factory('WaldorfBlofeld', ['$log', '$timeout', 'Sysex', 'WebMIDI', function($log, $timeout, Sysex, WebMIDI) {
	var obj = {
		id 			: 'WaldorfBlofeld',
		manuf 		: 'Waldorf Music',
		model 		: 'Blofeld',
		hints 		: [
			'In <strong>GLOBAL</strong> menu set <strong>Device ID</strong> to <strong>0</strong>',
		],
		params : {
			deviceId	: 0x00, //00h to 7Eh, 7Fh = broadcast
			banks : {
				bank1 : {label : 'Blofeld Bank A', address : 0x00},
				bank2 : {label : 'Blofeld Bank B', address : 0x01},
				bank3 : {label : 'Blofeld Bank C', address : 0x02},
				bank4 : {label : 'Blofeld Bank D', address : 0x03},
				bank5 : {label : 'Blofeld Bank E', address : 0x04},
				bank6 : {label : 'Blofeld Bank F', address : 0x05},
				bank7 : {label : 'Blofeld Bank G', address : 0x06},
				bank8 : {label : 'Blofeld Bank H', address : 0x07}
			},
			patchesPerBank : 128,
			transferPause : 400,
			parserConfig : {
				name : {
					from : 370,
					to : 389
				},
				dna : {
					from : 7,
					to : 390
				}
			}
		},
		sendToBuffer : function(program) {
			// var checksum = 0x7F; //A checksum of 7Fh is always accepted as valid.
			var midiData = Sysex.objectToArray(program.data);

			// Sysex.dumpData(midiData, 16);

			midiData[3] = this.params.deviceId;
			midiData[5] = 0x7F; //7F 00 Sound Mode Edit Buffer
			midiData[6] = 0x00; //
			midiData[midiData.length-2] = this.calculateBlofeldChecksum(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			// Sysex.dumpData(midiData, 16);

			midiData[3] = this.params.deviceId;
			midiData[5] = bank;
			midiData[6] = prgNo;
			midiData[midiData.length-2] = this.calculateBlofeldChecksum(midiData);

			return midiData;
		},
		retrieveBank : function(bank) {
			var midiData = [];
			var prgNo = 0;
			var patchesPerBank = this.params.patchesPerBank;
			var transferPause = this.params.transferPause;
			var deviceId = this.params.deviceId;

			var delayedProgramSend = function(prgNo) {
				$log.log(prgNo + ' / ' + patchesPerBank);
				midiData = [0xF0, 0x3E, 0x13, deviceId, 0x00, bank, prgNo, 0xF7];
				Sysex.sendSysex(midiData, WebMIDI.midiOut);

				if(prgNo < (patchesPerBank-1)) {
					prgNo++;
					$timeout(delayedProgramSend, transferPause, true, prgNo);
				}
			};
			delayedProgramSend(prgNo);
		},
		extractPatchNames : function(midiData) {
			return [Sysex.extractPatchName(midiData, this.params.parserConfig)];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return ?; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			return Sysex.renameProgram(newName, nameLength, program.data, this.params.parserConfig);
		},
        calculateBlofeldChecksum : function(midiData) {
            // Sum of all databytes truncated to 7 bits.
            // The addition is done in 8 bit format, the result is
            // masked to 7 bits (00h to 7Fh). A checksum of 7Fh is
            // always accepted as valid.
            // IMPORTANT: the MIDI status-bytes as well as the
            // ID's are not used for computing the checksum.

            var checksum = 0x00;
            for(var i=7; i<(midiData.length-2); i++) {
                checksum += midiData[i];
            }
            return checksum & 0x7F;
        }
	};
	return obj;
}]);

// For those interested in unpacking the uscd.edu DX7 patch data, here is
// DX7 data format information.

//      compiled from - the DX7 MIDI Data Format Sheet
//                    - article by Steve DeFuria (Keyboard Jan 87)
//                    - looking at what my DX7 spits out

// I have kept the kinda weird notation used in the DX7 Data Sheet to reduce
// typing errors. Where it doesn't quite make sense to me I've added comments.
// (And I will not be liable for errors etc ....)

// Contents: A: SYSEX Message: Bulk Data for 1 Voice
//           B: SYSEX Message: Bulk Data for 32 Voices
//           C: SYSEX Message: Parameter Change
//           D: Data Structure: Single Voice Dump & Voice Parameter #'s
//           E: Function Parameter #'s
//           F: Data Structure: Bulk Dump Packed Format

// ////////////////////////////////////////////////////////////
// A:
// SYSEX Message: Bulk Data for 1 Voice
// ------------------------------------
//        bits    hex  description

//      11110000  F0   Status byte - start sysex
//      0iiiiiii  43   ID # (i=67; Yamaha)
//      0sssnnnn  00   Sub-status (s=0) & channel number (n=0; ch 1)
//      0fffffff  00   format number (f=0; 1 voice)
//      0bbbbbbb  01   byte count MS byte
//      0bbbbbbb  1B   byte count LS byte (b=155; 1 voice)
//      0ddddddd  **   data byte 1

//         |       |       |

//      0ddddddd  **   data byte 155
//      0eeeeeee  **   checksum (masked 2's complement of sum of 155 bytes)
//      11110111  F7   Status - end sysex



// ///////////////////////////////////////////////////////////
// B:
// SYSEX Message: Bulk Data for 32 Voices
// --------------------------------------
//        bits    hex  description

//      11110000  F0   Status byte - start sysex
//      0iiiiiii  43   ID # (i=67; Yamaha)
//      0sssnnnn  00   Sub-status (s=0) & channel number (n=0; ch 1)
//      0fffffff  09   format number (f=9; 32 voices)
//      0bbbbbbb  20   byte count MS byte
//      0bbbbbbb  00   byte count LS byte (b=4096; 32 voices)
//      0ddddddd  **   data byte 1

//         |       |       |

//      0ddddddd  **   data byte 4096  (there are 128 bytes / voice)
//      0eeeeeee  **   checksum (masked 2's comp. of sum of 4096 bytes)
//      11110111  F7   Status - end sysex


// /////////////////////////////////////////////////////////////
// C:
// SYSEX MESSAGE: Parameter Change
// -------------------------------
//        bits    hex  description

//      11110000  F0   Status byte - start sysex
//      0iiiiiii  43   ID # (i=67; Yamaha)
//      0sssnnnn  10   Sub-status (s=1) & channel number (n=0; ch 1)
//      0gggggpp  **   parameter group # (g=0; voice, g=2; function)
//      0ppppppp  **   parameter # (these are listed in next section)
//                      Note that voice parameter #'s can go over 128 so
//                      the pp bits in the group byte are either 00 for
//                      par# 0-127 or 01 for par# 128-155. In the latter case
//                      you add 128 to the 0ppppppp byte to compute par#. 
//      0ddddddd  **   data byte
//      11110111  F7   Status - end sysex


// //////////////////////////////////////////////////////////////

// D:
// Data Structure: Single Voice Dump & Parameter #'s (single voice format, g=0)
// -------------------------------------------------------------------------

// Parameter
//  Number    Parameter                  Value Range
// ---------  ---------                  -----------
//   0        OP6 EG rate 1              0-99
//   1         "  "  rate 2               "
//   2         "  "  rate 3               "
//   3         "  "  rate 4               "
//   4         "  " level 1               "
//   5         "  " level 2               "
//   6         "  " level 3               "
//   7         "  " level 4               "
//   8        OP6 KBD LEV SCL BRK PT      "        C3= $27
//   9         "   "   "   "  LFT DEPTH   "
//  10         "   "   "   "  RHT DEPTH   "
//  11         "   "   "   "  LFT CURVE  0-3       0=-LIN, -EXP, +EXP, +LIN
//  12         "   "   "   "  RHT CURVE   "            "    "    "    "  
//  13        OP6 KBD RATE SCALING       0-7
//  14        OP6 AMP MOD SENSITIVITY    0-3
//  15        OP6 KEY VEL SENSITIVITY    0-7
//  16        OP6 OPERATOR OUTPUT LEVEL  0-99
//  17        OP6 OSC MODE (fixed/ratio) 0-1        0=ratio
//  18        OP6 OSC FREQ COARSE        0-31
//  19        OP6 OSC FREQ FINE          0-99
//  20        OP6 OSC DETUNE             0-14       0: det=-7
//  21 \
//   |  > repeat above for OSC 5, OSC 4,  ... OSC 1
// 125 /
// 126        PITCH EG RATE 1            0-99
// 127          "    " RATE 2              "
// 128          "    " RATE 3              "
// 129          "    " RATE 4              "
// 130          "    " LEVEL 1             "
// 131          "    " LEVEL 2             "
// 132          "    " LEVEL 3             "
// 133          "    " LEVEL 4             "
// 134        ALGORITHM #                 0-31
// 135        FEEDBACK                    0-7
// 136        OSCILLATOR SYNC             0-1
// 137        LFO SPEED                   0-99
// 138         "  DELAY                    "
// 139         "  PITCH MOD DEPTH          "
// 140         "  AMP   MOD DEPTH          "
// 141        LFO SYNC                    0-1
// 142         "  WAVEFORM                0-5, (data sheet claims 9-4 ?!?)
//                                        0:TR, 1:SD, 2:SU, 3:SQ, 4:SI, 5:SH
// 143        PITCH MOD SENSITIVITY       0-7
// 144        TRANSPOSE                   0-48   12 = C2
// 145        VOICE NAME CHAR 1           ASCII
// 146        VOICE NAME CHAR 2           ASCII
// 147        VOICE NAME CHAR 3           ASCII
// 148        VOICE NAME CHAR 4           ASCII
// 149        VOICE NAME CHAR 5           ASCII
// 150        VOICE NAME CHAR 6           ASCII
// 151        VOICE NAME CHAR 7           ASCII
// 152        VOICE NAME CHAR 8           ASCII
// 153        VOICE NAME CHAR 9           ASCII
// 154        VOICE NAME CHAR 10          ASCII
// 155        OPERATOR ON/OFF
//               bit6 = 0 / bit 5: OP1 / ... / bit 0: OP6

// Note that there are actually 156 parameters listed here, one more than in 
// a single voice dump. The OPERATOR ON/OFF parameter is not stored with the
// voice, and is only transmitted or received while editing a voice. So it
// only shows up in parameter change SYS-EX's.


// ////////////////////////////////////////////////////////

// E:
// Function Parameters: (g=2)
// -------------------------

// Parameter
// Number        Parameter           Range
// ---------     ----------          ------
// 64         MONO/POLY MODE CHANGE  0-1      O=POLY
// 65         PITCH BEND RANGE       0-12
// 66           "    "   STEP        0-12
// 67         PORTAMENTO MODE        0-1      0=RETAIN 1=FOLLOW
// 68              "     GLISS       0-1
// 69              "     TIME        0-99
// 70         MOD WHEEL RANGE        0-99
// 71          "    "   ASSIGN       0-7     b0: pitch,  b1:amp, b2: EG bias
// 72         FOOT CONTROL RANGE     0-99
// 73          "     "     ASSIGN    0-7           "
// 74         BREATH CONT RANGE      0-99
// 75           "     "   ASSIGN     0-7           "
// 76         AFTERTOUCH RANGE       0-99
// 77             "      ASSIGN      0-7           "

// ///////////////////////////////////////////////////////////////

// F:
// Data Structure: Bulk Dump Packed Format
// ---------------------------------------

// OK, now the tricky bit. For a bulk dump the 155 voice parameters for each
//  voice are packed into 32 consecutive 128 byte chunks as follows ...

// byte             bit #
//  #     6   5   4   3   2   1   0   param A       range  param B       range
// ----  --- --- --- --- --- --- ---  ------------  -----  ------------  -----
//   0                R1              OP6 EG R1      0-99
//   1                R2              OP6 EG R2      0-99
//   2                R3              OP6 EG R3      0-99
//   3                R4              OP6 EG R4      0-99
//   4                L1              OP6 EG L1      0-99
//   5                L2              OP6 EG L2      0-99
//   6                L3              OP6 EG L3      0-99
//   7                L4              OP6 EG L4      0-99
//   8                BP              LEV SCL BRK PT 0-99
//   9                LD              SCL LEFT DEPTH 0-99
//  10                RD              SCL RGHT DEPTH 0-99
//  11    0   0   0 |  RC   |   LC  | SCL LEFT CURVE 0-3   SCL RGHT CURVE 0-3
//  12  |      DET      |     RS    | OSC DETUNE     0-14  OSC RATE SCALE 0-7
//  13    0   0 |    KVS    |  AMS  | KEY VEL SENS   0-7   AMP MOD SENS   0-3
//  14                OL              OP6 OUTPUT LEV 0-99
//  15    0 |         FC        | M | FREQ COARSE    0-31  OSC MODE       0-1
//  16                FF              FREQ FINE      0-99
//  17 \
//   |  > these 17 bytes for OSC 5
//  33 /
//  34 \
//   |  > these 17 bytes for OSC 4
//  50 /
//  51 \
//   |  > these 17 bytes for OSC 3
//  67 /
//  68 \
//   |  > these 17 bytes for OSC 2
//  84 /
//  85 \
//   |  > these 17 bytes for OSC 1
// 101 /

// byte             bit #
//  #     6   5   4   3   2   1   0   param A       range  param B       range
// ----  --- --- --- --- --- --- ---  ------------  -----  ------------  -----
// 102               PR1              PITCH EG R1   0-99
// 103               PR2              PITCH EG R2   0-99
// 104               PR3              PITCH EG R3   0-99
// 105               PR4              PITCH EG R4   0-99
// 106               PL1              PITCH EG L1   0-99
// 107               PL2              PITCH EG L2   0-99
// 108               PL3              PITCH EG L3   0-99
// 109               PL4              PITCH EG L4   0-99
// 110    0   0 |        ALG        | ALGORITHM     0-31
// 111    0   0   0 |OKS|    FB     | OSC KEY SYNC  0-1    FEEDBACK      0-7
// 112               LFS              LFO SPEED     0-99
// 113               LFD              LFO DELAY     0-99
// 114               LPMD             LF PT MOD DEP 0-99
// 115               LAMD             LF AM MOD DEP 0-99
// 116  |  LPMS |      LFW      |LKS| LF PT MOD SNS 0-7   WAVE 0-5,  SYNC 0-1
// 117              TRNSP             TRANSPOSE     0-48
// 118          NAME CHAR 1           VOICE NAME 1  ASCII
// 119          NAME CHAR 2           VOICE NAME 2  ASCII
// 120          NAME CHAR 3           VOICE NAME 3  ASCII
// 121          NAME CHAR 4           VOICE NAME 4  ASCII
// 122          NAME CHAR 5           VOICE NAME 5  ASCII
// 123          NAME CHAR 6           VOICE NAME 6  ASCII
// 124          NAME CHAR 7           VOICE NAME 7  ASCII
// 125          NAME CHAR 8           VOICE NAME 8  ASCII
// 126          NAME CHAR 9           VOICE NAME 9  ASCII
// 127          NAME CHAR 10          VOICE NAME 10 ASCII

// /////////////////////////////////////////////////////////////////////


angular.module('f0f7.devices').factory('YamahaDx7', ['$log', 'Sysex', 'WebMIDI', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'YamahaDx7',
		manuf 		: 'Yamaha',
		model 		: 'DX7',
		hints 		: [
			'Also works with <strong>TX802</strong> and might work with <strong>DX7II</strong>, <strong>TX7</strong> and <strong>TX816</strong>',
			'Compatible with <strong>Dexed</strong> banks',
			'Set <strong>channel number</strong> to <strong>1</strong>',
			'You have to add at least <strong>32</strong> programs to a bank or <strong>Overwrite Bank</strong> will not work'
		],
		params : {
			deviceId	: 0x10, //DeviceId omni
			singleProgramsCannotBeTransmitted : true,
			banks : {
				bank1 : {label : 'Dx7 Bank', address : 0x00},
			},
			patchesPerBank : 32,
			transferPause : 5,
			buffer 		: [],
			parserConfig : {
				name : {
					from : 118,
					to : 127
					// from : 145,
					// to : 154
				},
				dna : {
					from : 0,
					to : 127
				}
			}
		},
		sendToBuffer : function(program, outputPort) {		
			var header = [0xF0, 0x43, 0x00, 0x00, 0x01, 0x1B];
			var rawData = Sysex.objectToArray(program.data);
			var expandedData = this.expand(rawData);
			var checksum = Sysex.calculateRolandChecksum(expandedData);
			var eox = [checksum, 0xF7];
			var midiData = header.concat(expandedData, eox);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			if(midiData.length > 0) {
				Sysex.sendSysex(midiData, WebMIDI.midiOut);
			}
		},
		reorderProgram : function(program, bank, prgNo) {
			if(prgNo === 0) {
				this.params.buffer = [];
			}
			var rawData = Sysex.objectToArray(program.data);
			this.params.buffer = this.params.buffer.concat(rawData);

			if(prgNo == (this.params.patchesPerBank-1)) {
				var header = [0xF0, 0x43, 0x00, 0x09, 0x20, 0x00];
				var checksum = Sysex.calculateRolandChecksum(this.params.buffer);
				var eox = [checksum, 0xF7];
				var midiData = header.concat(this.params.buffer, eox);
				this.params.buffer = [];
				return midiData;
			}
			return [];
		},
		retrieveBank : function(bank) {
			//F0 43 20 09 F7
			var midiData = [0xF0, 0x43, 0x20, 0x09, 0xF7];
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			var program = [];

			midiData = Sysex.objectToArray(midiData);

			// switch(midiData[3]) {
			// 	case 0x00 : program = this.extractSinglePatch(midiData); break; //format number (f=0; 1 voice)
			// 	case 0x09 : program = this.extractMultiDump(midiData); break; //format number (f=9; 32 voices)
			// }
			program = this.extractMultiDump(midiData); //always extract Multidump to be compatible with Dexed
			return program;
		},
		extractSinglePatch : function(midiData) {
			// midiData = midiData.slice(6, (midiData.length-2));
			// return [Sysex.extractPatchName(midiData, this.params.parserConfig)];
			return [];
		},
		extractMultiDump : function(midiData) {
			midiData = midiData.slice(6, (midiData.length-2));

			var chunk = 128;
			var i,j,data;
			var patchNames = [];

			for (i=0,j=midiData.length; i<j; i+=chunk) {
				data = midiData.slice(i,i+chunk);
				Sysex.dumpData(data, 16);
				patchNames.push(Sysex.extractPatchName(data, this.params.parserConfig));
			}
			return patchNames;
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 10; 
		},
		renameProgram : function(newName, program) {
			// newName = newName.toUpperCase();
			var nameLength = this.getMaxNameLength();
			var midiData = Sysex.objectToArray(program.data);
			return Sysex.renameProgram(newName, nameLength, midiData, this.params.parserConfig);
		},
		expand : function(midiData) {
			var expandedData = [];
			var oscData = midiData.concat([]); //Copy Array
			for(var osc=6; osc>=1; osc--) {
				var packedOscData = oscData.splice(0, 17);
				for(var i=0; i<11; i++) {
					expandedData.push(packedOscData[i]);
				}
				expandedData.push((packedOscData[11] & 0x03)); //LFT CURVE
				expandedData.push(((packedOscData[11] >> 2) & 0x03)); //RHT CURVE
				expandedData.push((packedOscData[12] & 0x07)); //OSC RATE SCALE 0-7
				expandedData.push((packedOscData[13] & 0x03)); //AMP MOD SENS   0-3
				expandedData.push(((packedOscData[13] >> 2) & 0x07)); // KEY VEL SENS   0-7 
				expandedData.push(packedOscData[14]); // OPERATOR OUTPUT LEVEL
				expandedData.push(packedOscData[15] & 0x01); // OSC MODE (fixed/ratio) 0-1 
				expandedData.push((packedOscData[15] >> 1) & 0x1F); // OSC FREQ COARSE        0-31
				expandedData.push(packedOscData[16]); // OSC FREQ FINE          0-99
				expandedData.push((packedOscData[12] >> 3) & 0x0F); // OSC DETUNE             0-14       0: det=-7
			}
			expandedData.push(midiData[102]);
			expandedData.push(midiData[103]);
			expandedData.push(midiData[104]);
			expandedData.push(midiData[105]);
			expandedData.push(midiData[106]);
			expandedData.push(midiData[107]);
			expandedData.push(midiData[108]);
			expandedData.push(midiData[109]);
			expandedData.push(midiData[110] & 0x1F); //134 ALGORITHM 0-31
			expandedData.push(midiData[111] & 0x07); //135 FEEDBACK 0-7
			expandedData.push((midiData[111] >> 3) & 0x01); //136 OSCILLATOR SYNC 0-1
			expandedData.push(midiData[112]); //137 LFO SPEED 0-99
			expandedData.push(midiData[113]); //DELAY
			expandedData.push(midiData[114]); //PITCH MOD DEPTH   
			expandedData.push(midiData[115]); //AMP   MOD DEPTH  
			expandedData.push(midiData[116] & 0x01); //141 LFO SYNC 0-1
			expandedData.push((midiData[116] >> 1 )& 0x0F); //142 WAVEFORM 0-5
			expandedData.push((midiData[116] >> 5 )& 0x03); //143 PITCH MOD SENSITIVITY 0-7
			expandedData.push(midiData[117]); //TRANSPOSE 0-48
			expandedData.push(midiData[118]); //ASCII
			expandedData.push(midiData[119]); //ASCII
			expandedData.push(midiData[120]); //ASCII
			expandedData.push(midiData[121]); //ASCII
			expandedData.push(midiData[122]); //ASCII
			expandedData.push(midiData[123]); //ASCII
			expandedData.push(midiData[124]); //ASCII
			expandedData.push(midiData[125]); //ASCII
			expandedData.push(midiData[126]); //ASCII
			expandedData.push(midiData[127]); //ASCII
			return expandedData;
		},
		pack : function(midiData) {

		}
	};
	return obj;
}]);
