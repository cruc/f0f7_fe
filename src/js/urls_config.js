angular.module('f0f7App').constant('urls', {
     // BASE: 'https://app.dev/fe',
     // API_ENDPOINT: 'https://app.dev/be/api'
     BASE: 'https://f0f7.net/fe',
     API_ENDPOINT: 'https://f0f7.net/be/api'
 });