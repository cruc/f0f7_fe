// F0	:Start of System Exclusive
// 00	:Manufacturer ID 1		(Access Music Electronics)
// 20	:Manufacturer ID 2		(Access Music Electronics)
// 33	:Manufacturer ID 3		(Access Music Electronics)
// 01	:Product ID 			(Virus)
// dd	:Device ID			00..0F: individual; 10: omni.
// [message]
// F7	:End of System Exclusive


angular.module('f0f7.devices').factory('AccessVirus', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'AccessVirus',
		manuf 		: 'Access',
		model 		: 'Virus',
		hints 		: [
			'In menu SYSTEM/MULTI EDIT set DeviceId to <strong>Omni</strong>',
		],
		params : {
			deviceId	: 0x10, //DeviceId omni
			banks : {
				bank1 : {label : 'Virus Bank A', address : 0x01},
				bank2 : {label : 'Virus Bank B', address : 0x02},
				bank3 : {label : 'Virus Bank C', address : 0x03},
				bank4 : {label : 'Virus Bank D', address : 0x04}
			},
			patchesPerBank : 128,
			transferPause : 300,
			parserConfig : {
				name : {
					from : 249,
					to : 258
				},
				dna : {
					from : 9,
					to : 265
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			// Sysex.dumpData(midiData, 16);

			midiData[5] = this.params.deviceId;
			midiData[7] = 0x00; //BankNumber
			midiData[8] = 0x40; //ProgramNumber Single buffer in Single Mode (40)
			midiData[midiData.length-2] = Sysex.calculateAccessChecksum(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[5] = this.params.deviceId;
			midiData[7] = bank; //BankNumber
			midiData[8] = prgNo; //Prg No
			midiData[midiData.length-2] = Sysex.calculateAccessChecksum(midiData);

			return midiData;
		},
		retrieveBank : function(bank) {
			var midiData = [0xF0, 0x00, 0x20, 0x33, 0x01, this.params.deviceId, 0x32, bank, 0xF7];
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			this.setDnaParams(midiData);
			return [Sysex.extractPatchName(midiData, this.params.parserConfig)];
		},
		setDnaParams : function(midiData) {
			this.params.parserConfig.dna.to = midiData.length-2; //different dna for virus a,b,c and ti
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 10; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			// var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);
			// var midiData = Sysex.changeNameinMidiData(newNameArray, nameLength, program.data, this.params.parserConfig);
			// // midiData[midiData.length-2] = Sysex.calculateAccessChecksum(midiData);
			// return Sysex.extractPatchName(midiData, this.params.parserConfig);

			var midiData = Sysex.objectToArray(program.data);
			this.setDnaParams(midiData);
			return Sysex.renameProgram(newName, nameLength, midiData, this.params.parserConfig);
		}
	};
	return obj;
});
