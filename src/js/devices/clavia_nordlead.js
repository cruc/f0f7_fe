// $F0 System Exclusive
// $33 Manufacturer ID (clavia)
// <Device ID> = Global MIDI Channel, 0-15
// $04 Model ID for Nord Lead
// <Message Type> See each type of message, below.
// <Message Specification> See each type of message, below.
// <Data 1> This and following bytes depend on the Message Type and Message
// Specification. Some messages have no data bytes at all.
// <Data 2>
// <Data 3>
// <etc.>
// $F7 End Of Exclusive


angular.module('f0f7.devices').factory('ClaviaNordlead', function($log, Sysex, $timeout, WebMIDI) {
	var obj = {
		id 			: 'ClaviaNordlead',
		manuf 		: 'Clavia',
		model 		: 'Nordlead',
		params : {
			deviceId	: 0x02, //= Global MIDI Channel. 0 to 15 ($0-$F)
			banks : {
				bank1 : {label : 'Nordlead Patch Slot A', address : 0x01},
				// bank2 : {label : 'Nordlead Patch Slot B', address : 0x02},
				// bank3 : {label : 'Nordlead Patch Slot C', address : 0x03},
				// bank4 : {label : 'Nordlead Patch Slot D', address : 0x04}
			},
			patchesPerBank : 40,//99 rom, 40 ram
			transferPause : 200,
			parserConfig : {
				dna : {
					from : 6,
					to : 139
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			// Sysex.dumpData(midiData, 16);

			midiData[2] = this.params.deviceId;
			midiData[4] = 0x00; //$00 to $04 0 to 4 Message Type specifies the Bank. 0=Edit Buffer, 1 to 4=Bank 1 to 4

			// midiData[5] = 0x00; //If Message Type = 0 (Edit Buffer), the Message Specification can be 0 to 3, corresponding to Patch Slot buttons A to D.
			// Sysex.dumpData(midiData, 16);

			var msgSpecAddress = [0x00,0x01,0x02,0x03]; //If Message Type = 0 (Edit Buffer), the Message Specification can be 0 to 3, corresponding to Patch Slot buttons A to D.

			//We dump to all buffers!
			var dumpDataToBuffer = function(bufferSlot) {
				midiData[5] = bufferSlot;
				Sysex.sendSysex(midiData, WebMIDI.midiOut);

				if(msgSpecAddress.length > 0) {
					bufferSlot = msgSpecAddress.splice(0,1);
					$timeout(dumpDataToBuffer, obj.params.transferPause, true, bufferSlot);
				} else {
					$log.info("Buffer dump completed");
				}
			};
			dumpDataToBuffer(msgSpecAddress.splice(0,1));
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[2] = this.params.deviceId;
			// midiData[4] = bank; //BankNumber
			midiData[4] = 0x01; //BankNumber
			midiData[5] = prgNo; //Prg No

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x33, this.params.deviceId, 0x04];
			var requestBank = 0x0B;// Message Type specifies the Bank. $A=Edit Buffer, $B to $E=Bank 1 to 4
			// switch(bank) {
			// 	case 0x01 : requestBank = 0x0B;break;
			// 	case 0x02 : requestBank = 0x0C;break;
			// 	case 0x03 : requestBank = 0x0D;break;
			// 	case 0x04 : requestBank = 0x0E;break;
			// }
			// console.log('requestBank ' , requestBank);

			Sysex.requestAllProgramsOfBank(requestBank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			var data = Sysex.objectToArray(midiData);
			var dna = Sysex.createDNA(data, this.params.parserConfig.dna.from, this.params.parserConfig.dna.to);
			return [{name : 'Vikings need no name', dna : dna, data : data}];
		},
		getMaxNameLength : function() {
			return 0;
		},
		renameProgram : function(newName, program) {
			return program;
		}
	};
	return obj;
});
