angular.module('f0f7.devices').factory('WaldorfBlofeld', function($log, $timeout, Sysex, WebMIDI) {
	var obj = {
		id 			: 'WaldorfBlofeld',
		manuf 		: 'Waldorf Music',
		model 		: 'Blofeld',
		hints 		: [
			'In <strong>GLOBAL</strong> menu set <strong>Device ID</strong> to <strong>0</strong>',
		],
		params : {
			deviceId	: 0x00, //00h to 7Eh, 7Fh = broadcast
			banks : {
				bank1 : {label : 'Blofeld Bank A', address : 0x00},
				bank2 : {label : 'Blofeld Bank B', address : 0x01},
				bank3 : {label : 'Blofeld Bank C', address : 0x02},
				bank4 : {label : 'Blofeld Bank D', address : 0x03},
				bank5 : {label : 'Blofeld Bank E', address : 0x04},
				bank6 : {label : 'Blofeld Bank F', address : 0x05},
				bank7 : {label : 'Blofeld Bank G', address : 0x06},
				bank8 : {label : 'Blofeld Bank H', address : 0x07}
			},
			patchesPerBank : 128,
			transferPause : 400,
			parserConfig : {
				name : {
					from : 370,
					to : 389
				},
				dna : {
					from : 7,
					to : 390
				}
			}
		},
		sendToBuffer : function(program) {
			// var checksum = 0x7F; //A checksum of 7Fh is always accepted as valid.
			var midiData = Sysex.objectToArray(program.data);

			// Sysex.dumpData(midiData, 16);

			midiData[3] = this.params.deviceId;
			midiData[5] = 0x7F; //7F 00 Sound Mode Edit Buffer
			midiData[6] = 0x00; //
			midiData[midiData.length-2] = this.calculateBlofeldChecksum(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			// Sysex.dumpData(midiData, 16);

			midiData[3] = this.params.deviceId;
			midiData[5] = bank;
			midiData[6] = prgNo;
			midiData[midiData.length-2] = this.calculateBlofeldChecksum(midiData);

			return midiData;
		},
		retrieveBank : function(bank) {
			var midiData = [];
			var prgNo = 0;
			var patchesPerBank = this.params.patchesPerBank;
			var transferPause = this.params.transferPause;
			var deviceId = this.params.deviceId;

			var delayedProgramSend = function(prgNo) {
				$log.log(prgNo + ' / ' + patchesPerBank);
				midiData = [0xF0, 0x3E, 0x13, deviceId, 0x00, bank, prgNo, 0xF7];
				Sysex.sendSysex(midiData, WebMIDI.midiOut);

				if(prgNo < (patchesPerBank-1)) {
					prgNo++;
					$timeout(delayedProgramSend, transferPause, true, prgNo);
				}
			};
			delayedProgramSend(prgNo);
		},
		extractPatchNames : function(midiData) {
			return [Sysex.extractPatchName(midiData, this.params.parserConfig)];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return ?; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			return Sysex.renameProgram(newName, nameLength, program.data, this.params.parserConfig);
		},
        calculateBlofeldChecksum : function(midiData) {
            // Sum of all databytes truncated to 7 bits.
            // The addition is done in 8 bit format, the result is
            // masked to 7 bits (00h to 7Fh). A checksum of 7Fh is
            // always accepted as valid.
            // IMPORTANT: the MIDI status-bytes as well as the
            // ID's are not used for computing the checksum.

            var checksum = 0x00;
            for(var i=7; i<(midiData.length-2); i++) {
                checksum += midiData[i];
            }
            return checksum & 0x7F;
        }
	};
	return obj;
});
