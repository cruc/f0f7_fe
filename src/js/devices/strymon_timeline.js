angular.module('f0f7.devices').factory('StrymonTimeline', function($log, Sysex, $timeout, WebMIDI) {
	var obj = {
		id 			: 'StrymonTimeline',
		manuf 		: 'Strymon',
		model 		: 'Timeline',
		params : {
			banks : {
				bank1 : {label : 'Strymon Bank', address : 0x00},
			},
			patchesPerBank : 200,
			transferPause : 200,
			parserConfig : {
				name : {
					from : 632,
					to : 647
				},
				dna : {
					from : 9,
					to : 648
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[6] = 0x62;
			midiData[7] = 0x01; //BankNumber
			midiData[8] = 0x48; //ProgramNumber Single buffer in Single Mode 
			midiData[midiData.length-2] = Sysex.calculateStrymonChecksum(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			if(prgNo > 0x7F) {
				prgNo = prgNo - 0x80;
				bank = bank + 1;
			}

			midiData[6] = 0x62; //MIDI_WRITE_SINGLE_PATCH
			midiData[7] = bank; //BankNumber
			midiData[8] = prgNo; //Prg No
			midiData[midiData.length-2] = Sysex.calculateStrymonChecksum(midiData);

			return midiData;
		},
		retrieveBank : function(bank) {
			//Damage Control Engineering 10 bytes	F0 00 01 55 12 01 63 00 00 F7
			var header = [0xF0, 0x00, 0x01, 0x55, 0x12, 0x01, 0x63];
			this.requestAllProgramsOfBank(bank, header, this.params.patchesPerBank, this.params.transferPause);
		},
		requestAllProgramsOfBank : function(bank, header, patchesPerBank, transferPause) {
			var dumpRequest = [];
			var prgNo = 0;
			var prgNoHex = 0x00;

			var delayedDumpRequest = function(prgNo) {
				$log.debug("Requesting Prg " + prgNo);
				dumpRequest = header.concat([bank, prgNoHex, 0xF7]);
				Sysex.sendSysex(dumpRequest, WebMIDI.midiOut);

				if(prgNo < (patchesPerBank-1)) {
					prgNo++;
					prgNoHex++;

					if(prgNoHex> 0x7F) { //Hex cannot be greater than 127. Set to 0x00 and increase bank.
						bank++;
						prgNoHex = 0x00;
					}

					$timeout(delayedDumpRequest, transferPause, true, prgNo);
				} else {
					$log.info("Bank dumpRequest completed");
				}
			};
			delayedDumpRequest(prgNo);
		},
		extractPatchNames : function(midiData) {
			//Damage Control Engineering 10 bytes	F0 00 01 55 12 01 01 48 45 F7
			if(midiData[9] == 0xF7) {
				$log.info("Respond to strange response from Strymon with even stranger response");
				Sysex.sendSysex([0xF0, 0x00, 0x01, 0x55, 0x12, 0x01, 0x26, 0xF7], WebMIDI.midiOut);
				return [];
			} else {
				return [Sysex.extractPatchName(midiData, this.params.parserConfig)];
			}
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 10; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var midiData = Sysex.objectToArray(program.data);
			newName = newName.toUpperCase();
			return Sysex.renameProgram(newName, nameLength, midiData, this.params.parserConfig);
		}
	};
	return obj;
});
