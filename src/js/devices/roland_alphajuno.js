angular.module('f0f7.devices').factory('RolandAlphajuno', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'RolandAlphajuno',
		manuf 		: 'Roland',
		model 		: 'Alpha Juno [Experimental]',
		hints 		: [
			'Set the MEMORY PROTECT SWITCH on the back of the keyboard to the OFF position',
			'Press [ MIDI ] and the display will read MIDI CH = #',
			'Keep pressing [ MIDI ] and turn the α-Dial until all of the settings look like this:',
			'MIDI CHANNEL= 1 / MIDI OMNI = OFF / MIDI LOCAL = ON / MIDI EXCL =ON / MIDI PROG C = ON',
			'Press [ DATA TRANSFER ] - [ WRITE ] - [ BULK LOAD ] all at the same time until the display reads Bulk Load MIDI.o (Notice the lowercase "o" !)'
		],
		params : {
			deviceId	: 0x00, //Unit # = MIDI Base Ch 0 - 15
			singleProgramsCannotBeTransmitted : true,
			banks : {
				bank1 : {label : 'Memory', address : 0x01},
			},
			patchesPerBank : 64,
			transferPause : 300,
			buffer 		: [],
			toneNo : 0,
			parserConfig : {
				name : {
					from : 21,
					to : 31
				},
				dna : {
					from : 0,
					to : 31
				}
			}
		},
		rolandCharset : [
			'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
			'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
			'0','1','2','3','4','5','6','7','8','9',' ','-'
		],
		sendToBuffer : function(program) {
			var header = [0xF0, 0x41, 0x35, this.params.deviceId, 0x23, 0x20, 0x01];
			var rawData = Sysex.objectToArray(program.data);
			var expandedData = this.expand(rawData);
			var eox = [0xF7];
			var midiData = header.concat(expandedData, eox);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			if(midiData.length > 0) {
				Sysex.sendSysex(midiData, WebMIDI.midiOut);
			}
		},
		reorderProgram : function(program, bank, prgNo) {
			if(prgNo === 0) {
				this.params.buffer = [];
				this.params.toneNo = 0;
			}
			var rawData = Sysex.objectToArray(program.data);
			this.params.buffer = this.params.buffer.concat(rawData);
			this.params.toneNo = this.params.toneNo + 1;

			if(this.params.toneNo % 4 === 0) { //Alpha Juno groups 4 tones in one dump
				var header = [0xF0, 0x41, 0x37, this.params.deviceId, 0x23, 0x20, 0x01, 0x00, (this.params.toneNo - 4)];
				var eox = [0xF7];
				
				var packedData = this.nibbleData(this.params.buffer);

				var midiData = header.concat(packedData, eox);
				this.params.buffer = [];
				return midiData;
			}
			return [];
		},
		retrieveBank : function(bank) {
			var midiData = [0xF0, 0x41, 0x41, this.params.deviceId, 0x23, 0xF7]; //Request a file RQF
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			var program = [];
			midiData = Sysex.objectToArray(midiData);

			switch(midiData[2]) {
				case 0x40 : this.sendACK(); break; //WSF : Want to sendfile
				case 0x37 : program = this.extractMultiDump(midiData); break;
				case 0x42 : program = this.extractMultiDump(midiData); 
					this.sendACK();
					break; //DAT
				case 0x45 : this.sendACK(); break; //EOF
			}
			return program;
		},
		extractMultiDump : function(midiData) {
			var parserConfig = this.params.parserConfig;
			if(midiData.length == 266) {
				//Bulk Dump contains 4 32 Bit tones
				var rawData = this.getRawData(midiData);
				var unpackedData = this.deNibbleData(rawData);

				var chunk = 32;
				var tone = [];
				var tones = [];

				for (i=0,j=unpackedData.length; i<j; i+=chunk) {
					tone = unpackedData.slice(i,i+chunk);
					var nameArray = tone.slice(parserConfig.name.from, parserConfig.name.to);
					var patchName = this.nameArrayToString(nameArray);
					var dna = Sysex.createDNA(tone, parserConfig.dna.from, parserConfig.dna.to);

					tones.push({name : patchName, dna : dna, data : tone});
				}
				return tones;

			} else {
				return [];
			}
		},
		sendACK : function() {
			console.log("Send ACK");
			var ack = [0xF0, 0x41, 0x43, this.params.deviceId, 0x23, 0xF7]; //ACK
			Sysex.sendSysex(ack, WebMIDI.midiOut);
		},
		getRawData : function(midiData) {
			return midiData.slice(9, 265);
		},
		nibbleData : function(midiData) {
			//converts 1 to 2 bytes
			// xx yy   data, sent nybblewise: 
			//         a data byte dd is sent as xx=dd&0x0F; yy=dd>>4
			
			var nibbledData = [];
			for(var i=0; i<midiData.length; i=i+1) {
				var xx = midiData[i] & 0x0F;
				var yy = midiData[i] >> 4;
				nibbledData.push(xx);
				nibbledData.push(yy);
			}

			return nibbledData;
		},
		deNibbleData : function(midiData) {
			var denibbledData = [];
			for(var i=0; i<midiData.length; i=i+2) {
				var xx = midiData[i] & 0x0F;
				var yy = midiData[i+1] & 0x0F;
				var dd = (yy << 4) | xx;
				denibbledData.push(dd);
			}

			return denibbledData;
		},
		setDnaParams : function(midiData) {
			this.params.parserConfig.dna.to = midiData.length-2; //different dna for virus a,b,c and ti
		},
		getMaxNameLength : function() {
			return 10;
		},
		nameArrayToString : function(nameArray) {
			var patchName = '';
			for(var i=0; i<nameArray.length; i++) {
				patchName += this.mapRolandHexToASCII(nameArray[i] & 0x3F);
			}
			patchName = patchName.trim();
			if(patchName.length === 0) {
				patchName = 'no name';
			}
			return patchName;
		},
		mapRolandHexToASCII : function(hex) {
			if(typeof this.rolandCharset[hex] !== 'undefined') {
				return this.rolandCharset[hex];
			} else {
				return ' ';
			}
		},
		renameProgram : function(newName, program) {
			var parserConfig = this.params.parserConfig;
			var nameLength = this.getMaxNameLength();
			var newNameArray = this.createPaddedArrayFromCharset(newName);

			var midiData = Sysex.objectToArray(program.data);

			for (var i = 0; i < newNameArray.length; i++) {
				midiData[i+21] = (midiData[i+21] & 0xC0) | (newNameArray[i] & 0x3F);
			}
			
			var renamedPrg = {};

			renamedPrg.name = this.nameArrayToString(newNameArray);
			renamedPrg.dna = Sysex.createDNA(midiData, parserConfig.dna.from, parserConfig.dna.to);
			renamedPrg.data = midiData;

			return renamedPrg;
		},
		createPaddedArrayFromCharset : function (str) {
			var nameLength = this.getMaxNameLength();
			var bytes = [];
			var charCode, index;
			
			for (var i = 0; i < nameLength; i++) {
			  if(typeof str[i] === 'undefined') {
			  	index = 62; //index of space
			  } else {
			  	index = this.rolandCharset.indexOf(str[i]);
			  }
			  bytes.push(index);
			}
		  	return bytes;
		},
		expand : function(midiData) {
			var expandedData = [];
			
			var b01 = (midiData[5] & 0x80) >> 6;
			var b02 = (midiData[6] & 0x80) >> 7;
			expandedData.push(b01 | b02); //DCO Env Mode

			var b03 = (midiData[7] & 0x80) >> 6;
			var b04 = (midiData[8] & 0x80) >> 7;
			expandedData.push(b03 | b04); //VCF Env Mode

			var b05 = (midiData[9] & 0x80) >> 6;
			var b06 = (midiData[10] & 0x80) >> 7;
			expandedData.push(b05 | b06); //VCA Env Mode

			var b13 = (midiData[17] & 0x80) >> 6;
			var b14 = (midiData[18] & 0x80) >> 7;
			expandedData.push(b13 | b14); //DCO W Pulse

			var b10 = (midiData[14] & 0x80) >> 5;
			var b11 = (midiData[15] & 0x80) >> 6;
			var b12 = (midiData[16] & 0x80) >> 7;
			expandedData.push(b10 | b11 | b12); //DCO W Saw

			var b07 = (midiData[11] & 0x80) >> 5;
			var b08 = (midiData[12] & 0x80) >> 6;
			var b09 = (midiData[13] & 0x80) >> 7;
			expandedData.push(b07 | b08 | b09); //DCO W Sub

			var b17 = (midiData[21] & 0x80) >> 6;
			var b18 = (midiData[22] & 0x80) >> 7;
			expandedData.push(b17 | b18); //DCO Range

			var b19 = (midiData[23] & 0x80) >> 6;
			var b20 = (midiData[24] & 0x80) >> 7;
			expandedData.push(b19 | b20); //DCO Sub level

			var b21 = (midiData[25] & 0x80) >> 6;
			var b22 = (midiData[26] & 0x80) >> 7;
			expandedData.push(b21 | b22); //DCO Noise level

			var b15 = (midiData[19] & 0x80) >> 6;
			var b16 = (midiData[20] & 0x80) >> 7;
			expandedData.push(b15 | b16); //HPF Cutoff Freq

			var b00 = (midiData[4] & 0x80) >> 7;
			expandedData.push(b00); //Chorus

			expandedData.push(midiData[3] & 0x7F); //DCO LFO Mod Depth
			expandedData.push(midiData[4] & 0x7F); //DCO Env Mod Depth
			expandedData.push((midiData[0] & 0xF0) >> 4); //DCO After Depth
			expandedData.push(midiData[5] & 0x7F); //DCO PWM Depth
			expandedData.push(midiData[6] & 0x7F); //DCO PWM Rate

			expandedData.push(midiData[7] & 0x7F); //VCF Cut Freq
			expandedData.push(midiData[8] & 0x7F); //VCF Res
			expandedData.push(midiData[10] & 0x7F); //VCF LFO MOD Depth
			expandedData.push(midiData[9] & 0x7F); //VCF Env MOD Depth
			expandedData.push(midiData[0] & 0x0F); //VCF Key Follow
			expandedData.push((midiData[1] & 0xF0) >> 4); //VCF After Depth

			expandedData.push(midiData[11] & 0x7F); //VCA Level
			expandedData.push(midiData[1] & 0x0F); //VCA After Depth
			expandedData.push(midiData[12] & 0x7F); //LFO Rate
			expandedData.push(midiData[13] & 0x7F); //LFO Delay

			expandedData.push(midiData[14] & 0x7F); //Env
			expandedData.push(midiData[15] & 0x7F); //Env
			expandedData.push(midiData[16] & 0x7F); //Env
			expandedData.push(midiData[17] & 0x7F); //Env
			expandedData.push(midiData[18] & 0x7F); //Env
			expandedData.push(midiData[19] & 0x7F); //Env
			expandedData.push(midiData[20] & 0x7F); //Env

			expandedData.push((midiData[2] & 0xF0) >> 4); //Env Key Follow

			var c0 = (midiData[27] & 0x40) >> 6;
			var c1 = (midiData[27] & 0x80) >> 6;
			var c2 = (midiData[28] & 0x40) >> 4;
			var c3 = (midiData[28] & 0x80) >> 4;
			var c4 = (midiData[29] & 0x40) >> 2;
			var c5 = (midiData[29] & 0x80) >> 2;
			var c6 = (midiData[30] & 0x40);
			var c7 = (midiData[30] & 0x80);
			expandedData.push(c0 | c1 | c2 | c3 | c4 | c5 | c6 | c7); //Chorus Rate

			expandedData.push(midiData[2] & 0x0F); //Bender Range

			return expandedData;
		},
	};
	return obj;
});
