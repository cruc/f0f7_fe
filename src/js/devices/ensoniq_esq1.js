/**
 * ESQ-1 SysEx Header
 * 
 * 11110000 Statusbyte
 * 00001111	Ensoniq ID
 * 00000010	ESQ-1
 * 0000nnnn MIDI Channel
 * 
 * 0xF0 0x0F 0x02 0x00
 *
 * Single Program Dump
 * 
 */


angular.module('f0f7.devices').factory('EnsoniqEsq1', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'EnsoniqEsq1',
		manuf 		: 'Ensoniq',
		model 		: 'ESQ-1',
		hints 		: [
			'ESQ-1 must be in program select mode (i.e. one of the program bank pages must be displayed)',
			'Set MIDI Channel to <strong>1</strong>',
			'MIDI enable parameter on the MIDI page must be set to <strong>ENABLE=KEYS+CT+PC+SS+SX</strong>',
			'This has to be done everytime you switch the ESQ-1 on.',
			'You have to add at least <strong>40</strong> programs to a bank or <strong>Overwrite Bank</strong> will not work',
			'Every time you prelisten a program on the ESQ-1 you have to press <strong>EXIT</strong>, else the next program will not be received'
		],
		params : {
			singleProgramsCannotBeTransmitted : true,
			banks : {
				bank1 : {label : 'ESQ-1 Bank 1', address : 0x00},
			},
			patchesPerBank : 40,
			transferPause : 15,
			buffer : [],
			parserConfig : {
				name : {
					from : 0,
					to : 5
				},
				dna : {
					from : 0,
					to : 204
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			Sysex.dumpData(midiData, 16);

			midiData[4] = 0x01; //Single Program Dump Code

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			if(midiData.length > 0) {
				Sysex.sendSysex(midiData, WebMIDI.midiOut);
			}
		},
		reorderProgram : function(program, bank, prgNo) {
			if(prgNo === 0) {
				this.params.buffer = [];
			}
			var rawData = this.getRawData(Sysex.objectToArray(program.data));
			this.params.buffer = this.params.buffer.concat(rawData);

			if(prgNo == (this.params.patchesPerBank-1)) {
				var header = [0xF0, 0x0F, 0x02, 0x00, 0x02];
				var eox = [0xF7];
				var midiData = header.concat(this.params.buffer, eox);
				this.params.buffer = [];
				return midiData;
			}
			return [];
		},
		retrieveBank : function(bank) {
			var midiData = [0xF0, 0x0F, 0x02, 0x00, 0x0A, 0xF7]; //All Program Dump Request
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			var program = [];

			midiData = Sysex.objectToArray(midiData);

			switch(midiData[4]) {
				case 0x01 : program = this.extractSinglePatch(midiData); break;
				case 0x02 : program = this.extractMultiDump(midiData); break;
			}
			
			return program;
		},
		extractSinglePatch : function(midiData) {
			if(midiData.length == 210) {
				var rawData = this.getRawData(midiData);
				var unpackedData = this.deNibbleData(rawData);

				// Sysex.dumpData(unpackedData, 16);

				var program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
				program.data = midiData;
				return [program];
			} else {
				return [];
			}
		},
		extractMultiDump : function(midiData) {
			var rawData = midiData.slice(5, (midiData.length-1));
			var unpackedData = this.deNibbleData(rawData);

			var chunk = 102;
			var i,j,data;
			var patchNames = [];
			var program = [];
			var header = [0xF0, 0x0F, 0x02, 0x00, 0x01];

			for (i=0,j=unpackedData.length; i<j; i+=chunk) {
				data = unpackedData.slice(i,i+chunk);

				// Sysex.dumpData(data, 16);

				program = Sysex.extractPatchName(data, this.params.parserConfig);
				program.data = header.concat(this.nibbleData(data), [0xF7]);
				
				patchNames.push(program);
			}
			return patchNames;
		},
		getRawData : function(midiData) {
			return midiData.slice(5, 209);
		},
		nibbleData : function(midiData) {
			//converts 1 to 2 bytes
			// xx yy   data, sent nybblewise: 
			//         a data byte dd is sent as xx=dd&0x0F; yy=dd>>4
			
			var nibbledData = [];
			for(var i=0; i<midiData.length; i=i+1) {
				var xx = midiData[i] & 0x0F;
				var yy = midiData[i] >> 4;
				nibbledData.push(xx);
				nibbledData.push(yy);
			}

			return nibbledData;
		},
		deNibbleData : function(midiData) {
			var denibbledData = [];
			for(var i=0; i<midiData.length; i=i+2) {
				var xx = midiData[i] & 0x0F;
				var yy = midiData[i+1] & 0x0F;
				var dd = (yy << 4) | xx;
				denibbledData.push(dd);
			}

			return denibbledData;
		},
		getMaxNameLength : function() {
			return 6;
		},
		renameProgram : function(newName, program) {			
			newName = newName.toUpperCase();
			var nameLength = this.getMaxNameLength();

			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);
			var rawData = this.getRawData(Sysex.objectToArray(program.data));
			var unpackedData = this.deNibbleData(rawData);

			var renamedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);

			var nibbledData = this.nibbleData(renamedData);
			var header = [0xF0, 0x0F, 0x02, 0x00, 0x01];
			midiData = header.concat(nibbledData, [0xF7]);

			program = Sysex.extractPatchName(renamedData, this.params.parserConfig);
			program.data = midiData;
			return program;
		},
	};
	return obj;
});
