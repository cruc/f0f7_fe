angular.module('f0f7.devices').factory('RolandJv80', function($log, $timeout, Sysex, WebMIDI) {
	var patchData = {};
	var dna;
	var obj = {
		id 			: 'RolandJv80',
		manuf 		: 'Roland',
		model 		: 'JV-80',
		hints 		: [
			'Press <strong>WRITE</strong>.',
			'Use the <strong>CURSOR RIGHT</strong> button to select <strong>PROTECT</strong> and press <strong>ENTER</strong>',
			'Use <strong>PARAMETER SLIDER 1</strong> to change the <strong>WRITE PROTECT</strong> memory protect value to <strong>OFF</strong>',
			'Press <strong>EXIT</strong> twice',
			'Press <strong>MIDI</strong>',
			'Use the <strong>DOWN</strong> <strong>ARROW</strong> button to select the <strong>SYS-EX</strong> display screen.',
			'Use <strong>PARAMETER SLIDER 2</strong> to set the Unit # to <strong>17</strong>.'
		],
		params : {
			deviceId	: 0x10, //JV deviceId 0x10 == 17
			banks : {
				bank1 : {label : 'Internal', address : 0x00},
			},
			patchesPerBank : 64,
			transferPause : 40,
			parserConfig : {
				name : {
					from : 9,
					to : 20
				},
				dna : {
					from : 7,
					to : 124
				}
			}
		},
		sendToBuffer : function(program) {
			var programKeys = ['PatchCommon', 'PatchTone1', 'PatchTone2', 'PatchTone3', 'PatchTone4'];
			// var programKeys = Object.keys(program.data);

			var dumpProgramParts = function(programKey) {
				programKey = programKey[0];
				// $log.log('programKey ' , programKey);
				var midiData = Sysex.objectToArray(program.data[programKey]);

				var offset = 0x00;
				switch(programKey) {
					case 'PatchCommon' : offset = 0x00; break;
					case 'PatchTone1'  : offset = 0x08; break;
					case 'PatchTone2'  : offset = 0x09; break;
					case 'PatchTone3'  : offset = 0x0A; break;
					case 'PatchTone4'  : offset = 0x0B; break;
				}
				
				// midiData = Array.from(midiData);
				midiData[2] = obj.params.deviceId; 	// DeviceId
				midiData[4] = 0x12; 				// Command ID DT1
				midiData[5] = 0x00; 				// [0x00, 0x08, 0x20, 0x00]; //Patch Mode Temporary Patch
				midiData[6] = 0x08; 	
				midiData[7] = 0x20 + offset; 	
				midiData[8] = 0x00;

				var checksum = Sysex.calculateRolandChecksum(midiData.slice(5, (midiData.length-2)));
				midiData[midiData.length-2] = checksum;

				Sysex.sendSysex(midiData, WebMIDI.midiOut);

				if(programKeys.length > 0) {
					programKey = programKeys.splice(0,1);
					$timeout(dumpProgramParts, obj.params.transferPause, true, programKey);
				} else {
					$log.info("Buffer dump completed");
				}
			};
			dumpProgramParts(programKeys.splice(0,1));
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var programData = program.data;
			var reorderedData = [];

			for(var programKey in programData) { 
			   if (programData.hasOwnProperty(programKey)) {
			       var midiData = programData[programKey];
					midiData = Sysex.objectToArray(midiData);

					var offset = 0x00;
					switch(programKey) {
						case 'PatchCommon' : offset = 0x00; break;
						case 'PatchTone1'  : offset = 0x08; break;
						case 'PatchTone2'  : offset = 0x09; break;
						case 'PatchTone3'  : offset = 0x0A; break;
						case 'PatchTone4'  : offset = 0x0B; break;
					}

					midiData[2] = obj.params.deviceId; 	// DeviceId
					midiData[4] = 0x12; 				// Command ID DT1
					midiData[5] = 0x01; 				
					midiData[6] = 0x40 + prgNo; 	
					midiData[7] = 0x20 + offset; 	
					midiData[8] = 0x00;

					midiData[midiData.length-2] = Sysex.calculateRolandChecksum(midiData.slice(5, (midiData.length-2)));	
					reorderedData = reorderedData.concat(midiData);
			   }
			}
			return reorderedData;
		},
		retrieveBank : function(bank) {
			var dumpRequest = [0xF0, 0x41, this.params.deviceId, 0x46, 0x11, 0x01, 0x40, 0x20, 0x00, 0x00, 0x40, 0x00, 0x00, 0x5F, 0xF7];
			// var checksum = Sysex.calculateRolandChecksum(dumpRequest.slice(5, (dumpRequest.length-2)));
			// dumpRequest[dumpRequest.length-2] = checksum;
			Sysex.sendSysex(dumpRequest, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			var program = [];
			switch(midiData[7]) {
				case 0x20 : 
					this.processPatchData('PatchCommon', midiData);
					break;
				case 0x28 : 
					this.processPatchData('PatchTone1', midiData);
					break;
				case 0x29 :
					this.processPatchData('PatchTone2', midiData);
					break;
				case 0x2A :
					this.processPatchData('PatchTone3', midiData);
					break;
				case 0x2B :
					this.processPatchData('PatchTone4', midiData);
					var patchName = this.getPatchName(patchData.PatchCommon);
					
					var dna = this.createDNA(patchData);

					program = [{name : patchName, dna : dna, data : patchData}];
					patchData = {};
					break;
			}
			return program;
		},
		processPatchData : function(patchKey, midiData) {
			patchData[patchKey] = new Uint8Array(midiData.length);
			patchData[patchKey] = midiData;
		},
		getPatchName : function(midiData) {
			var data = angular.fromJson(angular.toJson(midiData));
			var patchName = '';

			for(var k=this.params.parserConfig.name.from; k<=this.params.parserConfig.name.to; k++) {
				patchName += String.fromCharCode(data[k]);
			}
			return patchName.trim();
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return ?; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var midiData = Sysex.objectToArray(program.data.PatchCommon);

			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);
			midiData = Sysex.changeNameinMidiData(newNameArray, nameLength, midiData, this.params.parserConfig);
			midiData[midiData.length-2] = Sysex.calculateRolandChecksum(midiData.slice(5, (midiData.length-2)));

			program.data.PatchCommon = midiData;

			return {name : newName, dna : this.createDNA(program.data), data : program.data};
		},
		getPatchData : function() {
			return patchData;
		},
		createDNA : function(patchData) {
			var from = this.params.parserConfig.dna.from;
			var to = this.params.parserConfig.dna.to;
			var dnaArray = [];

			dnaArray = dnaArray.concat(patchData.PatchCommon.slice(7, patchData.PatchCommon.length-2));
			dnaArray = dnaArray.concat(patchData.PatchTone1.slice(from, to));
			dnaArray = dnaArray.concat(patchData.PatchTone2.slice(from, to));
			dnaArray = dnaArray.concat(patchData.PatchTone3.slice(from, to));
			dnaArray = dnaArray.concat(patchData.PatchTone4.slice(from, to));

			return Sysex.createDNA(dnaArray, 0, dnaArray.length);
		},
	};
	return obj;
});
