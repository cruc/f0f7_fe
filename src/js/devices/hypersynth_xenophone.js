angular.module('f0f7.devices').factory('HypersynthXenophone', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'HypersynthXenophone',
		manuf 		: 'Hypersynth',
		model 		: 'Xenophone',
		hints 		: [
			
		],
		params : {
			singleProgramsCannotBeTransmitted : true,
			banks : {
				bank1 : {label : 'Xenophone Bank 0', address : 0x00},
				bank2 : {label : 'Xenophone Bank 1', address : 0x01},
				bank3 : {label : 'Xenophone Bank 2', address : 0x02},
				bank4 : {label : 'Xenophone Bank 3', address : 0x03},
				bank5 : {label : 'Xenophone Bank 4', address : 0x04},
				bank6 : {label : 'Xenophone Bank 5', address : 0x05},
				bank7 : {label : 'Xenophone Bank 6', address : 0x06},
			},
			patchesPerBank : 128,
			transferPause : 150,
			parserConfig : {
				name : {
					from : 16,
					to : 32
				},
				dna : {
					from : 16,
					to : 279
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[4] = 0x0A; // Command ID single patch file
			midiData[5] = 0x07; // Bank # :Edit buffer
			midiData = this.embedChecksum(midiData);

			// Sysex.dumpData(midiData, 16);

			var header = [0xF0];
			var packedData = Sysex.packDSISysex(midiData);
			midiData = header.concat(packedData, 0xF7);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[4] = 0x0A; // Command ID single patch file
			midiData[5] = bank; // Bank 
			midiData[6] = prgNo; // prgNo
			midiData = this.embedChecksum(midiData);

			var header = [0xF0];
			var packedData = Sysex.packDSISysex(midiData);
			midiData = header.concat(packedData, 0xF7);

			return midiData;
		},
		// retrieveBank : function(bank) {
		// 	var midiData = [0xF0, 0x00, 0x00, 0x21, 0x7F, 0x03, 0x0B, bank, 0x00, 0xF7];
		// 	Sysex.sendSysex(midiData, WebMIDI.midiOut);
		// },
		retrieveBank : function(bank) {
			var header = [0xF0, 0x00, 0x00, 0x21, 0x7F, 0x03, 0x02];
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			var program = [];

			midiData = Sysex.objectToArray(midiData);
			var packedData = midiData.slice(1, midiData.length-1);
			var unpackedData = Sysex.unpackDSISysex(packedData);
			// Sysex.dumpData(unpackedData, 16);

			switch(unpackedData[4]) {
				case 0x0A : program = this.extractSinglePatch(unpackedData); break;
				case 0x0B : program = this.extractMultiDump(unpackedData); break;
			}
			return program;
		},
		extractMultiDump : function(unpackedData) {
			var chunk = 280;
			var i,j,data;
			var patchNames = [];

			for (i=0,j=unpackedData.length; i<j; i+=chunk) {
				data = unpackedData.slice(i,i+chunk);

				data[1] = 0x21;
				data[2] = 0x7F;
				data[3] = 0x03;

				patchNames.push(Sysex.extractPatchName(data, this.params.parserConfig));
			}
			return patchNames;
		},
		extractSinglePatch : function(unpackedData) {
			return [Sysex.extractPatchName(unpackedData, this.params.parserConfig)];
		},
		getMaxNameLength : function() {
			return 16; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);

			var unpackedData = Sysex.objectToArray(program.data);
			Sysex.dumpData(unpackedData, 16);
			
			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);
			unpackedData = this.embedChecksum(unpackedData);
			Sysex.dumpData(unpackedData, 16);

			program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			return program;
		},
		embedChecksum : function(unpackedData) {
			unpackedData = unpackedData.concat([]); //copy Array
			var checksum = this.calculateHypersynthChecksum(unpackedData);
			unpackedData[10] = checksum & 0xFF;
			unpackedData[9] = (checksum >> 8) & 0xFF;
			unpackedData[8] = (checksum >> 16) & 0xFF;
			unpackedData[7] = (checksum >> 24) & 0xFF;
			return unpackedData;
		},
		calculateHypersynthChecksum : function(midiData) {
			var sum = 0;
			for (var i = 16; i <= 279; i++) {
				sum += midiData[i];
			}
			return sum;
		}
	};
	return obj;
});
