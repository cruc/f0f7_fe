angular.module('f0f7.devices').factory('NovationBassstation2', function($log, Sysex, $timeout, WebMIDI) {
	var obj = {
		id 			: 'NovationBassstation2',
		manuf 		: 'Novation',
		model 		: 'Bass Station 2',
		hints 		: [
			'Be sure to have a current firmware on your <em>Bass Station 2</em>',
			'Devices with older firmware cannot store program names'
		],
		params : {
			deviceId	: 0x00, 
			banks : {
				bank1 : {label : 'Bank A', address : 0x01},
			},
			patchesPerBank : 128,
			transferPause : 150,
			parserConfig : {
				name : {
					from : 137,
					to : 152
				},
				dna : {
					from : 11,
					to : 153
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			// midiData[5] = this.params.deviceId;
			midiData[7] = 0x00; //Buffermode?
			midiData[8] = 0x00; //ProgramNumber Single buffer

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			// midiData[5] = this.params.deviceId;
			midiData[7] = 0x01; //Dumpmode?
			midiData[8] = prgNo; //Prg No

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x00, 0x20, 0x29, 0x00, 0x33, 0x00, 0x41];

			this.requestAllProgramsOfBank(header, this.params.patchesPerBank, this.params.transferPause);
		},
		requestAllProgramsOfBank : function(header, patchesPerBank, transferPause) {
			var dumpRequest = [];
			var prgNo = 0;

			var delayedDumpRequest = function(prgNo) {
				$log.debug("Requesting Prg " + prgNo);
				dumpRequest = header.concat([prgNo, 0xF7]);
				Sysex.sendSysex(dumpRequest, WebMIDI.midiOut);

				if(prgNo < (patchesPerBank-1)) {
					prgNo++;
					$timeout(delayedDumpRequest, transferPause, true, prgNo);
				} else {
					$log.info("Bank dumpRequest completed");
				}
			};
			delayedDumpRequest(prgNo);
		},
		extractPatchNames : function(midiData) {
			return [Sysex.extractPatchName(midiData, this.params.parserConfig)];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return ?; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var midiData = Sysex.objectToArray(program.data);
			return Sysex.renameProgram(newName, nameLength, midiData, this.params.parserConfig);
		}
	};
	return obj;
});
