angular.module('f0f7.devices').factory('DSIEvolver', function($log, Sysex, WebMIDI) {
	var specialEvolverProgramData = {};
	var dna;
	var obj = {
		id 			: 'DSIEvolver',
		manuf 		: 'Dave Smith Instruments',
		model 		: 'Evolver',
		hints 		: [
			'Should work with <strong>Mono</strong> and <strong>Poly Evolver</strong> and all Evolvers that support patch names',
			'In <strong>Global</strong> menu set <strong>MIDI SysEx</strong> to <strong>on</strong>',
		],
		params : {
			banks : {
				bank1 : {label : 'Evolver Bank 1', address : 0x00},
				bank2 : {label : 'Evolver Bank 2', address : 0x01},
				bank3 : {label : 'Evolver Bank 3', address : 0x02},
				bank4 : {label : 'Evolver Bank 4', address : 0x03}
			},
			patchesPerBank : 128,
			transferPause : 300,
			parserConfig : {
				name : {
					from : 7,
					to : 22
				},
				dna : {
					from : 7,
					to : 227
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);
			midiData[4] = 0x03; 	// Edit Buffer Data Dump
			midiData.splice(5, 2);	// Unset Bank and Prg
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			//Program Data Dump
			var programData = Sysex.objectToArray(program.data);

			programData[4] = 0x02; 	// Program Data Dump
			programData[5] = bank;		// 0-3
			programData[6] = prgNo;	// 0-127

			//Program Name Data Dump
			var header = [0xF0, 0x01, 0x20, 0x01, 0x11, bank, prgNo];
			var paddedNameArray = Sysex.createPaddedNameArray(this.getMaxNameLength(), program.name);
			var programNameData = header.concat(paddedNameArray, 0xF7);

			var midiData = programData.concat(programNameData);

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x01, 0x20, 0x01, 0x05];
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			var mode = 'unknown';
			switch(midiData[4]) {
				case 0x02 : mode = 'Program Data Dump'; break;
				case 0x11 : mode = 'Program Name Data Dump'; break;
				default: mode = 'unknown';
			}

			if (mode == 'Program Data Dump') {
				specialEvolverProgramData = new Uint8Array(midiData.length);
				specialEvolverProgramData = midiData;
				dna = Sysex.createDNA(specialEvolverProgramData, this.params.parserConfig.dna.from, this.params.parserConfig.dna.to);

			} else if (mode == 'Program Name Data Dump') {
				var data = angular.fromJson(angular.toJson(midiData));
				var patchName = '';

				for(var k=this.params.parserConfig.name.from; k<=this.params.parserConfig.name.to; k++) {
					patchName += String.fromCharCode(data[k]);
				}

				return [{name : patchName.trim(),  dna : dna, data : specialEvolverProgramData}];
			}
			return [];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return ?; 
		},
		renameProgram : function(newName, program) {
			return {name : newName.trim(),  dna : program.dna, data : program.data};
		}
	};
	return obj;
});
