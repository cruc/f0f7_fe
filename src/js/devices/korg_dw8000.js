angular.module('f0f7.devices').factory('KorgDw8000', function($log, $timeout, Sysex, WebMIDI) {
	var patchData = {};
	var dna;
	var obj = {
		id 			: 'KorgDw8000',
		manuf 		: 'Korg',
		model 		: 'DW-8000',
		hints 		: [
			'Make sure parameter 84 (MIDI channel) is set to 1.',
			'Make sure parameter 85 (Enable) is set to 2 (All).',
			'The Write switch on the back panel must be in the enable position.',
		],
		params : {
			deviceId	: 0x30, //= Format ID 3*H 0 to 15 
			banks : {
				bank1 : {label : 'Internal', address : 0x00},
			},
			patchesPerBank : 64,
			transferPause : 600,
			parserConfig : {
				dna : {
					from : 5,
					to : 57
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data.DataDump);
			midiData[2] = obj.params.deviceId; 	// DeviceId

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			//Send Program Change
			var prgChange = [0xC0, prgNo & 0x7F]; //1100nnnn 0ppppppp
			Sysex.sendSysex(prgChange, WebMIDI.midiOut);

			//Send Bulk Data
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);

			//Send Write Request
			var writeRequest = [0xF0, 0x42, this.params.deviceId, 0x03, 0x11, prgNo, 0xF7];
			Sysex.sendSysex(writeRequest, WebMIDI.midiOut);			
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data.DataDump);
			midiData[2] = this.params.deviceId;
			return midiData;
		},
		retrieveBank : function(bank) {
			this.requestAllProgramsOfBank(this.params.patchesPerBank, this.params.transferPause);
		},
		requestAllProgramsOfBank : function(patchesPerBank, transferPause) {
			var dumpRequest = [];
			var prgChange = [];
			var prgNo = 0;

			var delayedDumpRequest = function(prgNo) {
				$log.debug("Requesting Prg " + prgNo);

				//Send Program Change
				prgChange = [0xC0, prgNo & 0x7F]; //1100nnnn 0ppppppp
				Sysex.sendSysex(prgChange, WebMIDI.midiOut);
				$log.log('prgChange ' , prgChange);

				//Data Save Request
				dumpRequest = [0xF0, 0x42, obj.params.deviceId, 0x03, 0x10, 0xF7];
				Sysex.sendSysex(dumpRequest, WebMIDI.midiOut);
				$log.log('dumpRequest ' , dumpRequest);

				if(prgNo < (patchesPerBank-1)) {
					prgNo++;
					$timeout(delayedDumpRequest, transferPause, true, prgNo);
				} else {
					$log.info("Bank dumpRequest completed");
				}
			};
			delayedDumpRequest(prgNo);
		},
		extractPatchNames : function(midiData) {
			var program = [];
			var data;
			var dna;

			switch(midiData[4]) {
				case 0x40 : //DATA Dump
					data = Sysex.objectToArray(midiData);
					dna = Sysex.createDNA(data, this.params.parserConfig.dna.from, this.params.parserConfig.dna.to);
					program = [{name : 'DW-8000 Program', dna : dna, data : { DataDump : data}}];
					break;
			}
			return program;
		},
		getMaxNameLength : function() {
			return 0;
		}
	};
	return obj;
});
