angular.module('f0f7.devices').factory('DSIPro12', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'DSIPro12',
		manuf 		: 'Dave Smith Instruments',
		model 		: 'Pro 12 [beta]',
		hints 		: [
			'To prepare the Pro 12 to receive system exclusive messages:',
			'Press <strong>Global</strong>.',
			'From the Global menu, select <strong>MIDI Sysex Enable</strong> and make sure it\'s set to <strong>On</strong>.',
			'From the Global menu, select <strong>MIDI Sysex Cable.</strong>',
			'If you\'re using USB, choose <strong>USB</strong>. If you\'re using a MIDI interface, choose <strong>MIDI</strong>.',
			'Press <strong>Global</strong> again to exit the Global menu.',
		],
		params : {
			banks : {
				bank1 : {label : 'Pro 12 / User Bank 0', address : 0x00},
				bank2 : {label : 'Pro 12 / User Bank 1', address : 0x01},
				bank3 : {label : 'Pro 12 / User Bank 2', address : 0x02},
				bank4 : {label : 'Pro 12 / User Bank 3', address : 0x03},
				// bank5 : {label : 'Pro 2 / Bank 4', address : 0x04},
				// bank6 : {label : 'Pro 2 / Bank 5', address : 0x05},
				// bank7 : {label : 'Pro 2 / Bank 6', address : 0x06},
				// bank8 : {label : 'Pro 2 / Bank 7', address : 0x07},
			},
			patchesPerBank : 99,
			transferPause : 400,
			parserConfig : {
				rawData : {
					from: 6,
					to: 1177 //1171 bytes packed, 1024 expanded
				},
				name : {
					from : 402,
					to : 421,
				},
				dna : {
					from : 0,
					to : 1024
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);
			midiData[3] = 0x03; 	// Edit Buffer Data
			midiData.splice(4, 2);	// Unset Bank and Prg
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[3] = 0x02; 	// Program Data Dump
			midiData[4] = bank;		// 0 - 7
			midiData[5] = prgNo;	// 0 - 98

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x01, 0x2A, 0x05];
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			midiData = Sysex.objectToArray(midiData);
			var unpackedData = this.unpackMidiData(midiData);
			
			// Sysex.dumpData(unpackedData, 16);

			var program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			program.data = midiData;
			return [program];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 20; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, this.params.parserConfig.rawData.from);
			var unpackedData = this.unpackMidiData(midiData);

			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);
			program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			
			var packedData = Sysex.packDSISysex(unpackedData);
			program.data = header.concat(packedData, 0xF7);
			return program;
		},
		unpackMidiData : function(midiData) {
			var packedData = midiData.slice(this.params.parserConfig.rawData.from, this.params.parserConfig.rawData.to);
			var unpackedData = Sysex.unpackDSISysex(packedData);
			// Sysex.dumpData(unpackedData, 16);
			return unpackedData;
		}
	};
	return obj;
});
