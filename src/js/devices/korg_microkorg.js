angular.module('f0f7.devices').factory('KorgMicrokorg', function($log, $timeout, Sysex, WebMIDI) {
	var patchData = {};
	var dna;
	var obj = {
		id 			: 'KorgMicrokorg',
		manuf 		: 'Korg',
		model 		: 'microKORG / MS2000',
		hints 		: [
			'<strong>Set MIDI CH to 1</strong>',
			'<strong>Turn off write protect</strong>',
			'Hold down the <em>SHIFT</em> key and press the <em>8</em> key. The display will indicate <strong>ūtP<strong>',
			'Turn knob <em>1/Cutoff</em> to switch write protect',
			'<strong>Set the MIDI FILTER setting SYSTEM EXCLUSIVE to Enable (E-E).</strong>',
			'Hold down <em>SHIFT</em> and press Program Number 4. Display shows <strong>FLt.</strong>',
			'Turn knob <em>4/EG Release</em> to select <strong>E-E</strong>',
			'Click <strong>Request Programs</strong> below to transfer programs from the microKorg',
			'Alternatively you can send programs from the microKorg by executing a MIDI DATA DUMP',
			'To do that hold down the <em>SHIFT</em> key and press the <em>6</em> key.',
			'Select <strng>Pr9</strong> (all programs) by turning the <em>1/Cutoff</em> knob',
			'Dump the data by pressing the <em>6</em> key'
		],
		params : {
			// singleProgramsCannotBeTransmitted : true,
			deviceId	: 0x30, //= Format ID 3*H 0 to 15 
			banks : {
				bank1 : {label : 'Internal', address : 0x00},
			},
			patchesPerBank : 128,
			transferPause : 600,
			parserConfig : {
				name : {
					from : 0,
					to : 11
				},
				dna : {
					from : 0,
					to : 254
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);
			var header = [0xF0, 0x42, this.params.deviceId, 0x58, 0x40];//Microkorg CURRENT PROGRAM DATA DUMP
			var eox = [0xF7];
			
			midiData = Sysex.packDSISysex(midiData);
			midiData = header.concat(midiData, eox);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			//Send Program Change
			// var prgChange = [0xC0, prgNo & 0x7F]; //1100nnnn 0ppppppp
			// Sysex.sendSysex(prgChange, WebMIDI.midiOut);

			//Send Bulk Data
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);

			// //Send Write Request Microkorg
			// var writeRequest = [0xF0, 0x42, this.params.deviceId, 0x58, 0x11, 0x00, prgNo, 0xF7];
			// Sysex.sendSysex(writeRequest, WebMIDI.midiOut);			
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);
			
			var header = [0xF0, 0x42, this.params.deviceId, 0x58, 0x4C];//Microkorg PROGRAM DATA DUMP
			// var header = [0xF0, 0x42, this.params.deviceId, 0x58, 0x40];//Microkorg CURRENT PROGRAM DATA DUMP
			var eox = [0xF7];
			midiData = Sysex.packDSISysex(midiData);
			midiData = header.concat(midiData, eox);

			var writeRequest = [0xF0, 0x42, this.params.deviceId, 0x58, 0x11, 0x00, prgNo, 0xF7];
			midiData = midiData.concat(writeRequest);

			return midiData;
		},
		retrieveBank : function(bank) {
			//Microkorg PROGRAM DATA DUMP REQUEST
			var midiData = [0xF0, 0x42, this.params.deviceId, 0x58, 0x1C, 0xF7];
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			var program = [];
			var data;
			var dna;

			midiData = Sysex.objectToArray(midiData);

			switch(midiData[4]) {
				//CURRENT PROGRAM DATA DUMP
				case 0x40 : program = this.extractSinglePatch(midiData); break;
				//PROGRAM DATA DUMP
				case 0x4C : program = this.extractMultiDump(midiData); break;
				//ALL DATA DUMP
				case 0x50 : program = this.extractMultiDump(midiData); break;
			}
			return program;
		},
		extractMultiDump : function(midiData) {
			var packedData = midiData.slice(5, 37391);
			var unpackedData = Sysex.unpackDSISysex(packedData);

			var chunk = 254;
			var i,j,data;
			var patchNames = [];

			for (i=0,j=unpackedData.length; i<j; i+=chunk) {
				data = unpackedData.slice(i,i+chunk);
				if(data.length == 254) {
					patchNames.push(Sysex.extractPatchName(data, this.params.parserConfig));
				}
			}
			return patchNames;
		},
		extractSinglePatch : function(midiData) {
			var packedData = midiData.slice(5, 296);
			var unpackedData = Sysex.unpackDSISysex(packedData);

			return [Sysex.extractPatchName(unpackedData, this.params.parserConfig)];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 12; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			return Sysex.renameProgram(newName, nameLength, program.data, this.params.parserConfig);
		}
	};
	return obj;
});
