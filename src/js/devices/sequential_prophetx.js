angular.module('f0f7.devices').factory('SequentialProphetX', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'SequentialProphetX',
		manuf 		: 'Sequential',
		model 		: 'Prophet X [beta]',
		hints 		: [
			'Press the Global button on your X.',
            'Use Soft Knob 1 to select <strong>MIDI sysex cable</strong> then use the Soft Knob 3 to choose either <strong>USB</strong> or <strong>MIDI</strong>, depending on which port you are using to connect to your computer.',
			'You can only transfer programs from bank U1 to bank U4, higher banks are readable only (ROM).',
			'You can only change the name of Layer A of a program.'
		],
		params : {
			banks : {
				bank1 : {label : 'Prophet X / Bank U1', address : 0x00},
				bank2 : {label : 'Prophet X / Bank U2', address : 0x01},
				bank3 : {label : 'Prophet X / Bank U3', address : 0x02},
				bank4 : {label : 'Prophet X / Bank U4', address : 0x03},
			},
			patchesPerBank : 128,
			transferPause : 1000,
			parserConfig : {
				rawData : {
					from: 6,
					to: 4696 //4690 bytes packed, 4096 expanded
				},
				name : {
					from : 418,
					to : 437,
				},
				dna : {
					from : 0,
					to : 4096
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			midiData = Array.from(midiData);
			midiData[3] = 0x03; 	// Edit Buffer Data
			midiData.splice(4, 2);	// Unset Bank and Prg
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[3] = 0x02; 	// Program Data Dump
			midiData[4] = bank;		// 0 - 9
			midiData[5] = prgNo;	// 0 - 99

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x01, 0x30, 0x05];
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			midiData = Sysex.objectToArray(midiData);
			var unpackedData = this.unpackMidiData(midiData);
			var program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			program.data = midiData;
			return [program];
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 20; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = Sysex.createPaddedNameArray(nameLength, newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, this.params.parserConfig.rawData.from);

			var unpackedData = this.unpackMidiData(midiData);
			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);
			program = Sysex.extractPatchName(unpackedData, this.params.parserConfig);
			
			var packedData = Sysex.packDSISysex(unpackedData);
			program.data = header.concat(packedData, 0xF7);
			return program;
		},
		unpackMidiData : function(midiData) {
			var packedData = midiData.slice(this.params.parserConfig.rawData.from, this.params.parserConfig.rawData.to);
			var unpackedData = Sysex.unpackDSISysex(packedData);
			//Sysex.dumpData(unpackedData, 16);
			return unpackedData;
		}
	};
	return obj;
});
