// Single Patch Data Format
// Statistics:     134 Bytes/Single Patch
// = 268 nybbles transmitted + 5 bytes Header + 1 byte Checksum + 1
// byte EOX
// = 275 total transmitted bytes/Single Patch
// 
// 
// * single patch
// 0x01    opcode
// pp      patch number, 0-99.
// xx yy   data, sent nybblewise: 
//         a data byte dd is sent as xx=dd&0x0F; yy=dd>>4
// cc      checksum:
//         the sum of the original data bytes dd, masked with 0x7F.

// Here I don't see why they chop up the data bytes; none exceeds 7 bits.

// * patch data format
// byte    param   bits    description
//  0-7    n/a     6 each  patch name: ASCII clipped to 6 bits.
//  
//  request all                  F0 10 06 04 0 0 F7
// request single               F0 10 06 04 1 FD F7   
// request split patch          F0 10 06 04 2 FD F7
// request master parameters    F0 10 06 04 3 0 F7 

angular.module('f0f7.devices').factory('OberheimMatrix6', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'OberheimMatrix6',
		manuf 		: 'Oberheim',
		model 		: 'Matrix 6 / 6R / 1000',
		hints 		: [
			'Matrix 1000 only transfers 100 programs a time.',
			'The program # you select on the Matrix 1000 defines which block of 100 programs will be transferred',
			'E.g. if you want to transfer program 300 - 399, first select program 300 on the device',
			'You can copy Matrix 6 programs to Matrix 1000 and via versa',
			'LASER Mammoths rename function has no effect on Matrix 1000. Don\'t use it.'
		],
		params : {
			banks : {
				bank1 : {label : 'Matrix Bank', address : 0x00},
			},
			patchesPerBank : 100,
			transferPause : 40,
			parserConfig : {
				name : {
					from : 0,
					to : 8
				},
				dna : {
					from : 5,
					to : 273
				}
			}
		},
		sixBitCharset : [
			'@','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','[','\\',']','^','_',' ','!','"','#','$','%','&','\'','(',')','*','+',',','-','.','/','0','1','2','3','4','5','6','7','8','9',':',';','<','=','>','?'
		],
		sendToBuffer : function(program) {
			// 0DH - Single Patch Data to Edit Buffer
			// F0H 10H 06H 0DH 0 <patch data> <checksum> F7H
			// <data>     = patch data unpacked to two nibbles per byte 
			//              see patch format listing
			// <checksum> = sum of packed (not transmitted) <data> 
			// NOTE: On receipt, this data will be stored into the edit buffer.
			// NOTE: Wait at least ten msec after sending a patch to the Matrix-1000.
			var midiData = Sysex.objectToArray(program.data);

			// Sysex.dumpData(midiData, 16);

			midiData[3] = 0x0D; //Single Patch Data to Edit Buffer
			midiData[4] = 0x00;

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			// 01H-SINGLE PATCH DATA
			// F0H 10H 06H 01H <num> <patch data> <checksum> F7H

			var midiData = Sysex.objectToArray(program.data);

			midiData[3] = 0x01; //Opcode 1: Transmit a Single Patch
			midiData[4] = prgNo; //pp     Patch Number to transmit, 0 P 99 for Single

			return midiData;
		},
		retrieveBank : function(bank) {		
			// Byte     Function

			//     04H     Opcode
			//     xx     Code indicating what to transmit:
			//               0: Transmit all Single Patches, Splits, and Master parameters
			//               1: Transmit a Single Patch
			//               2: Transmit a Split
			//               3: Transmit Master parameters
			//     pp     Patch Number to transmit, 0 P 99 for Single
			// Patches, 0 P 49 for Splits.

			// This byte is ignored for Transmit Master Parameters and Transmit All
			// requests, but but must be      included to pad out the fixed-length message.

			//  Transmit all 
			// var midiData = [0xF0, 0x10, 0x06, 0x04, 0x00, 0x00, 0xF7];
			// Sysex.sendSysex(midiData, WebMIDI.midiOut);

			var header = [0xF0, 0x10, 0x06, 0x04];
			bank = 0x01; // this is a fake bank. the "type" value
			Sysex.requestAllProgramsOfBank(bank, WebMIDI.midiOut, header, this.params.patchesPerBank, this.params.transferPause);
		},
		extractPatchNames : function(midiData) {
			var parserConfig = this.params.parserConfig;

			midiData = Sysex.objectToArray(midiData);
			if(midiData.length == 275) {
				var rawData = this.getRawData(midiData);
				var unpackedData = this.deNibbleData(rawData);

				var nameArray = unpackedData.slice(parserConfig.name.from, parserConfig.name.to);
				var patchName = this.nameArrayToString(nameArray);

				var dna = Sysex.createDNA(midiData, parserConfig.dna.from, parserConfig.dna.to);
				return [{name : patchName, dna : dna, data : midiData}];
			} else {
				return [];
			}
		},
		getRawData : function(midiData) {
			return midiData.slice(5, 273);
		},
		nibbleData : function(midiData) {
			//converts 1 to 2 bytes
			// xx yy   data, sent nybblewise: 
			//         a data byte dd is sent as xx=dd&0x0F; yy=dd>>4
			
			var nibbledData = [];
			for(var i=0; i<midiData.length; i=i+1) {
				var xx = midiData[i] & 0x0F;
				var yy = midiData[i] >> 4;
				nibbledData.push(xx);
				nibbledData.push(yy);
			}

			return nibbledData;
		},
		deNibbleData : function(midiData) {
			var denibbledData = [];
			for(var i=0; i<midiData.length; i=i+2) {
				var xx = midiData[i] & 0x0F;
				var yy = midiData[i+1] & 0x0F;
				var dd = (yy << 4) | xx;
				denibbledData.push(dd);
			}

			return denibbledData;
		},
		getMaxNameLength : function() {
			return 8; 
		},
		nameArrayToString : function(nameArray) {
			var patchName = '';
			for(var i=0; i<nameArray.length; i++) {
			  patchName += this.mapOberheimHexToASCII(nameArray[i]);

			  // if(typeof nameArray[i] !== 'undefined') {
			  // 	nameArray[i] = (nameArray[i] | 0x40);
			  // 	if(nameArray[i] > 95) {
			  // 		nameArray[i] = nameArray[i] & 0x3f;
			  // 	}
			  // 	if(nameArray[i] < 32 || nameArray[i] > 127) {
			  // 		nameArray[i] = 32;
			  // 	}
			  // 	patchName += String.fromCharCode(nameArray[i]);
			  // }
			}

			patchName = patchName.trim();
			// patchName = patchName.replace(/\s+$/,""); //rtrim
			if(patchName.length === 0) {
				patchName = 'no name';
			}

			return patchName;
		},
		mapOberheimHexToASCII : function(hex) {
			if(typeof this.sixBitCharset[hex] !== 'undefined') {
				return this.sixBitCharset[hex];
			} else {
				return ' ';
			}
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			var newNameArray = this.createPaddedArrayFromCharset(newName);

			var midiData = Sysex.objectToArray(program.data);
			var header = midiData.slice(0, 5);

			var rawData = this.getRawData(midiData);
			var unpackedData = this.deNibbleData(rawData);
			// console.log('unpackedData ' , unpackedData);
			unpackedData = Sysex.changeNameinMidiData(newNameArray, nameLength, unpackedData, this.params.parserConfig);

			// console.log('newNameArray ' , newNameArray);

			var packedData = this.nibbleData(unpackedData);

			var checksum = Sysex.calculateOberheimChecksum(unpackedData);
			var eox = [checksum, 0xF7];

			midiData = header.concat(packedData, eox);

			program = this.extractPatchNames(midiData);

			return program[0];
		},
		createPaddedArrayFromCharset : function (str) {
			var nameLength = this.getMaxNameLength();
			var bytes = [];
			var charCode, index;
			
			for (var i = 0; i < nameLength; i++) {
			  if(typeof str[i] === 'undefined') {
			  	index = 32; //index of space
			  } else {
			  	index = this.sixBitCharset.indexOf(str[i].toUpperCase());
			  }
			  bytes.push(index);
			}
		  	return bytes;
		}
	};
	return obj;
});
