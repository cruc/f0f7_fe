// REMOTE PROGRAMMING
// ------------------
// The send request 1 and receive request 1 messages.

// These dump a lot of data across the MIDI, which is the same for both
// messages, except that the data go the other way. The exchanges are:

// Send request
// Computer:       F0 44 00 00 70+channel 10 program
// CZ101/1000:     F0 44 00 00 70+channel 30
// Computer:       70+channel 31
// CZ101/1000:     <tone data> F7
// Computer:       F7

// Receive request
// Computer:       F0 44 00 00 70+channel 20 program
// CZ101/1000:     F0 44 00 00 70+channel 30
// Computer:       <tone data> F7
// Cz101/1000:     F7

// The program byte is the same as that set by the PROGRAM CHANGE
// function, with the addition that you can request the temporary sound
// area as well ( number is 60 ). This is the area that is used if you
// have altered a preset and not saved it into internal memory.

angular.module('f0f7.devices').factory('CasioCz5000', function($log, Sysex, $timeout, WebMIDI) {
	var obj = {
		id 			: 'CasioCz5000',
		manuf 		: 'Casio',
		model 		: 'CZ-5000 [alpha]',
		hints 		: [
			'Set <strong>Midi Channel</strong> to <strong>1</strong>',
			'Programs will be transferred from and to the <strong>Internal Memory Tones</strong> section'
		],
		params : {
			deviceId : 0x70, //70+channel
			banks : {
				bank1 : {label : 'Casio User Memory Bank', address : 0x01},
			},
			patchesPerBank : 32,
			transferPause : 200,
			parserConfig : {
				dna : {
					from : 9,
					to : 264
				}
			}
		},
		sendToBuffer : function(program) {
			
			var midiData = Sysex.objectToArray(program.data);
			// Sysex.dumpData(midiData, 16);
			midiData[4] = this.params.deviceId;
			midiData[5] = 0x20; // Receive request
			midiData[6] = 0x60; //Edit buffer

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			midiData[4] = this.params.deviceId;
			midiData[5] = 0x20; // Receive request
			midiData[6] = prgNo + 0x20; //Prg No, 0x20 is start of internal

			return midiData;
		},
		retrieveBank : function(bank) {
			// Send request
			// Computer:       F0 44 00 00 70+channel 10 program
			// CZ101/1000:     F0 44 00 00 70+channel 30
			// Computer:       70+channel 31
			// CZ101/1000:      F7
			// Computer:       F7
			
			var header = [0xF0, 0x44, 0x00, 0x00, this.params.deviceId, 0x10];
			this.requestAllProgramsOfBank(header, this.params.patchesPerBank, this.params.transferPause);

		},
		requestAllProgramsOfBank : function(header, patchesPerBank, transferPause) {
			var dumpRequest = [];
			var prgNo = 0;

			var delayedDumpRequest = function(prgNo) {
				$log.debug("Requesting internal Prg " + prgNo);

				// Note that the preset tones are given numbers :
				// CZ101/1000

				// 0..0F   Preset sounds 1..16
				// 20..2F  Internal sounds 1..16
				// 40..4F  Cartridge sounds 1..16

				// CZ5000

				// 00..1F  Preset sounds A1,A2,A3....D6,D7,D8
				// 20..3F  Internal sounds A1,A2.....D6,D7,D8

				dumpRequest = header.concat([(prgNo + 0x20), obj.params.deviceId, 0x31, 0xF7]);
				// console.log('dumpRequest ' , dumpRequest);
				Sysex.sendSysex(dumpRequest, WebMIDI.midiOut);

				if(prgNo < (patchesPerBank-1)) {
					prgNo++;
					$timeout(delayedDumpRequest, transferPause, true, prgNo);
				} else {
					$log.info("Bank dumpRequest completed");
				}
			};
			delayedDumpRequest(prgNo);
		},
		extractPatchNames : function(midiData) {
			if(midiData[6] == 0xF7) {
				$log.info("Ignore strange response from Casio");
				return [];
			} else {
				var data = Sysex.objectToArray(midiData);
				if(data.length == 263) {
					$log.info("Request data from Casio is missing a byte");
					data.splice(5,0, 0x60);
					console.log(data);
				}
				var dna = Sysex.createDNA(data, this.params.parserConfig.dna.from, this.params.parserConfig.dna.to);
				return [{name : 'Casio Internal', dna : dna, data : data}];
			}
		},
		getMaxNameLength : function() {
			return 0;
		},
		renameProgram : function(newName, program) {
			return program;
		}
	};
	return obj;
});
