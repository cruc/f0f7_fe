angular.module('f0f7.devices').factory('RolandJd800', function($log, Sysex, WebMIDI) {
	var obj = {
		id 			: 'RolandJd800',
		manuf 		: 'Roland',
		model 		: 'JD 800',
		hints 		: [
			'In <strong>MIDI</strong> menu set <strong>Unit Number</strong> to <strong>17</strong> and <strong>Rx exclusive</strong> to <strong>ON-1</strong>',
			'Alternatively just set <strong>Rx exclusive</strong> to <strong>ON-2</strong>, that should ignore the <strong>Unit Number</strong>',
		],
		params : {
			deviceId	: 0x10, //JD deviceId 0x10 == 17
			banks : {
				bank1 : {label : 'Bank 1', address : 0x00},
			},
			patchesPerBank : 64,
			transferPause : 300,
			bulkQty 	: 0,
			buffer 		: [],
			parserConfig : {
				name : {
					from : 0,
					to : 15
				},
				dna : {
					from : 0,
					to : 384
				}
			}
		},
		sendToBuffer : function(program) {
			var midiData = Sysex.objectToArray(program.data);

			//first part
			var temporaryAddress, checksum, eox, firstPart, secPart;
			var header = [0xF0, 0x41, this.params.deviceId, 0x3d, 0x12];
			temporaryAddress = [0x00, 0x00, 0x00]; //temporary area address
			midiData = Array.from(midiData);

			firstPart = midiData.slice(0,256);
			firstPart = temporaryAddress.concat(firstPart);

			checksum = Sysex.calculateRolandChecksum(firstPart);
			eox = [checksum, 0xF7];

			firstPart = header.concat(firstPart, eox);
			firstPart = Sysex.convertToUint8(firstPart);

			Sysex.sendSysex(firstPart, WebMIDI.midiOut);

			//second part
			temporaryAddress = [0x00, 0x02, 0x00];
			secPart = midiData.slice(256, midiData.length);
			secPart = temporaryAddress.concat(secPart);

			checksum = Sysex.calculateRolandChecksum(secPart);
			eox = [checksum, 0xF7];

			secPart = header.concat(secPart, eox);
			midiData = Sysex.convertToUint8(secPart);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		sendProgramToBank : function(program, bank, prgNo) {
			var midiData = this.reorderProgram(program, bank, prgNo);
			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		reorderProgram : function(program, bank, prgNo) {
			var midiData = Sysex.objectToArray(program.data);

			//first part
			var address, checksum, eox, firstPart, secPart;
			var header = [0xF0, 0x41, this.params.deviceId, 0x3d, 0x12];

			var bytesInTheMiddle = 3*prgNo;
			var msb = (bytesInTheMiddle >> 7) + 0x05;

			address = [msb, bytesInTheMiddle & 0x7F, 0x00]; //address

			midiData = Array.from(midiData);

			firstPart = midiData.slice(0,256);
			firstPart = address.concat(firstPart);

			checksum = Sysex.calculateRolandChecksum(firstPart);
			eox = [checksum, 0xF7];

			firstPart = header.concat(firstPart, eox);

			//second part
			bytesInTheMiddle = (3*prgNo) + 0x02;
			msb = (bytesInTheMiddle >> 7) + 0x05;

			address = [msb, bytesInTheMiddle & 0x7F, 0x00]; //address

			secPart = midiData.slice(256, midiData.length);
			secPart = address.concat(secPart);

			checksum = Sysex.calculateRolandChecksum(secPart);
			eox = [checksum, 0xF7];

			secPart = header.concat(secPart, eox);

			midiData = firstPart.concat(secPart);

			return midiData;
		},
		retrieveBank : function(bank) {
			var header = [0xF0, 0x41, this.params.deviceId, 0x3D, 0x11];
			var address = [0x05, 0x00, 0x00, 0x01, 0x40, 0x00]; //address and weight for Patch Memory Area
			var checksum = Sysex.calculateRolandChecksum(address);

			var eox = [checksum, 0xF7];

			var midiData = header.concat(address, eox);
			midiData = Sysex.convertToUint8(midiData);

			Sysex.sendSysex(midiData, WebMIDI.midiOut);
		},
		extractPatchNames : function(midiData) {
			if(midiData[4] == 0x12 && midiData[5] === 0x00) { //Single Patch
				this.combineRolandSysex(midiData);

				if(this.params.bulkQty == 2) {
					return this.processBulk();
				}
			}
			if(midiData[4] == 0x12 && midiData[5] > 0x04) { //'Patch Memory Area'
				this.combineRolandSysex(midiData);

				if(this.params.bulkQty == 96) {
					return this.processBulk();
				}
			}
			return [];
		},
		combineRolandSysex : function(midiData) {
			this.params.bulkQty++;
			for (var i = 8; // Skip header
				i < midiData.length - 2; // Omit EOF and Checksum
				i++)
			{
				this.params.buffer.push(midiData[i]);
			}
		},
		processBulk : function () {
			var midiData = this.params.buffer;

			var i,j,data;
			var patchNames = [];
			var chunk = 384;

			for (i=0,j=midiData.length; i<j; i+=chunk) {

				data = midiData.slice(i,i+chunk);
				patchNames.push(Sysex.extractPatchName(data, this.params.parserConfig));
			}

			//Reset params
			this.params.bulkQty = 0;
			this.params.buffer = [];

			return patchNames;
		},
		getMaxNameLength : function() {
			return this.params.parserConfig.name.to - this.params.parserConfig.name.from + 1; //return 16; 
		},
		renameProgram : function(newName, program) {
			var nameLength = this.getMaxNameLength();
			return Sysex.renameProgram(newName, nameLength, program.data, this.params.parserConfig);
		}
	};
	return obj;
});
