angular.module('f0f7.proController', [])
.controller('ProController', function ($scope, Flash, $localStorage, Auth, RestService, $timeout, config, $location) {

	$scope.token = $localStorage.token;
	$scope.accounts = config.accounts;

	if($localStorage.token) {
		Auth.getUserData(function(res) {
				$scope.user = res.data;

				Auth.getBtClientToken(function (res) {
					setupBraintree(res);
				}, httpError);

			}, 	httpError);
	}

	function setupBraintree(res) {
		$timeout(function() {
			var clientToken = res.data.clientToken;

			braintree.setup(clientToken, "dropin", {
				container: "dropin-container",
				paypal : {
					singleUse : true,
					amount	: config.accounts.pro.price,
					currency : 'EUR',
					button: {
						type: 'checkout'
					},
				},
				onPaymentMethodReceived: function (obj) {
					console.log('onPaymentMethodReceived');
					sendPaymentMethodNonceToServer(obj);
				},
				onReady: function () {
					console.log('Braintree is ready');
					$timeout(function() {
						$scope.braintreeIsReady = true;
					}, 300);
				}
			});
		}, 100);
	}

	function sendPaymentMethodNonceToServer(obj) {
		Auth.sendPaymentMethodNonceToServer(obj, function(res) {
			console.log('sendPaymentMethodNonceToServer: success');
			if(res.success === true) {
				Auth.getUserData(function() {
					$location.path('/thank_you_for_supporting_us');
				}, httpError);
			} else {
				console.log(res);
				Flash.create('danger', '<strong>Something went wrong.</strong>', 0);
			}
		}, httpError);
	}

	function httpError(res) {
		Flash.create('danger', '<strong>Error:</strong> ' + res.message);
	}

	Flash.clear();

});
