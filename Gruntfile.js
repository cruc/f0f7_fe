/*
npm install PACKAGE --save-dev
 */

module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),
    concat: {
      bower_js: {
        src: [
          "www/bower_components/angular/angular.js",
          "www/bower_components/angular-route/angular-route.js",
          "www/bower_components/angular-drag-and-drop-lists/angular-drag-and-drop-lists.js",
          "src/js/FileSaver.js",
          "www/bower_components/ngstorage/ngStorage.js",
          "www/bower_components/angular-flash-alert/dist/angular-flash.js",
          "www/bower_components/angular-md5/angular-md5.js",
          "www/bower_components/angular-xeditable/dist/js/xeditable.js",
          "www/bower_components/angular-animate/angular-animate.js",
          "www/bower_components/angular-loading-bar/build/loading-bar.js",
          "www/bower_components/angular-hotkeys/build/hotkeys.js",
          "www/bower_components/braintree-web/dist/braintree.js",
          "www/bower_components/angular-sanitize/angular-sanitize.js",
          "www/bower_components/angular-ui-bootstrap/src/position/position.js",
          "www/bower_components/angular-bootstrap-confirm/dist/angular-bootstrap-confirm.js",
        ],
        dest: "src/js/angular_et_al.js"
      },
      js: {
        src: [
          "src/js/app.js",
          "src/js/angular.midi.js",
          "src/js/controllers.js",
          "src/js/pages_controller.js",
          "src/js/pro_controller.js",
          "src/js/users_controller.js",
          "src/js/sysex_service.js",
          "src/js/devices.js",
          "src/js/rest_service.js",
          "src/js/directives.js",
          "src/js/eloquent.js",
          "src/js/devices/generic.js",
          "src/js/devices/access_virus.js",
          "src/js/devices/clavia_nordlead.js",
          "src/js/devices/casio_cz1000.js",
          "src/js/devices/casio_cz5000.js",
          "src/js/devices/dsi_evolver.js",
          "src/js/devices/dsi_mopho.js",
          "src/js/devices/dsi_ob6.js",
          "src/js/devices/dsi_pro2.js",
          "src/js/devices/dsi_pro12.js",
          "src/js/devices/dsi_prophet08.js",
          "src/js/devices/dsi_prophetrev2.js",
          "src/js/devices/ensoniq_esq1.js",
          // "src/js/devices/hypersynth_xenophone.js",
          "src/js/devices/intellijel_plonk.js",
          "src/js/devices/intellijel_rainmaker.js",
          "src/js/devices/korg_dw8000.js",
          "src/js/devices/korg_microkorg.js",
          "src/js/devices/moog_voyager.js",
          "src/js/devices/novation_bassstation2.js",
          "src/js/devices/oberheim_ob8.js",
          "src/js/devices/oberheim_matrix6.js",
          "src/js/devices/roland_alphajuno.js",
          "src/js/devices/roland_jd800.js",
          "src/js/devices/roland_jv80.js",
          "src/js/devices/sequential_prophet6.js",
          "src/js/devices/sequential_prophetx.js",
          "src/js/devices/strymon_timeline.js",
          "src/js/devices/waldorf_microwave.js",
          "src/js/devices/waldorf_blofeld.js",
          "src/js/devices/yamaha_dx7.js",
        ],
        dest: "src/js/f0f7.js"
      },
      css: {
        // src: [
        //   "www/fe/css/bootstrap/bootstrap.css"
        // ],
        // dest: "www/fe/css/all.css"
      }
    },
    ngAnnotate: {
      options: {
        singleQuotes: true,
        // sourceMap : "www/fe/js/f0f7_src.js"
      },
      f0f7: {
        files: {
          "src/js/f0f7.js": ["src/js/f0f7.js"],
        },
      },
    },
    karma: {
      unit: {
        configFile: 'karma.conf.js',
        browsers: ['PhantomJS'],
        singleRun : true,
        autoWatch : false
      }
    },
    uglify: {
      options: {
        mangle: false,
        banner: "/*! <%= pkg.description %> */\n",
      },
      js: {
        options: {
          mangle: true,
          compress : true,
          beautify : false,
          sourceMap : true,
          sourceMapIncludeSources : true
        }, 
        files: {
          "www/fe/js/f0f7.min.js": ["src/js/f0f7.js"],
        }
      },
      bower_js: {
        files: {
          "www/fe/js/angular_et_al.min.js": ["src/js/angular_et_al.js"]
        }
      },
    },
    cssmin: {
      minify: {
        expand: true,
        cwd: "www/fe/css",
        src: ["*.css", "!*.min.css"],
        dest: "www/fe/css",
        ext: ".min.css",
        options: {
          banner: "/*! <%= pkg.description %> */",
          keepSpecialComments: 0,
          report: "min"
        }
      }
    },
    less: {
      bootstrap: {
        files: {
          "www/fe/css/styles.css": "src/less/bootstrap.less"
        }
      }
    },
    jshint: {
      options : {
        reporter: require("jshint-stylish"),
      },
      // src: ["app/f0f7_fe/js/*.js", "!**/node_modules/**"],
      src: ["www/fe/js/all.js"],
    },
    cachebreaker: {
        dist: {
          options: {
               match: [{
                  'styles.min.css' : 'www/fe/css/styles.min.css',
                  'angular_et_al.min.js' : 'www/fe/js/angular_et_al.min.js',
                  'f0f7.min.js' : 'www/fe/js/f0f7.min.js',
               }],
               replacement: 'md5',
           },
            files: {
                src: ['www/fe/index.html']
            }
        }
    },
    watch: {
      options: {
          livereload: true,
          // cwd: "www/fe/css",
      },
      less: {
        options: {
          livereload: true
        },
        files: "src/less/*",
        tasks: ["less:bootstrap", "concat:css", "cssmin:minify"]
      },
      scripts: {
        options: {
          livereload: false
        },
        files: ["src/js/**/*.js", "!**/node_modules/**", "tests/**/*.js"],
        // tasks: ["jshint", "concat:js", "uglify:js"]
        // tasks: ["concat:js", "jshint", "uglify:js"]
        tasks: ["karma", "concat:js", "ngAnnotate", "uglify:js"]
      },
      // css: {
      //   files: ["css/*.css"],
      //   tasks: []
      // }
    }
  });
  grunt.loadNpmTasks("grunt-contrib-cssmin");
  grunt.loadNpmTasks("grunt-contrib-less");
  grunt.loadNpmTasks("grunt-contrib-jshint");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-contrib-concat");
  grunt.loadNpmTasks("grunt-contrib-uglify");
  grunt.loadNpmTasks("grunt-ng-annotate");
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks("grunt-cache-breaker");

  grunt.registerTask("default", "watch");

  grunt.registerTask("builtjs", [
    "concat:bower_js",
    "concat:js",
    "ngAnnotate",
    "uglify:js",
    "uglify:bower_js",
  ]);
  
  grunt.registerTask("dist", [
    "builtjs", //concat and minify js
    "less:bootstrap", //bootstrap less to css
    "concat:css", //merge additional css
    "cssmin:minify", //minify css
    "cachebreaker:dist"
    ]
  );
};