<?php
namespace App\Test\TestCase\Model\Entity;

use App\Model\Entity\Bank;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Entity\Bank Test Case
 */
class BankTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Entity\Bank
     */
    public $Bank;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Bank = new Bank();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Bank);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
