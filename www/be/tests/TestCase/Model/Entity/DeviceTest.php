<?php
namespace App\Test\TestCase\Model\Entity;

use App\Model\Entity\Device;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Entity\Device Test Case
 */
class DeviceTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Entity\Device
     */
    public $Device;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Device = new Device();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Device);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
