<?php
namespace App\Test\TestCase\Model\Entity;

use App\Model\Entity\Program;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Entity\Program Test Case
 */
class ProgramTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Entity\Program
     */
    public $Program;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Program = new Program();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Program);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
