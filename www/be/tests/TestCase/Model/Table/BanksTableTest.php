<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BanksTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BanksTable Test Case
 */
class BanksTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BanksTable
     */
    public $BanksTable;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.banks',
        'app.users',
        'app.programs',
        'app.devices',
        'app.banks_programs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Banks') ? [] : ['className' => 'App\Model\Table\BanksTable'];
        $this->BanksTable = TableRegistry::get('Banks', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BanksTable);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeMarshal method
     *
     * @return void
     */
    public function testBeforeMarshal()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test isOwnedBy method
     *
     * @return void
     */
    public function testIsOwnedBy()
    {
        $bankId = 1;
        $userId = 1;
        $result = $this->BanksTable->isOwnedBy($bankId, $userId);
        $this->assertEquals($result, true);

        $bankId = 1;
        $userId = 2;
        $result = $this->BanksTable->isOwnedBy($bankId, $userId);
        $this->assertEquals($result, false);
    }

    public function testAddBank() {
        $deviceId = 12; //RainMaker
        $bankName = 'Rainman';
        $userId = 1;

        $result = $this->BanksTable->addBank($deviceId, $bankName, $userId);
        $this->assertEquals($result, 2);
    }

    public function testAddPrograms() {
        $deviceId = 12; //RainMaker
        $prgs = [
            [
                'name' => '1-Tap',
                'dna' => '42d995ecfdbe2170c23352cbbd3872d7',
                'data' => '[240,0,2,20,0,0,0,64,4,0,0,2,16,1,2,0,0,0,68,0,0,32,16,0,17,0,0,120,64,4,64,0,0,2,16,1,0,0,0,0,68,0,0,32,0,17,0,0,0,8,64,4,0,0,16,2,16,1,0,0,0,68,0,0,0,32,0,17,0,0,0,8,64,4,0,0,2,16,4,1,0,0,0,68,0,0,64,32,0,17,0,0,8,114,9,52,0,70,64,12,80,7,5,66,32,0,0,0,1,0,96,0,0,0,0,69,73,127,25,127,33,12,52,55,1,44,40,32,15,1,80,57,126,1,0,12,8,2,0,54,75,20,0,27,42,0,0,0,0,0,1,44,6,35,247]'
            ],
            [
                'name' => 'Paul Newman',
                'dna' => '6a43517f296362568d1ec7f6129bb9b1',
                'data' => '[240,0,2,20,0,1,32,64,4,0,0,2,112,1,2,0,0,79,68,0,0,32,16,0,1,0,0,120,64,4,64,0,0,2,16,1,0,0,0,0,68,0,0,32,0,17,0,0,0,8,64,4,0,0,16,2,16,1,0,0,0,68,0,0,0,32,0,17,0,0,0,8,64,4,0,0,2,16,4,1,0,0,0,68,0,0,64,32,0,17,0,0,8,61,9,62,64,70,64,52,88,7,12,66,32,0,112,4,0,0,32,0,0,0,0,69,73,89,18,13,0,80,1,0,6,1,32,64,13,1,80,104,126,1,0,14,8,2,0,16,35,40,0,33,75,16,41,40,33,0,3,127,127,71,247]'
            ],
            [
                'name' => '(empty)',
                'dna' => 'e7630b3a8fa83691624a04e8dc33d856',
                'data' => '[240,0,2,20,0,71,0,64,4,0,0,2,16,1,2,0,0,0,68,0,0,32,0,0,17,0,0,8,64,4,64,0,0,2,16,1,0,0,0,0,68,0,0,32,0,17,0,0,0,8,64,4,0,0,16,2,16,1,0,0,0,68,0,0,0,32,0,17,0,0,0,8,64,4,0,0,2,16,4,1,0,0,0,68,0,0,64,32,0,17,0,0,8,61,9,62,0,70,64,16,87,7,13,66,32,0,112,6,0,2,104,0,42,39,117,1,64,127,23,127,1,37,56,19,13,15,56,49,15,1,48,4,126,5,0,12,8,2,0,70,31,39,0,42,46,51,71,0,0,0,3,127,127,107,247]'
            ],
            [
                'name' => '(empty)',
                'dna' => 'e7630b3a8fa83691624a04e8dc33d856',
                'data' => '[240,0,2,20,0,71,0,64,4,0,0,2,16,1,2,0,0,0,68,0,0,32,0,0,17,0,0,8,64,4,64,0,0,2,16,1,0,0,0,0,68,0,0,32,0,17,0,0,0,8,64,4,0,0,16,2,16,1,0,0,0,68,0,0,0,32,0,17,0,0,0,8,64,4,0,0,2,16,4,1,0,0,0,68,0,0,64,32,0,17,0,0,8,61,9,62,0,70,64,16,87,7,13,66,32,0,112,6,0,2,104,0,42,39,117,1,64,127,23,127,1,37,56,19,13,15,56,49,15,1,48,4,126,5,0,12,8,2,0,70,31,39,0,42,46,51,71,0,0,0,3,127,127,107,247]'
            ],
        ];
        $bankName = 'Rainman';
        $userId = 1;
        $bankId = 2;
        $ProgramsTable = TableRegistry::get('Programs');
        $BanksProgramsTable = TableRegistry::get('BanksPrograms');

        $result = $ProgramsTable->find()->count();
        $this->assertEquals($result, 128);

        $result = $BanksProgramsTable->find()->count();
        $this->assertEquals($result, 128);

        $result = $this->BanksTable->addPrograms($deviceId, $prgs, $bankId, $userId);
        $this->assertEquals($result, true);

        //"Paul Newman" is the only new Program
        $result = $ProgramsTable->find()->count();
        $this->assertEquals($result, 129);

        // $result = $BanksProgramsTable->find()->count();
        // $this->assertEquals($result, 132);
        
        $result = $BanksProgramsTable->find('all')->where(['bank_id' => 2])->toArray();
        // debug($result);
        $this->assertEquals(count($result), 4);
    }
}
