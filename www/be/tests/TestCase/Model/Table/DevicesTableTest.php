<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DevicesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DevicesTable Test Case
 */
class DevicesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DevicesTable
     */
    public $DevicesTable;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.devices',
        'app.banks',
        'app.users',
        'app.programs',
        'app.banks_programs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Devices') ? [] : ['className' => 'App\Model\Table\DevicesTable'];
        $this->DevicesTable = TableRegistry::get('Devices', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DevicesTable);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test findIdbyClassname method
     *
     * @return void
     */
    public function testFindIdbyClassname()
    {
        $result = $this->DevicesTable->findIdbyClassname('IntellijelRainmaker');
        $this->assertEquals($result, 12);

        $result = $this->DevicesTable->findIdbyClassname('IntellijelRainmak');
        $this->assertEquals($result, null);

    }
}
