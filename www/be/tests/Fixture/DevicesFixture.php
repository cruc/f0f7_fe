<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DevicesFixture
 *
 */
class DevicesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'manufacturer' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'model' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'ngclass' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'created' => '2016-05-08 10:36:02',
            'modified' => '2016-05-08 10:38:14',
            'manufacturer' => 'Access',
            'model' => 'Virus',
            'ngclass' => 'AccessVirus'
        ],
        [
            'id' => 2,
            'created' => '2016-05-08 10:37:58',
            'modified' => '2016-05-08 10:37:58',
            'manufacturer' => 'DSI',
            'model' => 'Evolver',
            'ngclass' => 'DSIEvolver'
        ],
        [
            'id' => 3,
            'created' => '2016-05-08 10:39:47',
            'modified' => '2016-05-08 10:39:47',
            'manufacturer' => 'DSI',
            'model' => 'Prophet08',
            'ngclass' => 'DSIProphet08'
        ],
        [
            'id' => 4,
            'created' => '2016-05-08 10:40:21',
            'modified' => '2016-05-08 10:40:21',
            'manufacturer' => 'Moog',
            'model' => 'Voyager',
            'ngclass' => 'MoogVoyager'
        ],
        [
            'id' => 5,
            'created' => '2016-05-08 10:40:47',
            'modified' => '2016-05-08 10:40:47',
            'manufacturer' => 'Roland',
            'model' => 'JD 800',
            'ngclass' => 'RolandJd800'
        ],
        [
            'id' => 6,
            'created' => '2016-05-07 12:46:35',
            'modified' => '2016-05-07 12:46:35',
            'manufacturer' => 'Sequential',
            'model' => 'Prophet6',
            'ngclass' => 'SequentialProphet6'
        ],
        [
            'id' => 7,
            'created' => '2016-05-08 10:41:29',
            'modified' => '2016-05-08 10:41:29',
            'manufacturer' => 'Waldorf',
            'model' => 'Blofeld',
            'ngclass' => 'WaldorfBlofeld'
        ],
        [
            'id' => 8,
            'created' => '2016-05-24 13:46:36',
            'modified' => '2016-05-24 13:46:36',
            'manufacturer' => 'Clavia',
            'model' => 'Nordlead',
            'ngclass' => 'ClaviaNordlead'
        ],
        [
            'id' => 9,
            'created' => '2016-05-26 21:53:12',
            'modified' => '2016-05-26 21:53:12',
            'manufacturer' => 'Novation',
            'model' => 'Bass Station 2',
            'ngclass' => 'NovationBassstation2'
        ],
        [
            'id' => 10,
            'created' => '2016-06-02 21:10:16',
            'modified' => '2016-06-02 21:10:16',
            'manufacturer' => 'Strymon',
            'model' => 'Timeline',
            'ngclass' => 'StrymonTimeline'
        ],
        [
            'id' => 11,
            'created' => '2016-06-05 13:20:25',
            'modified' => '2016-06-05 13:20:25',
            'manufacturer' => 'Casio',
            'model' => 'CZ-1000',
            'ngclass' => 'CasioCz1000'
        ],
        [
            'id' => 12,
            'created' => '2016-07-01 14:15:02',
            'modified' => '2016-07-01 14:15:02',
            'manufacturer' => 'Intellijel',
            'model' => 'Rainmaker',
            'ngclass' => 'IntellijelRainmaker'
        ],
    ];
}
