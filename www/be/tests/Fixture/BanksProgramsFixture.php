<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BanksProgramsFixture
 *
 */
class BanksProgramsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'bank_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'program_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'pos' => ['type' => 'integer', 'length' => 11, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'bank_id' => ['type' => 'index', 'columns' => ['bank_id', 'program_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'bank_id' => 1,
            'program_id' => 1,
            'pos' => 0
        ],
        [
            'id' => 2,
            'bank_id' => 1,
            'program_id' => 2,
            'pos' => 1
        ],
        [
            'id' => 3,
            'bank_id' => 1,
            'program_id' => 3,
            'pos' => 2
        ],
        [
            'id' => 4,
            'bank_id' => 1,
            'program_id' => 4,
            'pos' => 3
        ],
        [
            'id' => 5,
            'bank_id' => 1,
            'program_id' => 5,
            'pos' => 4
        ],
        [
            'id' => 6,
            'bank_id' => 1,
            'program_id' => 6,
            'pos' => 5
        ],
        [
            'id' => 7,
            'bank_id' => 1,
            'program_id' => 7,
            'pos' => 6
        ],
        [
            'id' => 8,
            'bank_id' => 1,
            'program_id' => 8,
            'pos' => 7
        ],
        [
            'id' => 9,
            'bank_id' => 1,
            'program_id' => 9,
            'pos' => 8
        ],
        [
            'id' => 10,
            'bank_id' => 1,
            'program_id' => 10,
            'pos' => 9
        ],
        [
            'id' => 11,
            'bank_id' => 1,
            'program_id' => 11,
            'pos' => 10
        ],
        [
            'id' => 12,
            'bank_id' => 1,
            'program_id' => 12,
            'pos' => 11
        ],
        [
            'id' => 13,
            'bank_id' => 1,
            'program_id' => 13,
            'pos' => 12
        ],
        [
            'id' => 14,
            'bank_id' => 1,
            'program_id' => 14,
            'pos' => 13
        ],
        [
            'id' => 15,
            'bank_id' => 1,
            'program_id' => 15,
            'pos' => 14
        ],
        [
            'id' => 16,
            'bank_id' => 1,
            'program_id' => 16,
            'pos' => 15
        ],
        [
            'id' => 17,
            'bank_id' => 1,
            'program_id' => 17,
            'pos' => 16
        ],
        [
            'id' => 18,
            'bank_id' => 1,
            'program_id' => 18,
            'pos' => 17
        ],
        [
            'id' => 19,
            'bank_id' => 1,
            'program_id' => 19,
            'pos' => 18
        ],
        [
            'id' => 20,
            'bank_id' => 1,
            'program_id' => 20,
            'pos' => 19
        ],
        [
            'id' => 21,
            'bank_id' => 1,
            'program_id' => 21,
            'pos' => 20
        ],
        [
            'id' => 22,
            'bank_id' => 1,
            'program_id' => 22,
            'pos' => 21
        ],
        [
            'id' => 23,
            'bank_id' => 1,
            'program_id' => 23,
            'pos' => 22
        ],
        [
            'id' => 24,
            'bank_id' => 1,
            'program_id' => 24,
            'pos' => 23
        ],
        [
            'id' => 25,
            'bank_id' => 1,
            'program_id' => 25,
            'pos' => 24
        ],
        [
            'id' => 26,
            'bank_id' => 1,
            'program_id' => 26,
            'pos' => 25
        ],
        [
            'id' => 27,
            'bank_id' => 1,
            'program_id' => 27,
            'pos' => 26
        ],
        [
            'id' => 28,
            'bank_id' => 1,
            'program_id' => 28,
            'pos' => 27
        ],
        [
            'id' => 29,
            'bank_id' => 1,
            'program_id' => 29,
            'pos' => 28
        ],
        [
            'id' => 30,
            'bank_id' => 1,
            'program_id' => 30,
            'pos' => 29
        ],
        [
            'id' => 31,
            'bank_id' => 1,
            'program_id' => 31,
            'pos' => 30
        ],
        [
            'id' => 32,
            'bank_id' => 1,
            'program_id' => 32,
            'pos' => 31
        ],
        [
            'id' => 33,
            'bank_id' => 1,
            'program_id' => 33,
            'pos' => 32
        ],
        [
            'id' => 34,
            'bank_id' => 1,
            'program_id' => 34,
            'pos' => 33
        ],
        [
            'id' => 35,
            'bank_id' => 1,
            'program_id' => 35,
            'pos' => 34
        ],
        [
            'id' => 36,
            'bank_id' => 1,
            'program_id' => 36,
            'pos' => 35
        ],
        [
            'id' => 37,
            'bank_id' => 1,
            'program_id' => 37,
            'pos' => 36
        ],
        [
            'id' => 38,
            'bank_id' => 1,
            'program_id' => 38,
            'pos' => 37
        ],
        [
            'id' => 39,
            'bank_id' => 1,
            'program_id' => 39,
            'pos' => 38
        ],
        [
            'id' => 40,
            'bank_id' => 1,
            'program_id' => 40,
            'pos' => 39
        ],
        [
            'id' => 41,
            'bank_id' => 1,
            'program_id' => 41,
            'pos' => 40
        ],
        [
            'id' => 42,
            'bank_id' => 1,
            'program_id' => 42,
            'pos' => 41
        ],
        [
            'id' => 43,
            'bank_id' => 1,
            'program_id' => 43,
            'pos' => 42
        ],
        [
            'id' => 44,
            'bank_id' => 1,
            'program_id' => 44,
            'pos' => 43
        ],
        [
            'id' => 45,
            'bank_id' => 1,
            'program_id' => 45,
            'pos' => 44
        ],
        [
            'id' => 46,
            'bank_id' => 1,
            'program_id' => 46,
            'pos' => 45
        ],
        [
            'id' => 47,
            'bank_id' => 1,
            'program_id' => 47,
            'pos' => 46
        ],
        [
            'id' => 48,
            'bank_id' => 1,
            'program_id' => 48,
            'pos' => 47
        ],
        [
            'id' => 49,
            'bank_id' => 1,
            'program_id' => 49,
            'pos' => 48
        ],
        [
            'id' => 50,
            'bank_id' => 1,
            'program_id' => 50,
            'pos' => 49
        ],
        [
            'id' => 51,
            'bank_id' => 1,
            'program_id' => 51,
            'pos' => 50
        ],
        [
            'id' => 52,
            'bank_id' => 1,
            'program_id' => 52,
            'pos' => 51
        ],
        [
            'id' => 53,
            'bank_id' => 1,
            'program_id' => 53,
            'pos' => 52
        ],
        [
            'id' => 54,
            'bank_id' => 1,
            'program_id' => 54,
            'pos' => 53
        ],
        [
            'id' => 55,
            'bank_id' => 1,
            'program_id' => 55,
            'pos' => 54
        ],
        [
            'id' => 56,
            'bank_id' => 1,
            'program_id' => 56,
            'pos' => 55
        ],
        [
            'id' => 57,
            'bank_id' => 1,
            'program_id' => 57,
            'pos' => 56
        ],
        [
            'id' => 58,
            'bank_id' => 1,
            'program_id' => 58,
            'pos' => 57
        ],
        [
            'id' => 59,
            'bank_id' => 1,
            'program_id' => 59,
            'pos' => 58
        ],
        [
            'id' => 60,
            'bank_id' => 1,
            'program_id' => 60,
            'pos' => 59
        ],
        [
            'id' => 61,
            'bank_id' => 1,
            'program_id' => 61,
            'pos' => 60
        ],
        [
            'id' => 62,
            'bank_id' => 1,
            'program_id' => 62,
            'pos' => 61
        ],
        [
            'id' => 63,
            'bank_id' => 1,
            'program_id' => 63,
            'pos' => 62
        ],
        [
            'id' => 64,
            'bank_id' => 1,
            'program_id' => 64,
            'pos' => 63
        ],
        [
            'id' => 65,
            'bank_id' => 1,
            'program_id' => 65,
            'pos' => 64
        ],
        [
            'id' => 66,
            'bank_id' => 1,
            'program_id' => 66,
            'pos' => 65
        ],
        [
            'id' => 67,
            'bank_id' => 1,
            'program_id' => 67,
            'pos' => 66
        ],
        [
            'id' => 68,
            'bank_id' => 1,
            'program_id' => 68,
            'pos' => 67
        ],
        [
            'id' => 69,
            'bank_id' => 1,
            'program_id' => 69,
            'pos' => 68
        ],
        [
            'id' => 70,
            'bank_id' => 1,
            'program_id' => 70,
            'pos' => 69
        ],
        [
            'id' => 71,
            'bank_id' => 1,
            'program_id' => 71,
            'pos' => 70
        ],
        [
            'id' => 72,
            'bank_id' => 1,
            'program_id' => 72,
            'pos' => 71
        ],
        [
            'id' => 73,
            'bank_id' => 1,
            'program_id' => 73,
            'pos' => 72
        ],
        [
            'id' => 74,
            'bank_id' => 1,
            'program_id' => 74,
            'pos' => 73
        ],
        [
            'id' => 75,
            'bank_id' => 1,
            'program_id' => 75,
            'pos' => 74
        ],
        [
            'id' => 76,
            'bank_id' => 1,
            'program_id' => 76,
            'pos' => 75
        ],
        [
            'id' => 77,
            'bank_id' => 1,
            'program_id' => 77,
            'pos' => 76
        ],
        [
            'id' => 78,
            'bank_id' => 1,
            'program_id' => 78,
            'pos' => 77
        ],
        [
            'id' => 79,
            'bank_id' => 1,
            'program_id' => 79,
            'pos' => 78
        ],
        [
            'id' => 80,
            'bank_id' => 1,
            'program_id' => 80,
            'pos' => 79
        ],
        [
            'id' => 81,
            'bank_id' => 1,
            'program_id' => 81,
            'pos' => 80
        ],
        [
            'id' => 82,
            'bank_id' => 1,
            'program_id' => 82,
            'pos' => 81
        ],
        [
            'id' => 83,
            'bank_id' => 1,
            'program_id' => 83,
            'pos' => 82
        ],
        [
            'id' => 84,
            'bank_id' => 1,
            'program_id' => 84,
            'pos' => 83
        ],
        [
            'id' => 85,
            'bank_id' => 1,
            'program_id' => 85,
            'pos' => 84
        ],
        [
            'id' => 86,
            'bank_id' => 1,
            'program_id' => 86,
            'pos' => 85
        ],
        [
            'id' => 87,
            'bank_id' => 1,
            'program_id' => 87,
            'pos' => 86
        ],
        [
            'id' => 88,
            'bank_id' => 1,
            'program_id' => 88,
            'pos' => 87
        ],
        [
            'id' => 89,
            'bank_id' => 1,
            'program_id' => 89,
            'pos' => 88
        ],
        [
            'id' => 90,
            'bank_id' => 1,
            'program_id' => 90,
            'pos' => 89
        ],
        [
            'id' => 91,
            'bank_id' => 1,
            'program_id' => 91,
            'pos' => 90
        ],
        [
            'id' => 92,
            'bank_id' => 1,
            'program_id' => 92,
            'pos' => 91
        ],
        [
            'id' => 93,
            'bank_id' => 1,
            'program_id' => 93,
            'pos' => 92
        ],
        [
            'id' => 94,
            'bank_id' => 1,
            'program_id' => 94,
            'pos' => 93
        ],
        [
            'id' => 95,
            'bank_id' => 1,
            'program_id' => 95,
            'pos' => 94
        ],
        [
            'id' => 96,
            'bank_id' => 1,
            'program_id' => 96,
            'pos' => 95
        ],
        [
            'id' => 97,
            'bank_id' => 1,
            'program_id' => 97,
            'pos' => 96
        ],
        [
            'id' => 98,
            'bank_id' => 1,
            'program_id' => 98,
            'pos' => 97
        ],
        [
            'id' => 99,
            'bank_id' => 1,
            'program_id' => 99,
            'pos' => 98
        ],
        [
            'id' => 100,
            'bank_id' => 1,
            'program_id' => 100,
            'pos' => 99
        ],
        [
            'id' => 101,
            'bank_id' => 1,
            'program_id' => 101,
            'pos' => 100
        ],
        [
            'id' => 102,
            'bank_id' => 1,
            'program_id' => 102,
            'pos' => 101
        ],
        [
            'id' => 103,
            'bank_id' => 1,
            'program_id' => 103,
            'pos' => 102
        ],
        [
            'id' => 104,
            'bank_id' => 1,
            'program_id' => 104,
            'pos' => 103
        ],
        [
            'id' => 105,
            'bank_id' => 1,
            'program_id' => 105,
            'pos' => 104
        ],
        [
            'id' => 106,
            'bank_id' => 1,
            'program_id' => 106,
            'pos' => 105
        ],
        [
            'id' => 107,
            'bank_id' => 1,
            'program_id' => 107,
            'pos' => 106
        ],
        [
            'id' => 108,
            'bank_id' => 1,
            'program_id' => 108,
            'pos' => 107
        ],
        [
            'id' => 109,
            'bank_id' => 1,
            'program_id' => 109,
            'pos' => 108
        ],
        [
            'id' => 110,
            'bank_id' => 1,
            'program_id' => 110,
            'pos' => 109
        ],
        [
            'id' => 111,
            'bank_id' => 1,
            'program_id' => 111,
            'pos' => 110
        ],
        [
            'id' => 112,
            'bank_id' => 1,
            'program_id' => 112,
            'pos' => 111
        ],
        [
            'id' => 113,
            'bank_id' => 1,
            'program_id' => 113,
            'pos' => 112
        ],
        [
            'id' => 114,
            'bank_id' => 1,
            'program_id' => 114,
            'pos' => 113
        ],
        [
            'id' => 115,
            'bank_id' => 1,
            'program_id' => 115,
            'pos' => 114
        ],
        [
            'id' => 116,
            'bank_id' => 1,
            'program_id' => 116,
            'pos' => 115
        ],
        [
            'id' => 117,
            'bank_id' => 1,
            'program_id' => 117,
            'pos' => 116
        ],
        [
            'id' => 118,
            'bank_id' => 1,
            'program_id' => 118,
            'pos' => 117
        ],
        [
            'id' => 119,
            'bank_id' => 1,
            'program_id' => 119,
            'pos' => 118
        ],
        [
            'id' => 120,
            'bank_id' => 1,
            'program_id' => 120,
            'pos' => 119
        ],
        [
            'id' => 121,
            'bank_id' => 1,
            'program_id' => 121,
            'pos' => 120
        ],
        [
            'id' => 122,
            'bank_id' => 1,
            'program_id' => 122,
            'pos' => 121
        ],
        [
            'id' => 123,
            'bank_id' => 1,
            'program_id' => 123,
            'pos' => 122
        ],
        [
            'id' => 124,
            'bank_id' => 1,
            'program_id' => 124,
            'pos' => 123
        ],
        [
            'id' => 125,
            'bank_id' => 1,
            'program_id' => 125,
            'pos' => 124
        ],
        [
            'id' => 126,
            'bank_id' => 1,
            'program_id' => 126,
            'pos' => 125
        ],
        [
            'id' => 127,
            'bank_id' => 1,
            'program_id' => 127,
            'pos' => 126
        ],
        [
            'id' => 128,
            'bank_id' => 1,
            'program_id' => 128,
            'pos' => 127
        ],
    ];
}
