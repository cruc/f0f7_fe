<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Banks Program'), ['action' => 'edit', $banksProgram->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Banks Program'), ['action' => 'delete', $banksProgram->id], ['confirm' => __('Are you sure you want to delete # {0}?', $banksProgram->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Banks Programs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Banks Program'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Programs'), ['controller' => 'Programs', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Program'), ['controller' => 'Programs', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Banks'), ['controller' => 'Banks', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Bank'), ['controller' => 'Banks', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="banksPrograms view large-9 medium-8 columns content">
    <h3><?= h($banksProgram->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Bank') ?></th>
            <td><?= $banksProgram->has('bank') ? $this->Html->link($banksProgram->bank->name, ['controller' => 'Banks', 'action' => 'view', $banksProgram->bank->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Program') ?></th>
            <td><?= $banksProgram->has('program') ? $this->Html->link($banksProgram->program->name, ['controller' => 'Programs', 'action' => 'view', $banksProgram->program->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($banksProgram->id) ?></td>
        </tr>
    </table>
</div>
