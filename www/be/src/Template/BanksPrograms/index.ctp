<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Banks Program'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Programs'), ['controller' => 'Programs', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Program'), ['controller' => 'Programs', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Banks'), ['controller' => 'Banks', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Bank'), ['controller' => 'Banks', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="banksPrograms index large-9 medium-8 columns content">
    <h3><?= __('Banks Programs') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('bank_id') ?></th>
                <th><?= $this->Paginator->sort('program_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($banksPrograms as $banksProgram): ?>
            <tr>
                <td><?= $this->Number->format($banksProgram->id) ?></td>
                <td><?= $banksProgram->has('bank') ? $this->Html->link($banksProgram->bank->name, ['controller' => 'Banks', 'action' => 'view', $banksProgram->bank->id]) : '' ?></td>
                <td><?= $banksProgram->has('program') ? $this->Html->link($banksProgram->program->name, ['controller' => 'Programs', 'action' => 'view', $banksProgram->program->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $banksProgram->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $banksProgram->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $banksProgram->id], ['confirm' => __('Are you sure you want to delete # {0}?', $banksProgram->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
