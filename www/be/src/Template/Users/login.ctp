<div class="container">
	<div class="medium-offset-3 medium-6 columns content">
		<div class="users form">
			<?= $this->Flash->render('auth') ?>
			<?= $this->Form->create() ?>
			<fieldset>
				<legend><?= __('I am legend') ?></legend>
				<?= $this->Form->input('username') ?>
				<?= $this->Form->input('password') ?>
			</fieldset>
			<?= $this->Form->button(__('Login')); ?>
			<?= $this->Form->end() ?>
		</div>
	</div>
</div>