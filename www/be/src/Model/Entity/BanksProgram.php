<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BanksProgram Entity
 *
 * @property int $id
 * @property int $bank_id
 * @property int $program_id
 * @property int $pos
 *
 * @property \App\Model\Entity\Bank $bank
 * @property \App\Model\Entity\Program $program
 */
class BanksProgram extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
