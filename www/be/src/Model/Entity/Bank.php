<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Bank Entity.
 *
 * @property int $id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property string $name
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property int $device_id
 * @property \App\Model\Entity\Device $device
 * @property bool $is_public
 * @property \App\Model\Entity\Program[] $programs
 */
class Bank extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        // 'id' => false,
        // 'name' => true,
        // 'description' => true,
        // 'user_id' => true,
        // 'device_id' => true,
        // 'is_public' => true,
    ];
}
