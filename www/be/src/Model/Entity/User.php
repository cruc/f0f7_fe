<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity.
 */
class User extends Entity
{

    protected $_hidden = ['password', 'master'];
    protected $_virtual = ['is_valid_pro'];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'username' => true,
        'email' => true,
        'password' => true,
        'active' => true,
    ];

    protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }

    protected function _getIsValidPro()
    {
        $isValidProUser = false;
        if(isset($this->_properties['prodate']) && !is_null($this->_properties['prodate'])) {
            $proAccountValidUntil = date('Y-m-d', strtotime('+1 year ' . $this->_properties['prodate']));
            if(date('Y-m-d') <=  $proAccountValidUntil) {
                $isValidProUser = true;
            }
        }
        return $isValidProUser;
    }
}