<?php
namespace App\Model\Table;

use App\Model\Entity\Bank;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

//Marshall
use Cake\Event\Event;
use ArrayObject;

/**
 * Banks Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Devices
 * @property \Cake\ORM\Association\BelongsToMany $Programs
 */
class BanksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('banks');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Devices', [
            'foreignKey' => 'device_id'
        ]);
        $this->belongsToMany('Programs', [
            'foreignKey' => 'bank_id',
            'targetForeignKey' => 'program_id',
            'joinTable' => 'banks_programs',
            'through' => 'BanksPrograms',
        ]);
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
       $sanitizedFields = array('name', 'description');
       foreach($sanitizedFields as $field) {
           if(isset($data[$field])) {
               $data[$field] = strip_tags($data[$field]);
           }
       }
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->add('name', [
                'minLength' => [
                    'rule' => ['minLength', 3],
                    'last' => true,
                    'message' => 'Names need to be at least 3 characters long',
                ],
                'maxLength' => [
                    'rule' => ['maxLength', 50],
                    'message' => 'Names cannot be too long.'
                ]
            ])
            ->notEmpty('name', 'A name is required');

        $validator
            ->boolean('is_public')
            ->requirePresence('is_public', 'create')
            ->notEmpty('is_public');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['device_id'], 'Devices'));
        return $rules;
    }

    public function isOwnedBy($bankId, $userId)
    {
        return $this->exists(['id' => $bankId, 'user_id' => $userId]);
    }

    public function addBank($deviceId, $bankName, $userId)
    {
        $bank = $this->newEntity();
        $bank->name = $bankName;
        $bank->user_id = $userId;
        $bank->device_id = $deviceId;
        $bank->is_public = 0;

        if($result = $this->save($bank)) {
            $bankId = $result->id;
        } else {
            $bankId = false;
        }
        return $bankId;
    }

    public function addPrograms($deviceId, $prgs, $bankId, $userId)
    {
        $success = true;
        $programs = [];

        foreach ($prgs as $keyPrg => $program) {

            //Check if DNA exists
            $existingProgram = $this->Programs
                ->find()
                ->select('id')
                ->where([
                    'dna' => $program['dna'],
                    'device_id' => $deviceId
                ])
                ->first();

            if(isset($existingProgram->id)) {
                $programs[$keyPrg]['program_id'] = $existingProgram->id;
            } else {

                $programsTable = $this->Programs;
                $prg = $programsTable->newEntity();

                $prg->name = $program['name'];
                $prg->dna = $program['dna'];
                $prg->device_id = $deviceId;
                $prg->user_id = $userId;
                $prg->data = json_encode($program['data']);

                if ($programsTable->save($prg)) {
                    $programs[$keyPrg]['program_id'] = $prg->id;
                }

            }
            $programs[$keyPrg]['bank_id'] = $bankId;
            $programs[$keyPrg]['pos'] = $keyPrg;
        }
        
        $banksProgramsTable = $this->BanksPrograms;
        $entities = $banksProgramsTable->newEntities($programs);

        foreach ($entities as $entity) {
            if(!$banksProgramsTable->save($entity)) {
                if($success === true) {
                    $success = false;
                }
            }
        }
        return $success;
    }

    // public function addBankAndPrograms($deviceId, $prgs, $bankName, $userId)
    // {
    //     $programs = [];

    //     foreach ($prgs as $keyPrg => $program) {

    //         //Check if DNA exists
    //         $existingProgram = $this->Programs
    //             ->find()
    //             ->select('id')
    //             ->where([
    //                 'dna' => $program['dna'],
    //                 'device_id' => $deviceId
    //             ])
    //             ->first();

    //         if(isset($existingProgram->id)) {
    //             $programs[$keyPrg]['id'] = $existingProgram->id;
    //         } else {
    //             $programs[$keyPrg]['name'] = $program['name'];
    //             $programs[$keyPrg]['dna'] = $program['dna'];
    //             $programs[$keyPrg]['device_id'] = $deviceId;
    //             $programs[$keyPrg]['user_id'] = $userId;
    //             $programs[$keyPrg]['data'] = json_encode($program['data']);
    //         }
    //         $programs[$keyPrg]['_joinData']['pos'] = $keyPrg;
    //     }

    //     $data = [
    //         'name' => $bankName,
    //         'user_id' => $userId,
    //         'device_id' => $deviceId,
    //         'is_public' => 0,
    //         'programs' => $programs
    //     ];
    //     $entity = $this->newEntity($data);

    //     return $this->save($entity, ['associated' => ['Programs']]);
    // }
}
