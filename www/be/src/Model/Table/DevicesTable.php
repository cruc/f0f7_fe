<?php
namespace App\Model\Table;

use App\Model\Entity\Device;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Devices Model
 *
 * @property \Cake\ORM\Association\HasMany $Banks
 * @property \Cake\ORM\Association\HasMany $Programs
 */
class DevicesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('devices');
        $this->displayField('model');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Banks', [
            'foreignKey' => 'device_id'
        ]);
        $this->hasMany('Programs', [
            'foreignKey' => 'device_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('model');

        $validator
            ->allowEmpty('manufacturer');

        return $validator;
    }

    public function findIdbyClassname($ngClass) {
       $query = $this->find('all', [
           'conditions' => ['Devices.ngclass' => $ngClass],
       ]);
       $row = $query->first();
       return $row['id'];
    }
}
