<?php
namespace App\Controller\Api;

use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use Cake\Event\Event;
use Cake\Log\Log;
use Cake\Mailer\Email;
use Cake\Network\Exception\UnauthorizedException;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

use Braintree_Configuration;
use Braintree_ClientToken;
use Braintree_Transaction;

class BraintreeController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $bt = Configure::read('Braintree');
        Braintree_Configuration::environment($bt['environment']);
        Braintree_Configuration::merchantId($bt['merchantId']); 
        Braintree_Configuration::publicKey($bt['publicKey']);  
        Braintree_Configuration::privateKey($bt['privateKey']); 
    }

    public function get_client_token()
    {
        $data['clientToken'] = Braintree_ClientToken::generate();
        $this->set([
            'data' => $data,
            '_serialize' => ['data']
            ]);
    }

    public function payment_method_nonce_received()
    {
        $success = false;
        if(isset($this->request->data["nonce"])) {
            Log::notice($this->request->data, ['scope' => ['payments']]);
            $nonceFromTheClient = $this->request->data["nonce"];
            $amount = Configure::read('Account.pro.price');
            $result = Braintree_Transaction::sale([
                'amount' => $amount,
                'paymentMethodNonce' => $nonceFromTheClient,
                'options' => [
                    'submitForSettlement' => true
                ]
            ]);

            if($result->success === true) {
                if($this->_updateUsersProdate($this->Auth->user('id'))) {
                    $success = true;
                    $email = new Email('admin');
                    $email
                        ->subject('LASER Mammoth: LASERS upgraded!')
                        ->send('Achtung, ' . $this->Auth->user('username') . ' has upgraded his LASERS!');
                }
            } else {
                Log::error('Received Nonce but there was a problem with the transaction.', ['scope' => ['payments']]);
            }

            // $data['result'] = $result;
            // $data['request'] = $this->request->data;
            // $data['nonce'] = $nonceFromTheClient;
            // $data['user'] = $this->Auth->user();
            
            $data = $result;
            
        } else {
            Log::error('Missing nonce', ['scope' => ['payments']]);
        }

        $this->set([
            'data' => $data,
            'success' => $success,
            '_serialize' => ['success', 'data']
            ]);
    }

    protected function _updateUsersProdate($userid)
    {
        $usersTable = TableRegistry::get('Users');
        $user = $usersTable->get($userid);
        $data['prodate'] = date('Y-m-d H:i:s');
        $usersTable->patchEntity($user, $data, [
            'fieldList' => ['prodate']
        ]);
        return $usersTable->save($user);
    }

}