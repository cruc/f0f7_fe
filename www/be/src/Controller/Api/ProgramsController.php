<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;

class ProgramsController extends AppController
{

	public $paginate = [
		'page' => 1,
		'limit' => 5,
		'maxLimit' => 15,
		'sortWhitelist' => [
			'id', 'name'
		]
	];

	public function initialize()
	{
	    parent::initialize();
	    $this->Auth->config('authorize', ['Controller']);
	    $this->Auth->allow(['index', 'view']);
	}

	public function isAuthorized($user = null)
	{
		if (in_array($this->request->action, ['edit', 'delete'])) {
			if (isset($this->request->params['pass'][0])) {
				$programId = (int)$this->request->params['pass'][0];
				if ($this->Programs->isOwnedBy($programId, $user['id'])) {
					return true;
				}
			}
			return false;
		}
		return true;
	}
}