<?php
namespace App\Controller\Api;

use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use Cake\Mailer\Email;

use Cake\Auth\DefaultPasswordHasher;

use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;

class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->config('authorize', ['Controller']);
        $this->Auth->allow(['add', 'token', 'password_request', 'password_reset', 'optin']);
    }

    public function isAuthorized($user = null)
    {
        if (in_array($this->request->action, ['edit', 'view', 'delete'])) {
            if (isset($this->request->params['pass'][0])) {
                $userId = (int)$this->request->params['pass'][0];
                if ($userId === $user['id']) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    /**
     * Create new user
     */
    public function add()
    {
        $data = $this->request->data();
        
        $data['master'] = 0;
        $data['active'] = 0;
        $data['prodate'] = null;

        $user = $this->Users->newEntity($data);
        if($this->Users->save($user)) {
            $success = true;
            $results['message'] = 'We have send you an email with a link to confirm your registration.';

            $hash = (new DefaultPasswordHasher)->hash($user->created);

            $email = new Email('default');
            $email
                ->to($user->email)
                ->subject('LASER Mammoth: confirm your account')
                ->send('Hey ' . $user->username . ', welcome on board.' . "\n"
                    . 'Click on the link to confirm your account.' . "\n\n"
                    . Configure::read('AngularJS.baseUrl') . '/optin?id='. $user->id . '&hash=' .  rawurlencode($hash) . "\n\n"
                    . 'LASER Mammoth will attack with LASERS!');

        } else {
            $success = false;
            $results['message'] = 'A validation problem occurred';
            $results['errors'] = $user->errors();
        }

        $this->set([
            'data' => $results,
            'success' => $success,
            '_serialize' => ['success', 'data']
            ]);
    }

    /**
     * Return JWT token if posted user credentials pass FormAuthenticate
     */
    public function token()
    {
        $user = $this->Auth->identify();

        if($user)
        {
            // Checks if user is activated
            $userData = $this->Users->get($user['id']);

            if((int)$userData->active == 0) {
                throw new UnauthorizedException('Account not verified. Check the email we have sent you on registration.');
            }

            $this->set([
                'success' => true,
                'data' => [
                    'token' => JWT::encode(
                        [
                            'sub' => $user['id'],
                            'exp' =>  time() + 7776000 //90days
                        ],
                        Security::salt()
                    )
                ],
                '_serialize' => ['success', 'data']
            ]);
        } else {
            throw new UnauthorizedException('Invalid username or password');
        }
    }

    public function password_request()
    {
        $query = $this->Users->find('all', [
            'conditions' => ['Users.username' => $this->request->data['username']]
        ]);

        if($query->isEmpty()) {
            $success = false;
            $results['message'] = 'There is no registered user with this username.';
        } else {
            $user = $query->first();

            $hash = (new DefaultPasswordHasher)->hash($user->created);

            $email = new Email('default');
            $email
                ->to($user->email)
                ->subject('LASER Mammoth: password request')
                ->send('Hey ' . $user->username . ', somebody has requested a new password for your LASER Mammoth account.' . "\n"
                    . 'Click on the link to reset your password.' . "\n\n"
                    . Configure::read('AngularJS.baseUrl') . '/set_a_new_password?id='. $user->id . '&hash=' .  rawurlencode($hash) . "\n\n"
                    . 'LASER Mammoth will attack with LASERS!');


            $success = true;
            $results['message'] = 'We have send you an email with instructions how to reset your password.';
        }

        $this->set([
            'data' => $results,
            'success' => $success,
            '_serialize' => ['success', 'data']
            ]);
    }

    public function password_reset()
    {
        $data = $this->request->data();
        $userId = $data['id'];
        $user = $this->Users->get($userId);

        $data['active'] = 1; //password change == optin

        $hash = rawurldecode($data['hash']);

        if ((new DefaultPasswordHasher)->check($user->created, $hash)) {
            $this->Users->patchEntity($user, $data, [
                'fieldList' => ['password', 'active']
            ]);

            if($this->Users->save($user)) {
                $success = true;
                $results['message'] = 'Try out your shiny new password!';
            } else {
                $success = false;
                $results['errors'] = $user->errors();
            }

        } else {
            $success = false;
            $results['message'] = 'Something wrong with the link? This will never work.';
        }

        $this->set([
            'data' => $results,
            'success' => $success,
            '_serialize' => ['success', 'data']
            ]);

    }

    public function optin()
    {
        $userId = $this->request->data['id'];
        $user = $this->Users->get($userId);
        $hash = rawurldecode($this->request->data['hash']);

        if ((new DefaultPasswordHasher)->check($user->created, $hash)) {
            $data['active'] = 1;
            $this->Users->patchEntity($user, $data);

            if($this->Users->save($user)) {
                $success = true;
                $results = [
                    'id' => $user->id,
                    'token' => JWT::encode(
                        [
                        'sub' => $user->id,
                        'exp' =>  time() + 604800
                        ],
                        Security::salt()
                    )
                ];
                $results['message'] = 'That went through, welcome!';
            } else {
                $success = false;
                $results['errors'] = $user->errors();
                $results['message'] = 'Could not update user. Something with validation?';
            }

        } else {
            $success = false;
            $results['message'] = 'Something wrong with the link? This will never work.';
        }

        $this->set([
            'data' => $results,
            'success' => $success,
            '_serialize' => ['success', 'data']
            ]);

    }
}