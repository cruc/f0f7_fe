<?php
namespace App\Controller\Api;

use App\Controller\Api\AppController;
use Cake\ORM\TableRegistry;

class BanksController extends AppController
{

	public $paginate = [
		'page' => 1,
		'limit' => 5,
		'maxLimit' => 15,
		'sortWhitelist' => [
			'id', 'created', 'name'
		],
		'order' => ['created' => 'DESC']
	];

	public function initialize()
	{
		parent::initialize();
		$this->Auth->config('authorize', ['Controller']);
		$this->Auth->allow(['public_index']);
	}

	public function beforeFilter(\Cake\Event\Event $event)
	{
		parent::beforeFilter($event);
		$this->Crud->mapAction('public_index', 'Crud.Index');
	}

	public function isAuthorized($user = null)
	{
		if (in_array($this->request->action, ['edit', 'delete', 'update_programs'])) {
			if (isset($this->request->params['pass'][0])) {
				$bankId = (int)$this->request->params['pass'][0];
				if ($this->Banks->isOwnedBy($bankId, $user['id'])) {
					return true;
				}
			}
			return false;
		}
		return true;
	}

	public function index()
	{
		$this->Crud->on('beforePaginate', function(\Cake\Event\Event $event) {
			if (isset($this->request->query['ngclass'])) {
				$this->paginate['conditions']['device_id'] = $this->Banks->Devices->findIdbyClassname($this->request->query['ngclass']);
			}
			$this->paginate['conditions']['user_id'] = $this->Auth->user('id');
		});
		return $this->Crud->execute();
	}

	public function public_index()
	{
		$this->Crud->on('beforePaginate', function(\Cake\Event\Event $event) {
			if (isset($this->request->query['ngclass'])) {
				$this->paginate['conditions']['device_id'] = $this->Banks->Devices->findIdbyClassname($this->request->query['ngclass']);
			}
			$this->paginate['conditions']['is_public'] = 1;
			$event->subject->query->contain(['Users' => function($q) {
				return $q->select(['id', 'username', 'prodate']);
			}]);
		});
		return $this->Crud->execute();
	}

	public function add()
	{	
		$deviceId = $this->Banks->Devices->findIdbyClassname($this->request->data['bank']['ngclass']);
		$bankId =  $this->Banks->addBank($deviceId, $this->request->data['bank']['name'], $this->Auth->user('id'));
		if($bankId) {
			if($this->Banks->addPrograms(
					$deviceId, 
					$this->request->data['programs'],
					$bankId,
					$this->Auth->user('id')
				)) {

				$data = ['id' => $bankId];
				$success = true;
				$this->response->statusCode(201);
			} else {
				$data = [];
				$success = false;
			}
		} else {
			$data = [];
			$success = false;
		}

		$this->set([
			'data' => $data,
			'success' => $success,
			'_serialize' => ['success', 'data']
			]);
	}

	public function update_programs()
	{
		$deviceId = $this->Banks->Devices->findIdbyClassname($this->request->data['bank']['ngclass']);
		$bankId = $this->request->data['bank']['id'];

		$BanksPrograms = TableRegistry::get('BanksPrograms');
		if($BanksPrograms->deleteAll(['bank_id' => $bankId])) {
			if($this->Banks->addPrograms(
					$deviceId,
					$this->request->data['programs'],
					$bankId,
					$this->Auth->user('id')
				)) {

				$success = true;
			} else {
				$success = false;
			}
		} else {
			$success = false;
		}

		$this->set([
			'success' => $success,
			'_serialize' => ['success', 'data']
			]);
	}

	public function view($id)
	{
		$query = $this->Banks->find('all', [
		    'conditions' => ['Banks.id' => $id]
		]);

		$query->contain(['Programs'  => [
			'sort' => ['pos' => 'ASC']
		]]);

		$data = $query->first();

		$this->set([
			'data' => $data,
			'success' => true,
			'_serialize' => ['success', 'data']
			]);

	}
}