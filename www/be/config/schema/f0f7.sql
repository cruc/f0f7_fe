# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.62-0+deb8u1)
# Database: f0f7
# Generation Time: 2020-02-05 14:57:43 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table banks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `banks`;

CREATE TABLE `banks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  `user_id` int(11) unsigned NOT NULL,
  `device_id` int(11) unsigned NOT NULL,
  `is_public` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table banks_programs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `banks_programs`;

CREATE TABLE `banks_programs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `bank_id` int(11) unsigned DEFAULT NULL,
  `program_id` int(11) unsigned DEFAULT NULL,
  `pos` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bank_id` (`bank_id`,`program_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table devices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `devices`;

CREATE TABLE `devices` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `manufacturer` varchar(255) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `ngclass` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;

INSERT INTO `devices` (`id`, `created`, `modified`, `manufacturer`, `model`, `ngclass`)
VALUES
    (1,'2016-05-08 10:36:02','2016-05-08 10:38:14','Access','Virus','AccessVirus'),
    (2,'2016-05-08 10:37:58','2016-05-08 10:37:58','DSI','Evolver','DSIEvolver'),
    (3,'2016-05-08 10:39:47','2016-05-08 10:39:47','DSI','Prophet08','DSIProphet08'),
    (4,'2016-05-08 10:40:21','2016-05-08 10:40:21','Moog','Voyager','MoogVoyager'),
    (5,'2016-05-08 10:40:47','2016-05-08 10:40:47','Roland','JD 800','RolandJd800'),
    (6,'2016-05-07 12:46:35','2016-05-07 12:46:35','Sequential','Prophet6','SequentialProphet6'),
    (7,'2016-05-08 10:41:29','2016-05-08 10:41:29','Waldorf','Blofeld','WaldorfBlofeld'),
    (8,'2016-05-24 13:46:36','2016-05-24 13:46:36','Clavia','Nordlead','ClaviaNordlead'),
    (9,'2016-05-26 21:53:12','2016-05-26 21:53:12','Novation','Bass Station 2','NovationBassstation2'),
    (10,'2016-06-03 10:25:33','2016-06-03 10:25:33','Strymon','TimeLine','StrymonTimeline'),
    (11,'2016-06-06 14:41:12','2016-06-06 14:41:12','Casio','CZ-1000','CasioCz1000'),
    (12,'2016-06-27 13:24:34','2016-06-27 13:24:34','Intellijel','Rainmaker','IntellijelRainmaker'),
    (13,'2016-08-03 14:15:49','2016-08-03 14:15:49','Roland','JV-80','RolandJv80'),
    (14,'2016-08-06 16:42:12','2016-08-06 16:42:12','Oberheim','Matrix 6','OberheimMatrix6'),
    (15,'2016-08-08 15:39:50','2016-08-08 15:39:50','Yamaha','DX7','YamahaDx7'),
    (16,'2016-08-08 20:58:50','2016-08-08 20:58:50','DSI','OB-6','DSIOb6'),
    (17,'2016-08-10 17:34:33','2016-08-10 17:34:33','Korg','DW-8000','KorgDw8000'),
    (18,'2016-10-17 15:45:46','2016-10-17 15:45:46','DSI','Mopho','DSIMopho'),
    (19,'2016-10-20 11:29:44','2016-10-20 11:29:44','Korg','microKORG','KorgMicrokorg'),
    (20,'2016-12-12 16:31:45','2016-12-12 16:31:45','DSI','Pro 2','DSIPro2'),
    (21,'2017-04-26 13:53:08','2017-04-26 13:53:08','Roland','Alpha Juno','RolandAlphajuno'),
    (22,'2017-04-26 14:42:26','2017-04-26 14:42:26','DSI','Prophet 12','DSIPro12'),
    (23,'2017-07-13 17:53:00','2017-07-13 17:53:00','Intellijel','Plonk','IntellijelPlonk'),
    (24,'2017-08-04 18:36:57','2017-08-04 18:36:57','Ensoniq','ESQ1','EnsoniqEsq1'),
    (25,'2017-11-05 15:25:08','2017-11-05 15:25:15','Oberheim','OB-8','OberheimOb8'),
    (26,'2017-12-10 13:52:51','2017-12-10 13:52:51','DSI','Prophet Rev2','DSIProphetRev2'),
    (27,'2019-02-11 17:47:21','2019-02-11 17:47:21','Sequential','Prophet X','SequentialProphetX'),
    (28,'2019-03-23 11:43:16','2019-03-23 11:43:16','Waldorf','MicroWave','WaldorfMicrowave'),
    (29,'2019-07-04 14:46:54','2019-07-04 14:46:58','Casio','CZ-5000','CasioCz5000');

/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table programs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `programs`;

CREATE TABLE `programs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `dna` varchar(255) DEFAULT NULL,
  `device_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `data` mediumtext,
  PRIMARY KEY (`id`),
  KEY `dna` (`dna`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `modified` datetime DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `master` tinyint(1) DEFAULT '0',
  `active` tinyint(1) DEFAULT '0',
  `prodate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `created`, `modified`, `username`, `email`, `password`, `master`, `active`, `prodate`)
VALUES
    (1,'2016-05-10 17:00:52','2020-02-05 14:50:42','admin','kontakt@numerisch.com','$2y$10$q739kyE8/fyktApBNchCze5V2SFCZLv9K8/mfjKhcSmJXgzUe2kFi',1,1,'2016-11-20 12:10:37');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
