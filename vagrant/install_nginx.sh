#!/usr/bin/env bash

echo "--- Good morning, master. Let's get to work. Installing now. ---"

echo "--- Updating packages list ---"
sudo apt-get update
# echo "--- Upgrade all ---"
# sudo apt-get dist-upgrade -y
# sudo apt-get update

echo "--- Configure MySQL ---"
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

echo "--- Installing base packages ---"
sudo apt-get install -y vim curl locales rsync debconf-utils

echo "--- Installing PHP-specific packages ---"
# sudo apt-get install -y mysql-server-5.5 nginx php5-fpm php5-mysql php5-gd php5-intl php5-mcrypt php5-cli php5-curl git-core php5-imagick npm
sudo apt-get install -y mysql-server-5.5 nginx php5-fpm php5-mysql php5-gd php5-intl php5-mcrypt php5-cli php5-curl git-core npm ssl-cert

echo "--- Tweaking max sizes for form upload ---"
sudo sed -i "s/post_max_size = .*/post_max_size = 64M/" /etc/php5/fpm/php.ini
sudo sed -i "s/upload_max_filesize = .*/upload_max_filesize = 64M/" /etc/php5/fpm/php.ini
sudo sed -i "s/max_input_vars = .*/max_input_vars = 20000/" /etc/php5/fpm/php.ini
sudo sed -i "s/memory_limit = .*/memory_limit = 512M/" /etc/php5/fpm/php.ini

echo "--- Path Info for nginx (check manually!) ---"
sudo sed -i "s/cgi.fix_pathinfo = .*/cgi.fix_pathinfo = 0/" /etc/php5/fpm/php.ini

# Check in pool.d/www.conf : listen = /var/run/php5-fpm.sock

echo "--- Copy Skeleton ---"
sudo rsync -rv --exclude=.DS_Store /var/www/html/f0f7/vagrant/skeleton/ /

# Set listen = /var/run/php5-fpm.sock in pool.d/www.conf

echo "--- Restarting nginx ---"
sudo ln -s /etc/nginx/sites-available/app.dev /etc/nginx/sites-enabled/
sudo service php5-fpm restart
sudo service nginx restart

echo "--- Create Database ---"
sudo echo "create database IF NOT EXISTS f0f7" | mysql -u root -proot
sudo mysql -u root -proot f0f7 < /var/www/html/f0f7/www/be/config/schema/f0f7.sql

# add sql user f0f7 and grant
sudo mysql -u root -proot -e "CREATE USER f0f7@localhost IDENTIFIED BY 'root';"
sudo mysql -u root -proot -e "GRANT ALL PRIVILEGES ON f0f7.* TO 'f0f7'@'localhost';"
sudo mysql -u root -proot -e "FLUSH PRIVILEGES;"

echo "--- Install Composer ---"
sudo curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer

echo "--- Install Bower ---"
sudo ln -s /usr/bin/nodejs /usr/bin/node
sudo npm install -g bower

echo "--- mysql_secure_installation ---"
# mysql_secure_installation

echo "--- All set to go! Would you like to play a game? ---"

# ssl keys
# http://machiine.com/2013/easy-way-to-give-user-permission-to-edit-and-add-files-in-varwww/

